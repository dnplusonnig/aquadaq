////////////////////////////////////////////////////////////
//
// Implementation of the class TSIS1100
//
// John RM Annand 19th Feb. 2014
//
///////////////////////////////////////////////////////////
//--------------------------------------------------------
// TSIS110
// Interface to SIS1100 PCI-VME optical bridge

#ifndef _TSIS1100_H_
#define _TSIS1100_H_

#include "TObject.h"

class TSIS1100: public TObject{
private:
  int fdevA24D16;                      // File desc. 
  int fdevA32D16;                      // File desc. 
  int fdevA32D32;                      // File desc. 
public:
  TSIS1100();          // Create a new branch
  virtual ~TSIS1100();
  int GetA24D16(){ return fdevA24D16; }
  int GetA32D16(){ return fdevA32D16; }
  int GetA32D32(){ return fdevA32D32; }
  UShort_t RA24D16(UInt_t);
  void WA24D16(UInt_t, UShort_t);
  UShort_t RA32D16(UInt_t);
  void WA32D16(UInt_t, UShort_t);
  UInt_t RA32D32(UInt_t);
  void WA32D32(UInt_t, UInt_t);
  ClassDef( TSIS1100, 0)       // Camac Branch driver 
};

#endif /* _TSIS1100_H_ */
