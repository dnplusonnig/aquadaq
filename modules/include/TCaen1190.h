/////////////////////////////////////////////////////
// 
// Caen 1190 32 Channel TDC
//
// Magnus Lundin 05-07-27 
// John Annand 19/02/14 Modify for TSIS1100 interface class
//
////////////////////////////////////////////////////
#ifndef _TCaen1190_H_
#define _TCaen1190_H_

#include "TObject.h"
class TSIS1100;
class TCaen1190: public TObject{
private:
  TSIS1100* fSIS;
  UInt_t fbase;
  UInt_t hwaddress;                      // The hwaddress
  UShort_t debug;                        // Debug flag
  UShort_t multLimit;
public:
  TCaen1190( TSIS1100*, UInt_t BASE, UShort_t threshold, UShort_t mult = 4, UShort_t debugflag = 0);  // hw address and thresholds
  virtual ~TCaen1190();

  // Generic write/read access to the 16 bit mem area
  void SetShort( UShort_t address, UShort_t value);
  UShort_t GetShort( UShort_t address);
  void SetLong( UShort_t address, UInt_t value);
  UInt_t GetLong( UShort_t address);
  Short_t SetOpCode( UShort_t com, UShort_t obj);
  Short_t GetOpCode();
  void SoftwareClear(){ SetShort( 0x1016, 0x1);};

  void SetWindowWidthNS( Double_t wwns);
  void SetWindowOffsetNS( Double_t wons);
  UShort_t DataReady(){ if ( (GetShort( 0x1002) & 0x0001) == 0x0001 ) return 1; else return 0;}
  UShort_t GetData( UShort_t *ch, UInt_t *val);
  // debug functions
  void Info();               
  void PrintStatus();
  void bitprint( UInt_t buffer);
  void Test();

  ClassDef( TCaen1190, 0)
};

#endif /* _TCaen1190_H_ */




