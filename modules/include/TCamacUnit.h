/////////////////////////////////////////////////////
// 
// Camac unit abstract class
//
// Dahn Nilsson 1999-03-02
////////////////////////////////////////////////////
#ifndef _TCamacUnit_H_
#define _TCamacUnit_H_

#include "TObject.h"
#include "TCamacBranch.h"
#include "TCamacCrate.h"

class TCamacUnit: public TObject{

private:

  TCamacBranch *fCBD;
  const int fCrate;             // Crate number
  const int fSlot;              // Slot number
  int fNoChannels;              // Number of channels

  volatile Short_t *pRead[16];
  volatile Short_t *pReadClear[16];
  volatile Short_t *pWrite[16];
  
  volatile Short_t *fClear; // Clear module
  volatile Short_t *fTest; // Test module

public:
  TCamacUnit(TCamacBranch *cbd, int c, int n, int ch = 16);
  TCamacUnit(TCamacCrate *C, int n, int ch = 16);
  virtual ~TCamacUnit();

  Short_t& operator[](int);
  Int_t ReadArray( Short_t *Array, Int_t Count);
  Short_t ReadClear(Int_t n){ return *pReadClear[n];} // Read and clear

  Bool_t Q() {return fCBD->Q();} // Q response
  Bool_t X() {return fCBD->X();} // X response

  Bool_t Write(int val, int n);

  void Clear() {if (*fClear) ;}
  void Test(int l=3);
  void Print();
  
  ClassDef( TCamacUnit, 0)  // Camac unit
};

#endif /* _TCamacUnit_H_ */
