/////////////////////////////////////////////////////
// 
// Phillips 7186
// 16-channel TDC
//
// Dahn Nilsson 1999-10-06
////////////////////////////////////////////////////
#ifndef _TPhillips7186_H_
#define _TPhillips7186_H_

#include "TObject.h"
#include "TCamacBranch.h"
#include "TCamacCrate.h"

class TPhillips7186: public TObject{

private:

  TCamacBranch *fCBD;
  const int fCrate;             // Crate number
  const int fSlot;              // Slot number
  int fNoChannels;              // Number of channels

  volatile Short_t *pRead[16];
  volatile Short_t *pWrite[16];
  
  volatile Short_t *fClear; // Clear module
  volatile Short_t *fTest;  // Test module
  volatile Short_t *fHit;   // Read hit pattern module
  volatile Short_t *fSparse; // Sparse readout

  volatile Short_t *func(int func, int address) // Get address
    {return (volatile Short_t *)fCBD->cnaf( fCrate, fSlot, address, func); }

public:
  TPhillips7186(TCamacBranch *cbd, int c, int n);
  TPhillips7186(TCamacCrate *C, int n);
  virtual ~TPhillips7186();

  Short_t& operator[](int n){ return *(Short_t *)pRead[n];}
  Int_t ReadArray( Short_t *Array, Int_t Count);
  Int_t ReadSparseArray( UShort_t *Array);

  Bool_t TestFunc( int f, int a) { if( *func( f, a) ) ; return Q();}
  Short_t ReadFunc( int f, int a) { return *func( f, a);}
  Bool_t WriteFunc( int f, int a, int val) { *func( f, a) = val ; return Q();}

  Bool_t Q() {return fCBD->Q();} // Q response
  Bool_t X() {return fCBD->X();} // X response

  Bool_t Write(int val, int n){ *pWrite[n] = val; return Q();}

  void Clear() {if (*fClear) ;}

  Short_t GetControl(){ return *func( 6, 0);}
  Short_t GetHit(){ return *fHit;}

  Bool_t  Reset();		// Reset all 

  Bool_t SetPedestal( int ch, int ped); // Set pedestal for channel ch
  Bool_t SetLowerThreshold( int ch, int tr);  // Set lower threshold for channel ch
  Bool_t SetUpperThreshold( int ch, int tr);  // Set upper threshold for channel ch

  Short_t GetPedestal( int ch);                // Set pedestal for channel ch
  Short_t GetLowerThreshold( int ch);          // Set lower threshold for channel ch
  Short_t GetUpperThreshold( int ch);          // Set upper threshold for channel ch

  Bool_t EnablePedestal(int b=1);             // Enable pedestal, b=0 disables
  Bool_t EnableLowerThreshold(int b=1);       // Enable lower threshold, b=0 disables
  Bool_t EnableUpperThreshold(int b=1);       // Enable upper threshold, b=0 disables

  /* Test functions */
  Bool_t DigitalTest(int pattern=3);    // Run the digital test with pattern
  Bool_t ScaleTest( int scale=1);       // Run the scale test (scale= 1 or 2)
  Bool_t Test(int l=3);		        // Run a verbose self test

  void Print();
  
  ClassDef( TPhillips7186, 0);  
};

#endif /* _TPhillips7186_H_ */
