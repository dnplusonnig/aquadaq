////////////////////////////////////////////////////////////////////////////
// 
// Caen C208 (CAMAC)
// 16 channel programmable CFD
// John Annand 19/02/14 Modify for TSIS1100 interface class
// 
///////////////////////////////////////////////////////////////////////////
#ifndef _TCaenC208_H_
#define _TCaenC208_H_

#include "TObject.h"
#include "TCamacBranch.h"
#include "TCamacCrate.h"

class TCaenC208: public TObject{

private:

  static const int fNoChannels = 16;

  TCamacBranch *fCBD;           
  Int_t fCrate;			// Crate number
  Int_t fSlot;			// Slot number

  UInt_t fReadThreshold[fNoChannels];  
  UInt_t fReadInhibit;
  UInt_t fReadOutputWidth;
  UInt_t fReadDeadtime;
  UInt_t fReadMajorityThreshold;

  UInt_t fWriteThreshold[fNoChannels];  
  UInt_t fWriteInhibit;
  UInt_t fWriteOutputWidth;
  UInt_t fWriteDeadtime;
  UInt_t fWriteMajorityThreshold;
  
  void InitUnit();		

  // volatile Short_t *func(int func, int address) // Get address
  //  {return (volatile Short_t *)fCBD->cnaf( fCrate, fSlot, address, func); }

public:

  TCaenC208(TCamacBranch *cbd, int c, int n);
  TCaenC208(TCamacCrate *C, int n);
  virtual ~TCaenC208();

  UShort_t ReadThreshold( UShort_t ch){ return fCBD->Read(fReadThreshold[ch]);};
  UShort_t ReadInhibit(){ return fCBD->Read(fReadInhibit);};
  UShort_t ReadOutputWidth(){ return fCBD->Read(fReadOutputWidth);};
  UShort_t ReadDeadtime(){ return fCBD->Read(fReadDeadtime);};
  UShort_t ReadMajorityThreshold(){ return fCBD->Read(fReadMajorityThreshold);};

  void WriteThreshold(UShort_t ch,UShort_t val){fCBD->Write(fWriteThreshold[ch],val);}
  void WriteInhibit(UShort_t val){fCBD->Write(fWriteInhibit,val);}
  void WriteOutputWidth(UShort_t val){fCBD->Write(fWriteOutputWidth,val);}
  void WriteDeadtime(UShort_t val){fCBD->Write(fWriteDeadtime,val);}
  void WriteMajorityThreshold(UShort_t val){fCBD->Write(fWriteMajorityThreshold,val);}


  void WriteThresholds( UShort_t val);
  void PrintThresholds();

  Bool_t Q() {return fCBD->Q();} // Q response
  Bool_t X() {return fCBD->X();} // X response

  ClassDef( TCaenC208, 0 )  
};

#endif // _TCaenC208_HH_
