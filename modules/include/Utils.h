/////////////////////////////////////////////////////
// 
// Mapping and other useful common functions
//
// Magnus Lundin 2007-01-23
//
////////////////////////////////////////////////////
#ifndef _UTILS_H_
#define _UTILS_H_

#include "TObject.h"

Short_t Map( char *memdev, UInt_t hwadd, UInt_t size, volatile UShort_t *base16, volatile UInt_t *base32);

//void BitPrint();


#endif /* _UTILS_H_ */




