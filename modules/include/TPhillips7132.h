#ifndef _TPhillips7132_H_
#define _TPhillips7132_H_

#include "TObject.h"
#include "TCamacBranch.h"
#include "TCamacCrate.h"

class TPhillips7132 : public TObject
{
private:
  TCamacBranch *fCBD;
  const UShort_t fCrate;             // Crate number
  const UShort_t fSlot;              // Slot number
  UShort_t fNoChannels;              // Number of channels

  volatile UShort_t *pRead[16];	     // Two banks, need 16 bit access
  volatile UShort_t *selBank; 
  volatile UShort_t *fClear;

public:
  TPhillips7132( TCamacBranch *cbd, UShort_t c, UShort_t n, UShort_t ch = 32);
  TPhillips7132(TCamacCrate *C, UShort_t n, UShort_t ch = 32);
  virtual ~TPhillips7132();

  void  PrintAll();		// Print all channels

  void Clear() {if (*fClear) ;}
  int Test( UShort_t t = 3 );	// Test scaler

  void  ReadArray( UInt_t *arr);
  UInt_t& operator[](UShort_t);
  void SelectBank(int b = 0 ){ *selBank = b;}

  ClassDef( TPhillips7132, 0);  
};



#endif /* _TPhillips7132_H_ */
