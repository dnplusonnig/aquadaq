/////////////////////////////////////////////////////
// 
// Juri
//
// Magnus Lundin 
//
////////////////////////////////////////////////////
#ifndef _TJuri_H_
#define _TJuri_H_

#include "TObject.h"


class TJuri: public TObject{

private:

  int ffd;                               // File descriptor
  volatile void *fbase;                  // Base address for io
  volatile UShort_t *fbase16;            // 16 bit access
  UInt_t hwaddress;                      // The hwaddress
  UShort_t debug;                        // Debug flag

public:
  TJuri( UInt_t BASE);  // hw address
  virtual ~TJuri();

  // Generic write/read access to the 16 bit mem area
  void SetShort( UShort_t address, UShort_t value);
  UShort_t GetShort( UShort_t address);
  
  void Init( UInt_t BASE);


  ClassDef( TJuri, 0 )
};

#endif /* _TJuri_H_ */




