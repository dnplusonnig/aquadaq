//  John Annand 20th Feb. 2014  Modified for SIS1100
#ifndef _TLRS2551_H_
#define _TLRS2551_H_

#include "TObject.h"
#include "TCamacBranch.h"
#include "TCamacCrate.h"

class TLRS2551 : public TObject
{
private:
  TCamacBranch *fCBD;
  const int fCrate;             // Crate number
  const int fSlot;              // Slot number
  int fChannels;                // Number of channels

  UInt_t pRead[12];	     // Data read
  UInt_t pReadClear[12];     // Data read and clear
  UInt_t fClear;	     // Clear module
  UInt_t fIncrement;	     // Increment all channels by 1

public:
  TLRS2551( TCamacBranch *cbd, int c, int n, int ch = 12);
  TLRS2551(TCamacCrate *C, int n, int ch = 12);
  virtual ~TLRS2551();

  UShort_t ReadArray( UInt_t *arr); 
  UShort_t ReadClearArray( UInt_t *arr); 
  void PrintAll();
  void Clear(){ fCBD->Read(fClear);}
  void Increment(){ fCBD->Read(fIncrement);}

  UShort_t TestBits( UShort_t highestBit, UShort_t ch, UInt_t bp);

  ClassDef( TLRS2551, 0);

};



#endif /* _TLRS2551_H_ */
