/////////////////////////////////////////////////////
// 
// Camac Branch driver 
//
// Dahn Nilsson 1999-03-02
// Magnus Lundin 2004-05-27 minor modifications
// John Annand 19/02/14 Modify for TSIS1100 interface class
//
////////////////////////////////////////////////////
#ifndef _TCamacBranch_H_
#define _TCamacBranch_H_
#include "TObject.h"
#include "CBDcamac.h"

#define QBIT 0x8000
#define XBIT 0x4000

class TSIS1100;
class TCamacBranch: public TObject{

private:
  TSIS1100* fSIS;
  UInt_t fbase;             // Base address for io
  UInt_t fcsr;           // Control and status register
  UInt_t fbtb;           // Branch register
  UInt_t fifr;           // Interrupt flag register

  int fInitInterrupt();             // enable interrupts
  int fWait();                      // requsting an interrupt
  int fCleanUpInterrupt();          // disable interrupts

public:
  TCamacBranch(TSIS1100*,int = 0);          // Create a new branch
  virtual ~TCamacBranch();
  UShort_t Read(UInt_t);
  void Write(UInt_t, UShort_t);
  /*
  UShort_t Read(UInt_t addr) {
    UShort_t val;
    vme_A24D16_read(fdevmem,addr,&val);
    return val;
  }
  void Write(UInt_t addr, UShort_t val){
    vme_A24D16_write(fdevmem,(u_int32_t)addr,(u_int16_t)val);
  }
  */
  Short_t Csr() { return Read(fcsr); }
  void SetCsr(Short_t csr) {Write(fcsr,csr);}
  void SetIfr(Short_t ifr) {Write(fifr,ifr);}
  UInt_t cnaf(int c, int n, int a, int f, int t = 1) // t = 0 for 24-bit
    { return (fbase + CBDOffset( c, n, a, f, t));}
  Bool_t Q() {return (Read(fcsr) & QBIT) > 1;}
  Bool_t X() {return (Read(fcsr) & XBIT) > 1;}

  Bool_t Online(int c)              // Check if crate c is online
  {return ( c > 0 && c < 8 ? (Read(fbtb) & ( 1 << c)) : 0);}

  int Wait();                           // Wait for interrupts

  Short_t Poll( UShort_t secs);         // Polling IT2 during sec
  Short_t ToggleAck4();                 // Output pulse on ack4

  void Reset() {Write(fbtb,1);}
  void Print();
  
  ClassDef( TCamacBranch, 0)       // Camac Branch driver 
};

#endif /* _TCamacBranch_H_ */
