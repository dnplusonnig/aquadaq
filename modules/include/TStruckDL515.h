/////////////////////////////////////////////////////
// 
// Struck DL515 Flash ADC
//
// Dahn Nilsson 1999-10-12
////////////////////////////////////////////////////
#ifndef _TStruckDL515_H_
#define _TStruckDL515_H_

#include "TObject.h"
#include "TCamacBranch.h"
#include "TCamacCrate.h"

#define DL515BASE    0xf0e00000	// Base address
#define DL515SIZE    0x8100	// Size of memory region

class TStruckDL515: public TObject{

private:

  int fdevmem;                   // File desc. for /dev/mem
  volatile void *fbase;          // Base address for io

  Int_t AutoStop;
  Int_t fSum;			// Number of channels to sum up
  Int_t fPos;			// Address counter
  Int_t fLast;			// Last position in fStore;

  volatile UInt_t *fData;	// Data array
  volatile UInt_t *fStart;	// Software start
  volatile UInt_t *fStop;	// Software stop
  volatile UInt_t *fEOCTrigg;	// Reset EOC/Trigger
  volatile UInt_t *fAutoStop;	// Set AutoStop

  volatile UInt_t *fStatus;	// Address counter & Status

  static const int fNoChannels = 2048; // Number of channels
  static const int fMaxMem = 8192; // Total memory

  UInt_t fStore[fNoChannels];

  void Init();
public:
  TStruckDL515();
  TStruckDL515(int sum);	// Sum up sum channels
  virtual ~TStruckDL515();

  Int_t ReadArray(Short_t *Array, Int_t Channel, Int_t Count = fNoChannels);
  Int_t ReadLongArray(UInt_t *Array, Int_t Count = fNoChannels);

  void Start()    { *fStart = 1; }
  void Stop()     { *fStop = 1; }
  void ResetEOC() { *fEOCTrigg = 1; }
  void SetAutoStop( int as = 1 ) { *fAutoStop = AutoStop = as; }  // Set autostop, as = 0 resets
  
  void Load();	   // Load data into storage

  Short_t Status() { return ( 0xffff & *fStatus ); }

  Bool_t Test(int l=3);
  void Print();
  
  ClassDef( TStruckDL515, 0 )
};

#endif /* _TStruckDL515_H_ */
