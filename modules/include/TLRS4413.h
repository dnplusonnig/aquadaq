////////////////////////////////////////////////////////////////////////////
// 
// LeCroy 4413 high-speed discrimintaor (CAMAC)
// 16 channels
// 
// 061206 Magnus Lundin
// John Annand 19/02/14 Modify for TSIS1100 interface class
//
///////////////////////////////////////////////////////////////////////////////////
#ifndef _TLRS4413_H_
#define _TLRS4413_H_

#include "TObject.h"
#include "TCamacBranch.h"
#include "TCamacCrate.h"

class TLRS4413: public TObject{

private:

  TCamacBranch *fCBD;           
  Int_t fCrate;			   // Crate number
  Int_t fSlot;			   // Slot number

  UInt_t pReadMask;    // Read mask
  UInt_t pWriteMask;   // Write mask
  UInt_t pReadThres;   // Read thresholds
  UInt_t pWriteThres;  // Write thresholds
  UInt_t pLocal;       // Local mode
  UInt_t pRemote;      // Remote mode

  void InitUnit();		   // Initialize stuff

public:

  TLRS4413(TCamacBranch *cbd, int c, int n);
  TLRS4413(TCamacCrate *C, int n);
  virtual ~TLRS4413();

  void EnableChannels( UShort_t bp);           // Enable channels
  void SetLocalMode();                         // Set unit to local mode
  void SetRemoteMode();                        // Set unit to remote mode

  void PrintDisabled();                        // Print the disabled channels
  void CopyLocalThres();                       // Copy the locally set threshold to software thresholds
  void SetVoltThresholds( Double_t val);           
  void PrintThresholds();

  Bool_t Q() {return fCBD->Q();} // Q response
  Bool_t X() {return fCBD->X();} // X response

  ClassDef( TLRS4413, 0 )  
};

#endif // _TLRS4413_HH_
