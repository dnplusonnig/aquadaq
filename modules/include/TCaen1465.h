/////////////////////////////////////////////////////
// 
// Caen 1465 64 Channel QDC
//
// Magnus Lundin 05-05-28 
//
////////////////////////////////////////////////////
#ifndef _TCaen1465_H_
#define _TCaen1465_H_

#include "TObject.h"

class TCaen1465: public TObject{


private:

  int ffd;                               // File desc. 
  volatile void *fbase;                  // Base address for io
  volatile UShort_t *fbase16;            // 16 bit access
  volatile UInt_t   *fbase32;            // 32 bit access
  UInt_t hwaddress;                      // The hwaddress
  UShort_t debug;                        // Debug flag

public:
  TCaen1465( UInt_t BASE, UShort_t debugflag = 0);   
  virtual ~TCaen1465();

  // Generic write/read access to the 16/32 bit memory areas
  void SetShort( UShort_t address, UShort_t value);
  UShort_t GetShort( UShort_t address);
  UShort_t GetShort( UShort_t address, UShort_t bitmask);
  UInt_t GetLong( UShort_t address);
  void SetBit( UShort_t address, UShort_t bitNo, UShort_t value);

  void Init( UInt_t BASE);
  void Reset() { SetShort(0x000e,0x1);}
  void PrintHardwareStatus();
  void PrintModuleIdentifierWords();
  void PrintGeneralControl();
  UShort_t DataReady(){ if ( ((GetShort( 0x3006) >> 11) & 0x0001) == 1 ) return 1; else return 0;}
  UShort_t DataMemoryFull(){ if ( ((GetShort( 0x3006) >> 12) & 0x0001) == 1 ) return 0; else return 1;}
  UShort_t Busy(){ if ( (GetShort( 0x3006) & 0x4000) == 0x4000 ) return 1; else return 0;}
  void SetRange( const char *s);    // low, high, auto

  void EnableSparse(){ SetBit( 0x0102, 4, 1); SetBit( 0x0182, 4, 1);}   // Enable zero/overflow suppression
  void DisableSparse(){ SetBit( 0x0102, 4, 0); SetBit( 0x0182, 4, 0);}  // Disable zero/overflow suppression
  void SetHighHexThreshold( UShort_t thres);
  UShort_t GetHighHexThreshold();
  void SetLowHexThreshold( UShort_t thres);
  UShort_t GetLowHexThreshold();

  void IncrementPointer(){ SetShort( 0x0004, 0x0001);}// makes the read pointer point to the next 
                                                      // 32-bit word in the MEB
  UShort_t GetMultiplicity( UInt_t buffer);           // the multiplicity of the event
  UShort_t IsHeader( UInt_t buffer);                              
  UShort_t IsDatum( UInt_t buffer);
  UShort_t IsEOB( UInt_t buffer);                                   

  UShort_t GetChannel( UInt_t buffer);
  UShort_t GetValue( UInt_t buffer);
  UShort_t GetData( UShort_t *ch, UShort_t *val);

  void Test();
  void bitprint( UInt_t buffer);
  void errormessage(const char *s){ fprintf( stderr,
  "Module at %d. Error at line %d in %s\n%s\n", hwaddress, __LINE__, __FILE__, s);}

  ClassDef( TCaen1465, 0)
};

#endif /* _TCaen1465_H_ */




