////////////////////////////////////////////////////////////////
// 
// Silena 4418V  (CAMAC)
// 8-Input ADC (10V)
//
//
// The Model 4418V has 8 input channels which acquire the 
// peak voltage of the input signals that occur within the 
// time interval marked by a common gate signal. The circuit 
// accepts all the typical pulse shapes of nuclear electronic 
// modules, such as spectroscopy amplifiers, time-to-amplitude 
// converters. In the 4418V, digitized data are available
// first on the front-panel ECL port and then on the CAMAC 
// dataway, giving the user two readout options. All zero or 
// zero-and-overflow data words may be suppressed to provide 
// data compression. 
// 
//
// Dahn Nilsson 2000-02-09
///////////////////////////////////////////////////////////////
#ifndef _TSilena4418V_H_
#define _TSilena4418V_H_

#include "TObject.h"
#include "TCamacBranch.h"
#include "TCamacCrate.h"

class TSilena4418V: public TObject{

private:

  static const int fNoChannels = 8; // Number of channels

  TCamacBranch *fCBD;
  Int_t fCrate;			// Crate number
  Int_t fSlot;			// Slot number

  volatile Short_t *pRead[fNoChannels];	// Read addresses
  volatile Short_t *pWrite[fNoChannels]; // Write addresses
  
  volatile Short_t *fClear;	// Clear module
  volatile Short_t *fTest;	// Test module

  void InitUnit();		// Initialize stuff

  volatile Short_t *func(int func, int address) // Get address
    {return (volatile Short_t *)fCBD->cnaf( fCrate, fSlot, address, func); }

public:

  TSilena4418V(TCamacBranch *cbd, int c, int n);
  TSilena4418V(TCamacCrate *C, int n);
  virtual ~TSilena4418V();

  Short_t& operator[](int n){ return *(Short_t *)pRead[n];}
  Int_t ReadArray( Short_t *Array, Int_t Count);

  Short_t GetHit() {return (0xff & *func( 0, 15));}

  Bool_t Q() {return fCBD->Q();} // Q response
  Bool_t X() {return fCBD->X();} // X response

  Bool_t Write(int val, int n){ *pWrite[n] = val; return Q();}

  void WriteStatus(int s){ *func( 20, 14) = s; }
  Short_t ReadStatus(){ return *func( 4, 14); }

  void WriteCommonThreshold(int t){ *func( 20, 9) = t; }
  Short_t ReadCommonThreshold(){ return (0xffff & *func( 4, 9)); }

  void WriteLowerThreshold(int n, int t){ *func( 17, 8 + n) = t; }
  Short_t ReadLowerThreshold(int n){ return (0xffff & *func( 1, 8 + n)); }

  void WriteUpperThreshold(int n, int t){ *func( 17, n) = t; }
  Short_t ReadUpperThreshold(int n){ return (0xffff & *func( 1, n)); }

  void WriteOffset(int n, int o){ *func( 20, n) = o; }
  Short_t ReadOffset(int n){ return (0xffff & *func( 4, n)); }

  void Clear() {if (*fClear) ;}
  void Test(int l=3);

  void Print();
  
  ClassDef( TSilena4418V, 0 )  // Camac unit
};

#endif // _TSilena4418V_HH_
