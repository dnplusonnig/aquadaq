/////////////////////////////////////////////////////
// 
// Flash ADC
//
// Dahn Nilsson 1999-09-29
////////////////////////////////////////////////////
#ifndef _TFADC_H_
#define _TFADC_H_

#include "TObject.h"
#include "TCamacBranch.h"
#include "TCamacCrate.h"

class TFADC: public TObject{

private:

  TCamacBranch *fCBD;
  const int fCrate;             // Crate number
  const int fSlot;              // Slot number
  int fNoChannels;              // Number of channels

  volatile Short_t *fRead;      // Sequential readout
  volatile Short_t *fClear;     // Clear module
  volatile Short_t *fTest;      // Test module
  volatile Short_t *fEnable;
  volatile Short_t *fDisable;

  volatile Short_t *func(int func, int address) // Get address
    {return (volatile Short_t *)fCBD->cnaf( fCrate, fSlot, address, func); }

public:
  TFADC(TCamacBranch *cbd, int c, int n);
  TFADC(TCamacCrate *C, int n);
  virtual ~TFADC();

  Int_t ReadArray( Short_t *Array, Int_t Count);

  Bool_t Q() {return fCBD->Q();} // Q response
  Bool_t X() {return fCBD->X();} // X response

  Bool_t TestFunc( int f, int a) { if( *func( f, a) ) ; return Q();}
  Short_t ReadFunc( int f, int a) { return *func( f, a);}
  Bool_t WriteFunc( int f, int a, int val) { *func( f, a) = val ; return Q();}

  Bool_t Clear() {if (*fClear) ; return Q();}
  Bool_t Enable() {if (*fEnable) ; return Q();}
  Bool_t Disable() {if (*fDisable) ; return Q();}
  Bool_t Test(int l=3);
  void Print();
  
  ClassDef( TFADC, 0);  
};

#endif /* _TFADC_H_ */
