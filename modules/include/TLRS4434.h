//////////////////////////////////////////////////////////////////////
//
// Implementation of the LeCroy 32-channel scaler 
//
// 000508 Magnus Lundin
// John Annand 19/02/14 Modify for TSIS1100 interface class
//
//////////////////////////////////////////////////////////////////////
#ifndef _TLRS4434_H_
#define _TLRS4434_H_

#include "TObject.h"
#include "TCamacBranch.h"
#include "TCamacCrate.h"

/* 32 channels, Clear, Bus disable */
#define TLRS4434CLEAR 0x3f40      

/* Read 32 channels starting at channel 0, 
Load counters to the common register, Bus disable */
#define TLRS4434LOAD  0x3f20      

class TLRS4434 : public TObject
{
private:
  TCamacBranch *fCBD;
  const int fCrate;             // Crate number
  const int fSlot;              // Slot number
  const int fChannels;        // Number of channels

  UInt_t pComm;      // Pointer to command register
  UInt_t pReadClear; // Pointer to the first channel to be read 
  UInt_t pRead;      // Pointer to the first channel to be read 

public:
  TLRS4434(TCamacCrate *C, int n, int channels = 32);
  virtual ~TLRS4434();

  void Clear() { fCBD->Write(pComm,TLRS4434CLEAR); }  
  void PrintAll();                   // Print all channels
  void Test();                       // A simple test

  UInt_t ReadChannelOne();
  UShort_t ReadClearArray( UInt_t *arr);
  UShort_t ReadArray( UInt_t *arr);

  ClassDef( TLRS4434, 0);

};


#endif /* _TLRS4434_H_ */





