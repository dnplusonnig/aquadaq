/////////////////////////////////////////////////////
// 
// Caen 534 Flash ADC
//
// Magnus Lundin 01-01-28 (based on TStruckDL515)
//
////////////////////////////////////////////////////
#ifndef _TCaen534_H_
#define _TCaen534_H_

#include "TObject.h"
#include "TCamacBranch.h"
#include "TCamacCrate.h"

#define CAEN534SIZE    0xffff	         // Size of memory region

#define AIDLE 0x1000                     // A idle 1, A active 0  
#define AFULL 0x2000                     // A full 1, A free   0
#define BIDLE 0x4000                     // B idle 1, B active 0  
#define BFULL 0x8000                     // B full 1, B free   0

#define DEBUG 0                          // verbose


class TCaen534: public TObject{

private:

  int fdevmem;                           // File desc. for /dev/mem
  volatile void *fbase;                  // Base address for io
  UInt_t hwaddress;                      // The hwaddress

  UInt_t fDepth;                         // number of channels to read backwards from the 
                                         //                                  stop trigger
  UInt_t fChannels;                      // number of input channels

  volatile UInt_t *fData;	         // Data array

  volatile UShort_t *fEventCounter;	 // Event counter      (ro)
  volatile UShort_t *fStop;	         // Software stop      (wo)

  volatile UShort_t *fReleaseB;	         // Release block B    (wo)
  volatile UShort_t *fLatchBStat;        // Latch B and status (r/w)
  volatile UShort_t *fReleaseA;	         // Release block A    (wo)
  volatile UShort_t *fLatchAStat;        // Latch A and status (ro)

  volatile UShort_t *fMemMode;           // Memory mode        (wo)
  volatile UShort_t *fRegMode;           // Register mode      (wo)

  volatile UShort_t *fDAC;	         // DAC                (wo)
  volatile UShort_t *fReset;	         // Software reset     (wo)

  static const int fNoChannels = 4096;   // Number of channels

  UInt_t fStore[fNoChannels];            // holds the data


  void Init();

  void Reset() { *fReset = 1;}

  UInt_t *GetStopAddress(); // returns pointer to the stop address

  void Stop() { *fStop = 1;}

  void ReleaseA() { *fReleaseA = 1;}
  UShort_t GetLatchAStat() { return (0xffff & *fLatchAStat ); }
  void ReleaseB() { *fReleaseB = 1;}
  UShort_t GetLatchBStat() { return (0xffff & *fLatchBStat); }

  int AReadable();          // return 1 if A is idle and full else 0 
  int BReadable();          // return 1 if B is idle and full else 0

  void SetMemMode() { *fMemMode = 1;}
  void SetRegMode() { *fRegMode = 1;}

  UInt_t GetDepth() { return fDepth;};               // returns the depth
  UInt_t GetChannels() { return fChannels;};         // returns the nuber of input channels

public:
  TCaen534( UInt_t base, int depth, int channels);   // hwaddress, depth, channels	
  virtual ~TCaen534();

  UShort_t GetEventCounter() { return *fEventCounter & 0xff;}

  UShort_t SetDAC( UShort_t value ) { *fDAC = value; return value; } 

  int Load();	                                     // Load data into storage

  void Test( UInt_t depth);

  void PrintBlockStatus();  // print status for block A and B

  Int_t ReadArray( UShort_t *Array, UInt_t Channel, UInt_t depth);
  
  ClassDef( TCaen534, 0 )
};

#endif /* _TCaen534_H_ */
