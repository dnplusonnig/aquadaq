//
// TCaenv260 16 channel NIM scaler
// Magnus Lundin 061130
//
//
#ifndef __TCaen260_H__
#define __TCaen260_H__

#include "TObject.h"

class TCaen260: public TObject{

private:
  
  int ffd2416;                           // File desc. 
  int ffd2432;                           // File desc. 
  volatile void *ftmpbase2432;           // Base address for io
  volatile void *ftmpbase2416;           // Base address for io
  volatile UShort_t *fbase2416;          // 16 bit access
  volatile UInt_t   *fbase2432;          // 32 bit access
  UInt_t hwaddress;                      // The hwaddress
  UShort_t debug;                        // Debug flag

public:
  TCaen260(  UInt_t BASE, UShort_t debugflag);
  ~TCaen260();

  void Init( UInt_t BASE);

  // Generic write/read access to the 16 bit mem area
  void SetShort( UShort_t address, UShort_t value);
  UShort_t GetShort( UShort_t address);

  void SetInhibit(){ GetShort( 0x52);}
  void ResetInhibit(){ GetShort( 0x54);}

  void Clear(){ GetShort( 0x50);}	// Clear all scalers
  void Increment(){ GetShort( 0x56);}   // Increment all scalers

  UShort_t GetData( UShort_t *ch, UInt_t *val); 
  UInt_t GetChannelData( UShort_t ch); 
  void PrintAll();

  ClassDef( TCaen260, 0)

};
#endif // __TCaen260_H__
