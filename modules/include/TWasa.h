/////////////////////////////////////////////////////
// 
// Wasa
//
// Magnus Lundin 
//
////////////////////////////////////////////////////
#ifndef _TWasa_H_
#define _TWasa_H_

#include "TObject.h"


class TWasa: public TObject{

private:

  int ffd;                               // File descriptor
  volatile void *fbase;                  // Base address for io
  volatile UShort_t *fbase16;            // 16 bit access
  volatile UInt_t   *fbase32;            // 32 bit access
  UInt_t hwaddress;                      // The hwaddress
  UShort_t debug;                        // Debug flag

public:
  TWasa( UInt_t BASE, UShort_t debugflag = 0);  // hw address
  virtual ~TWasa();

  // Generic write/read access to the 16 bit mem area
  void SetShort( UShort_t address, UShort_t value);
  UShort_t GetShort( UShort_t address);

  void SetLong( UShort_t address, UInt_t value);
  UInt_t GetLong( UShort_t address);
  
  void Init( UInt_t BASE);


  ClassDef( TWasa, 0 )
};

#endif /* _TWasa_H_ */




