/////////////////////////////////////////////////////
// 
// Caen 1488 64 Channel TDC
//
// Magnus Lundin 05-09-21
//
////////////////////////////////////////////////////
#ifndef _TCaen1488_H_
#define _TCaen1488_H_

#include "TObject.h"


class TCaen1488: public TObject{

private:

  int ffd;                               // File desc. 
  volatile void *fbase;                  // Base address for io
  volatile UShort_t *fbase16;            // 16 bit access
  volatile UInt_t   *fbase32;            // 32 bit access
  UInt_t hwaddress;
  UShort_t debug;

public:
  TCaen1488( UInt_t BASE, UShort_t debugflag);
  virtual ~TCaen1488();

  // Generic write/read access to the 16 bit mem area
  void SetShort( UShort_t address, UShort_t value);
  UShort_t GetShort( UShort_t address);
  UShort_t GetShort( UShort_t address, UShort_t bitmask);
  UInt_t GetLong( UShort_t address);

  void EnableZeroOverflowSupression();
  void SetHighThreshold( UShort_t ht);
  void SetLowThreshold( UShort_t lt);
  void SetRange( UShort_t r);

  void Init( UInt_t BASE);
  void Reset() { SetShort(0x000e,0x1);}
  void PrintHardwareStatus();
  void PrintModuleIdentifierWords();
  void PrintGeneralControl();
  void PrintAcquisitionStatus();
  void bitprint( UInt_t buffer);

  UShort_t DataReady(){      if ( (GetShort( 0x3006) & 0x0800) == 0x0800) return 1; else return 0;}
  UShort_t DataMemoryFull(){ if ( (GetShort( 0x3006) & 0x1000) == 0x1000) return 0; else return 1;}
  UShort_t Busy(){           if ( (GetShort( 0x3006) & 0x4000) == 0x4000) return 1; else return 0;}


  void IncrementPointer(){ SetShort( 0x0004, 0x0001);}// makes the read pointer point to the next 
                                                      // 32-bit word in the MEB
  UShort_t GetMultiplicity( UInt_t buffer);           // the multiplicity of the event
  UShort_t IsHeader( UInt_t buffer);                              
  UShort_t IsDatum( UInt_t buffer);
  UShort_t IsEOB( UInt_t buffer);                                   

  UShort_t GetChannel( UInt_t buffer);
  UShort_t GetValue( UInt_t buffer);
  UShort_t GetData( UShort_t *ch, UShort_t *val);


  ClassDef( TCaen1488, 0)
};

#endif /* _TCaen1488_H_ */




