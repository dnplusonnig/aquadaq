
#include "TBorer1008.h"

ClassImp( TBorer1008 )

TBorer1008::TBorer1008(TCamacBranch *cbd, int c, int n): 
fCBD( cbd ), fCrate(c), fSlot(n)
{
  InitUnit();
}

TBorer1008::TBorer1008(TCamacCrate *C, int n):
fCBD( C->GetCBD() ), fCrate( C->Crate() ), fSlot(n)
{
  InitUnit();
}

TBorer1008::~TBorer1008( )
{
}

void TBorer1008::InitUnit()
{
  if( ! fCBD->Online( fCrate ) )
    fprintf( stderr, "Warning: Crate %d is not online!\n", fCrate);

  for( int i = 0; i < fNoChannels; i++)
    {
      // Note t = 0 to indicate 24-bit access
      pRead[i] = (volatile UShort_t *)fCBD->cnaf( fCrate, fSlot, i, 0, 1);
      pWrite[i] = (volatile UShort_t *)fCBD->cnaf( fCrate, fSlot, i, 17, 1);
      pStart[i] = (volatile Short_t *)fCBD->cnaf( fCrate, fSlot, i, 25);
      pClear[i] = (volatile Short_t *)fCBD->cnaf( fCrate, fSlot, i, 9);
    }
}

UInt_t TBorer1008::LoadCounter( UInt_t val)
{
  // 16 bit access
  *( volatile UShort_t *)(pWrite[0]  - 1)  = 0xffff & (val >> 16); 
  *( volatile UShort_t *)(pWrite[0]     )  = 0xffff & val; 

  return 0;
}

UInt_t TBorer1008::GetCounter( UShort_t ch)
{
  UInt_t ret;

  if ( ch > 1)
    {
      printf("channel out of range TBorer1008\n");
      return 0;
    }

  // 16 bit access
  ret =  *( volatile UShort_t *)(pRead[ch]  - 1) << 16; 
  ret |= *( volatile UShort_t *)pRead[ch];

  return ret;
}

Int_t TBorer1008::ReadArray( Int_t *Array, Int_t count)
{
  // Read an array of count values into Array
  // from channel starting on 0.
  
  int i;

  for( i = 0; i < count; i++)
    *Array++ = *(Int_t *)pRead[i];

  return i;
}

void TBorer1008::Test(int l)
{

}

void TBorer1008::Print( )
{
  for( int i = 0; i < fNoChannels; i++)
    printf( "Channel: %d, value: %d\n", i, operator[](i));
}
