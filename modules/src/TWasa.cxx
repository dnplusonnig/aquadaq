///////////////////////////////////////////////////////////////////
//
// Caen 977 
//
// Magnus Lundin 
//
///////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <fcntl.h>

#include "TWasa.h"

ClassImp( TWasa )

const char description[] = "Wasa data interface";
const UShort_t WASASIZE = 0xffff;

#ifdef SIS
const char memdev[] = "/dev/sis110024d16";
#endif
#ifdef SBS
const char memdev[] = "/dev/vmedrv24d16";
#endif

///////////////////////////////////////////////////////////////////
TWasa::TWasa( UInt_t BASE, UShort_t debugflag)
{
  hwaddress = BASE;
  debug = debugflag;
  Init( BASE);
}
///////////////////////////////////////////////////////////////////
TWasa::~TWasa( )
{
  munmap( (char *)fbase, WASASIZE);
  close( ffd);
}
///////////////////////////////////////////////////////////////////
// Generic write/read access to the 16 bit memory area (the control section)
void TWasa::SetShort( UShort_t address, UShort_t value)
{
  *(fbase16 + address/2) = value;
  return;
}
UShort_t TWasa::GetShort( UShort_t address)
{
 return ( *(fbase16 + address/2) );  
}

void TWasa::SetLong( UShort_t address, UInt_t value)
{
  *(fbase32 + address/4) = value;
  return;
}

UInt_t TWasa::GetLong( UShort_t address)
{
 return ( *(fbase32 + address/4) );  
}

///////////////////////////////////////////////////////////////////
void TWasa::Init( UInt_t BASE)
{
  if (  (ffd = open( memdev, O_RDWR | O_SYNC)) == -1) 
    {
      printf("Module %s at 0x%x : file %s line %d\n", description, hwaddress, __FILE__, __LINE__);
      //      printf("open() failed: %s\n", strerror(errno));
      exit(EXIT_FAILURE);
    }

  fbase = (volatile void *)mmap(NULL, WASASIZE, PROT_WRITE, MAP_SHARED, ffd,
				BASE);

  if ( fbase == (void *)-1 )
    {
      printf("Module %s at 0x%x : file %s line %d\n", description, hwaddress, __FILE__, __LINE__);
      //      printf("mmap() falied: %s\n", strerror(errno));
      exit(EXIT_FAILURE);
    }

  fbase16 = (volatile UShort_t *)((char *)fbase);
  fbase32 = (volatile UInt_t *)((char *)fbase);  

  if ( debug > 1)
    {
      fprintf( stderr, "fbase at %p \n", fbase);
    }

  return;
}

