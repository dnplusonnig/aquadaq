
#include "TSen2047.h"

ClassImp( TSen2047 )

TSen2047::TSen2047(TCamacBranch *cbd, int c, int n): 
fCBD( cbd ), fCrate(c), fSlot(n)
{

  if( ! cbd->Online( fCrate ) )
    fprintf( stderr, "Warning: Crate %d is not online!\n", fCrate);

  fClear = (volatile Short_t *)cbd->cnaf( fCrate, fSlot, 0, 9);
  fTest = (volatile Short_t *)cbd->cnaf( fCrate, fSlot, 0, 25);

  pRead = (volatile Short_t *)cbd->cnaf( fCrate, fSlot, 0, 0);
  pReadClear = (volatile Short_t *)cbd->cnaf( fCrate, fSlot, 0, 2);
}

TSen2047::TSen2047(TCamacCrate *C, int n):
fCBD( C->GetCBD() ), fCrate( C->Crate() ), fSlot(n)
{

  if( ! fCBD->Online( fCrate ) )
    fprintf( stderr, "Warning: Crate %d is not online!\n", fCrate);

  fClear = (volatile Short_t *)fCBD->cnaf( fCrate, fSlot, 0, 9);
  fTest = (volatile Short_t *)fCBD->cnaf( fCrate, fSlot, 0, 25);

  pRead = (volatile Short_t *)fCBD->cnaf( fCrate, fSlot, 0, 0);
  pReadClear = (volatile Short_t *)fCBD->cnaf( fCrate, fSlot, 0, 2);
}

TSen2047::~TSen2047( )
{
}

void TSen2047::Test()
{
  volatile Short_t dum;
  printf("Test sets all 16 bit to 1\n");
  dum = *fTest;
  return;
}

void TSen2047::Print( )
{
  printf( "Value: %x\n", Read());
  return;
}

void TSen2047::BitPrint()
{
  Short_t i = 15;
  UShort_t buffer = Read();

  while( i >= 0)
    {
      if ( (buffer >> i) & 0x00000001 )
        printf("1");
      else
        printf("0");
      if ( (i % 8 == 0) && (i != 0))
        printf("|");
      i--;
    }
  printf("\n");
}
