///////////////////////////////////////////////////////////////////
//
// Juri
//
// Magnus Lundin 
//
///////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <fcntl.h>

#include "TJuri.h"

ClassImp( TJuri )

const char description[] = "Juri";
const UShort_t JURISIZE = 0xffff;

#ifdef SIS
const char memdev[] = "/dev/sis110016d16";
#endif
#ifdef SBS
const char memdev[] = "/dev/vmedrv16d16";
#endif

///////////////////////////////////////////////////////////////////
TJuri::TJuri( UInt_t BASE)
{
  hwaddress = BASE;
  Init( BASE);
}
///////////////////////////////////////////////////////////////////
TJuri::~TJuri( )
{
  munmap( (char *)fbase, JURISIZE);
  close( ffd);
}
///////////////////////////////////////////////////////////////////
// Generic write/read access 
void TJuri::SetShort( UShort_t address, UShort_t value)
{
  *(fbase16 + address/2) = value;
  return;
}
UShort_t TJuri::GetShort( UShort_t address)
{
 return ( *(fbase16 + address/2) );  
}
///////////////////////////////////////////////////////////////////
void TJuri::Init( UInt_t BASE)
{
  if (  (ffd = open( memdev, O_RDWR | O_SYNC)) == -1) 
    {
      printf("Module %s at 0x%x : file %s line %d\n", description, hwaddress, __FILE__, __LINE__);
      //      printf("open() failed: %s\n", strerror(errno));
      exit(EXIT_FAILURE);
    }

  fbase = (volatile void *)mmap(NULL, JURISIZE, PROT_WRITE, MAP_SHARED, ffd,
				BASE);

  if ( fbase == (void *)-1 )
    {
      printf("Module %s at 0x%x : file %s line %d\n", description, hwaddress, __FILE__, __LINE__);
      //      printf("mmap() falied: %s\n", strerror(errno));
      exit(EXIT_FAILURE);
    }

  fbase16 = (volatile UShort_t *)((char *)fbase);

  if ( debug > 1)
    {
      fprintf( stderr, "fbase at %p \n", fbase);
    }

  return;
}
