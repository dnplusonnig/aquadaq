// John Annand 20th Feb. 2014  Modify for SIS1100
//
#include "TLRS4416.h"

ClassImp( TLRS4416 )

const char fDescription[] = "LeCroy discriminator";  


TLRS4416::TLRS4416(TCamacBranch *cbd, int c, int n): 
fCBD( cbd ), fCrate(c), fSlot(n)
{
  InitUnit();
}

TLRS4416::TLRS4416(TCamacCrate *C, int n):
fCBD( C->GetCBD() ), fCrate( C->Crate() ), fSlot(n)
{
  InitUnit();
}

TLRS4416::~TLRS4416( )
{
}

void TLRS4416::InitUnit()
{
  if( ! fCBD->Online( fCrate ) )
    {
      printf("Camac module %s in crate %d slot %d: file %s line %d\n", 
	     fDescription, fCrate, fSlot, __FILE__, __LINE__);            
      fprintf( stderr, "Warning: Crate is not online\n");
    }
  pTest = fCBD->cnaf( fCrate, fSlot, 0, 25);
  pMask = fCBD->cnaf( fCrate, fSlot, 0, 16);
  pReadThresh = fCBD->cnaf( fCrate, fSlot, 0, 1);
  pWriteThresh = fCBD->cnaf( fCrate, fSlot, 0, 17);
  return;
}

void TLRS4416::MaskChannels( UShort_t bitpattern)
{
  if ( bitpattern > 0xffff)
    {
      printf("Camac module %s in crate %d slot %d: file %s line %d\n", 
	     fDescription, fCrate, fSlot, __FILE__, __LINE__);            
      fprintf( stderr, "Warning: bitpattern out of range\n");
    }
  bitpattern = bitpattern & 0xffff;
  fCBD->Write(pMask,bitpattern);
  return;
}

void TLRS4416::PrintThreshold()
{
  printf("Camac module %s in crate:%d slot:%d  threshold:%d\n", 
	 fDescription, fCrate, fSlot,fCBD->Read(pReadThresh) );            
}

void TLRS4416::SetThreshold( UShort_t thres)
{
  if ( thres > 0x03ff)
    {
      printf("Camac module %s in crate %d slot %d: file %s line %d\n", 
	     fDescription, fCrate, fSlot, __FILE__, __LINE__);            
      fprintf( stderr, "Warning: threshold out of range, not applied\n");
      return;
    }
  fCBD->Write(pWriteThresh,thres);
  return;
}

void TLRS4416::Test()
{
  // Generate test pulse
  fCBD->Read(pTest);
  return;
}



