///////////////////////////////////////////////////////////////////
//
// Caen 977 
//
// Magnus Lundin 
//
///////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <fcntl.h>

#include "TCaen977.h"

ClassImp( TCaen977 )

const char description[] = "Caen 977 I/O Register";
const UShort_t CAEN977SIZE = 0xffff;

#ifdef SIS
const char memdev[] = "/dev/sis110032d16";
#endif
#ifdef SBS
const char memdev[] = "/dev/vmedrv32d16";
#endif

///////////////////////////////////////////////////////////////////
TCaen977::TCaen977( UInt_t BASE, UShort_t debugflag)
{
  hwaddress = BASE;
  debug = debugflag;
  Init( BASE);
  SoftwareReset();
  GatedInputMode();  
  AccessTest();
}
///////////////////////////////////////////////////////////////////
TCaen977::~TCaen977( )
{
  munmap( (char *)fbase, CAEN977SIZE);
  close( ffd);
}
///////////////////////////////////////////////////////////////////
// Generic write/read access to the 16 bit memory area (the control section)
void TCaen977::SetShort( UShort_t address, UShort_t value)
{
  *(fbase16 + address/2) = value;
  return;
}
UShort_t TCaen977::GetShort( UShort_t address)
{
 return ( *(fbase16 + address/2) );  
}
///////////////////////////////////////////////////////////////////
void TCaen977::Init( UInt_t BASE)
{
  if (  (ffd = open( memdev, O_RDWR | O_SYNC)) == -1) 
    {
      printf("Module %s at 0x%x : file %s line %d\n", description, hwaddress, __FILE__, __LINE__);
      //      printf("open() failed: %s\n", strerror(errno));
      exit(EXIT_FAILURE);
    }

  fbase = (volatile void *)mmap(NULL, CAEN977SIZE, PROT_WRITE, MAP_SHARED, ffd,
				BASE);

  if ( fbase == (void *)-1 )
    {
      printf("Module %s at 0x%x : file %s line %d\n", description, hwaddress, __FILE__, __LINE__);
      //      printf("mmap() falied: %s\n", strerror(errno));
      exit(EXIT_FAILURE);
    }

  fbase16 = (volatile UShort_t *)((char *)fbase);
  fbase32 = (volatile UInt_t *)((char *)fbase);  

  if ( debug > 1)
    {
      fprintf( stderr, "fbase at %p \n", fbase);
    }

  return;
}
/////////////////////////////////////////////////////////////////////////
UShort_t TCaen977::GetBitpatternSH()
{
  UShort_t bp = GetShort( 0x0006);
  return bp;
}
/////////////////////////////////////////////////////////////////////////
UShort_t TCaen977::GetBitpatternMH()
{
  UShort_t bp = GetShort( 0x0008);
  return bp;
}
/////////////////////////////////////////////////////////////////////////
UShort_t TCaen977::SetBitpattern( UShort_t bp)
{  
  OutputMode();
  SetShort( 0x0000, bp);
  ClearBitpattern();
  GatedInputMode();  
  return 0;
}
/////////////////////////////////////////////////////////////////////////
UShort_t TCaen977::PollBit( UShort_t bit, UShort_t sec)
{ 
  time_t start, now;

  // Mask output ie input is not relayed to output
  SetShort( 0x000c, 0xffff);

  // Get the control register
  UShort_t cr = GetShort( 0x0028);   

  // Do not require the gate to be present for input to be registered
  // Pavel 101125
  // SetShort( 0x0028, 0x0007 & (cr & ~0x0002));
  SetShort( 0x0028, 0x0007 & (cr | 0x0002));

  start = time(NULL);
  now = start;

  // Poll 
  while( !((GetShort( 0x0006) >> bit) & 0x0001) 
	 && (difftime(now, start) < (double)sec) ) 
    now = time(NULL); 

  ClearBitpattern();
  

  if (difftime(now, start) < (double)sec)  // no timeout
    return 0;
  else
    return 1; // timeout
}
/////////////////////////////////////////////////////////////////////////
UShort_t TCaen977::AccessTest()
{
  UShort_t pattern;

  pattern = 0xffff;
  SetShort( 0x002a, pattern);
  if ( GetShort( 0x002a) != pattern)
    {
      printf("Module %s at 0x%x : file %s line %d\n", description, hwaddress, __FILE__, __LINE__);
      printf("AccessTest failed, pattern 0x%x\n", pattern);
      exit(EXIT_FAILURE);
    }

  pattern = 0x0000;
  SetShort( 0x002a, pattern);
  if ( GetShort( 0x002a) != pattern)
    {
      printf("Module %s at 0x%x : file %s line %d\n", description, hwaddress, __FILE__, __LINE__);
      printf("AccessTest failed, pattern 0x%x\n", pattern);
      exit(EXIT_FAILURE);
    }

  pattern = 0x7777;
  SetShort( 0x002a, pattern);
  if ( GetShort( 0x002a) != pattern)
    {
      printf("Module %s at 0x%x : file %s line %d\n", description, hwaddress, __FILE__, __LINE__);
      printf("AccessTest failed, pattern 0x%x\n", pattern);
      exit(EXIT_FAILURE);
    }

  return 0;
}
/////////////////////////////////////////////////////////////////////////
void TCaen977::PrintStatus()
{
  printf("Input set reg      \t 0x%x\n", GetShort( 0x0000));
  printf("Input mask reg     \t 0x%x\n", GetShort( 0x0002));
  printf("Input read reg     \t 0x%x\n", GetShort( 0x0004));
  printf("Single hit read reg\t 0x%x\n", GetShort( 0x0006));
  printf("Multi hit read reg \t 0x%x\n", GetShort( 0x0008));
  printf("Output set reg     \t 0x%x\n", GetShort( 0x000a));
  printf("Output mask reg    \t 0x%x\n", GetShort( 0x000c));
  printf("Interrupt mask     \t 0x%x\n", GetShort( 0x000e));
  printf("Test control reg   \t 0x%x\n", GetShort( 0x001a));
  printf("Interrupt level    \t 0x%x\n", GetShort( 0x0020));
  printf("Interrupt vector   \t 0x%x\n", 0x00ff & GetShort( 0x0022));
  printf("Serial number      \t 0x%x\n", GetShort( 0x0024));
  printf("Firmware version   \t 0x%x\n", GetShort( 0x0026));
  printf("Control reg        \t 0x%x\n", GetShort( 0x0028));

  return;
}
