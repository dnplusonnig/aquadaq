////////////////////////////////////////////////////////////
//
// Implementation of the class TSIS1100
//
// John RM Annand 19th Feb. 2014
//
///////////////////////////////////////////////////////////
//--------------------------------------------------------
// TSIS110
// Interface to SIS1100 PCI-VME optical bridge

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include "sis3100_vme_calls.h"
#include "TSIS1100.h"

//char devA24D16[] = "/dev/sis110024d16";
//char devA32D16[] = "/dev/sis110032d16";
//char devA32D32[] = "/dev/sis110032d32";
char devA24D16[] = "/dev/sis1100_00remote";

ClassImp( TSIS1100 )

///////////////////////////////////////////////////////////////////
TSIS1100::TSIS1100()
{
  // Open descriptor to device driver
  if (  (fdevA24D16 = open( devA24D16, O_RDWR | O_SYNC)) == -1) 
    {
      perror("open devA24D16");
      exit(1);
    }
  /*
  if (  (fdevA32D16 = open( devA32D16, O_RDWR | O_SYNC)) == -1) 
    {
      perror("open devA32D16");
      exit(1);
    }
  if (  (fdevA32D32 = open( devA32D32, O_RDWR | O_SYNC)) == -1) 
    {
      perror("open devA32D32");
      exit(1);
    }
  */
  fdevA32D16 = fdevA32D32 = fdevA24D16;
  printf("SIS1100 Driver descriptors A24D16:%d  A32D16:%d  A32D32:%d\n",
	 fdevA24D16,fdevA32D32,fdevA32D32);
}

///////////////////////////////////////////////////////////////////
TSIS1100::~TSIS1100( )
{
  close( fdevA24D16 );
  //close( fdevA32D32 );
  //close( fdevA32D16 );
}

UShort_t TSIS1100::RA24D16(UInt_t addr) {
  UShort_t val;
  int err = vme_A24D16_read(fdevA24D16,addr,&val);
  if( err ){
    printf("Error read A24D16: %d @addr %x\n",err,addr);
    return 0xffff;
  }
  return val;
}
void TSIS1100::WA24D16(UInt_t addr, UShort_t val){
   int err =  vme_A24D16_write(fdevA24D16,addr,val);
   if( err ){
     printf("Error write A24D16: %d @addr %x\n",err,addr);
     return;
   }
}
UShort_t TSIS1100::RA32D16(UInt_t addr) {
  UShort_t val;
  int err = vme_A32D16_read(fdevA32D16,addr,&val);
  if( err ){
    printf("Error read A32D16: %d @addr %x\n",err,addr);
    return 0xffff;
  }
  return val;
}
void TSIS1100::WA32D16(UInt_t addr, UShort_t val){
  int err = vme_A32D16_write(fdevA32D16,addr,val);
  if( err ){
    printf("Error write A32D16: %d @addr %x\n",err,addr);
    return;
  }
}
UInt_t TSIS1100::RA32D32(UInt_t addr) {
  UInt_t val;
  int err = vme_A32D32_read(fdevA32D32,addr,&val);
  if( err ){
    printf("Error read A32D32: %d @addr %x\n",err,addr);
    return 0xffffffff;
  }
  return val;
}
void TSIS1100::WA32D32(UInt_t addr, UInt_t val){
  int err = vme_A32D32_write(fdevA32D32,addr,val);
  if( err ){
    printf("Error write A32D32: %d @addr %x\n",err,addr);
    return;
  }
}







