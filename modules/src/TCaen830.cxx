///////////////////////////////////////////////////////////////////
//
// Caen 830    32 Channel Multievent Latching Scaler
//
// Magnus Lundin 2002-07-19 (loosly based on TStruckDL515)
// Erik Ljungbertz 2004-09-10
// Magnus Lundin 050602
// John Annand 19/02/14 Modify for TSIS1100 interface class
//
///////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <fcntl.h>

#include "TCaen830.h"
#include "TSIS1100.h"

ClassImp( TCaen830 )

const char description[] = "Caen 830 Scaler";
const UShort_t CAEN830SIZE = 0xffff;

// Groundstates
const UShort_t CONT = 0x0000;
const UShort_t STAT = 0x0000;                    


///////////////////////////////////////////////////////////////////
TCaen830::TCaen830( TSIS1100* sis, UInt_t BASE, UShort_t debugflag)
{
  fSIS = sis;
  hwaddress = BASE;
  debug = debugflag;
  fbase = BASE;
  if ( debug > 1){
    printf("fbase at %x \n", fbase);
    Info();
    PrintStatus();
  }
  if ( InGroundstate() != 1 ){
    printf("Module %s at 0x%x : file %s line %d\n", description, hwaddress, __FILE__, __LINE__);
    printf("Module not in groundstate\n");
    exit(-1);
  }
  SoftwareReset();
  SoftwareClear();
  SetShort( 0x110c, 0x0001);       // disable trigger
  SetShort( 0x110c, 0x0002);       // disable trigger
}
///////////////////////////////////////////////////////////////////
TCaen830::~TCaen830( )
{
}
///////////////////////////////////////////////////////////////////
// Generic write/read access to the 16 bit memory area (the control section)
void TCaen830::SetShort( UShort_t address, UShort_t value)
{
  fSIS->WA32D16(fbase+address,value);
}
UShort_t TCaen830::GetShort( UShort_t address)
{
  return fSIS->RA32D16(fbase+address);
}
UShort_t TCaen830::GetShort( UShort_t address, UShort_t bitmask)
{
  return (bitmask & (fSIS->RA32D16(fbase+address)));
}
// Generic write/read access to 32 bit memory area
UInt_t TCaen830::GetUInt( UShort_t address)
{
  return fSIS->RA32D32(fbase+address);
}
void TCaen830::SetUInt( UShort_t address, UInt_t value)
{
  fSIS->WA32D32(fbase+address,value);
  return ;  
}
//////////////////////////////////////////////////////////////////////////
void TCaen830::EnableChannels( UInt_t bitmask)
{
  // Use a bitmask to enable/disable channels (section 3.6).
  SetUInt( 0x1100, bitmask); 
  return;
}
//////////////////////////////////////////////////////////////////////////
UShort_t TCaen830::BufferEmpty()
{
  if ( GetShort(0x110e)  & 0x1 )
    return 0;
  else
    return 1;
}
//////////////////////////////////////////////////////////////////////////
UShort_t TCaen830::BufferFull()
{
  if ( (GetShort(0x110e) >> 2) & 0x1 )
    return 1;
  else
    return 0;
}
//////////////////////////////////////////////////////////////////////////////
UShort_t TCaen830::GetData( UShort_t *ch, UInt_t *val)
{
  UInt_t buffer, address;
  UShort_t i;

  // Check that the modeule is not busy
  if ( Busy())
    {
      printf("Module %s at 0x%x : file %s line %d\n", description, hwaddress, __FILE__, __LINE__);
      printf("module busy, return\n");
      return 0;      
    }

  for ( i = 0; i < 32; i++)
    {
      address = 0x1000 + i*4;
      buffer = GetUInt( address);      

      if (debug > 1)
	bitprint( buffer);

      *(ch+i)  = i;  // Set channel number
      *(val+i) = buffer;
    }

  return 32;
}
//////////////////////////////////////////////////////////////////////////////
//
//  Test, debug and info below
//

// Check that the module is in the prefered state
UShort_t TCaen830::InGroundstate()
{
  if ( ( GetShort(0x1108, 0x001f) == CONT   ) &&
       ( GetShort(0x110e, 0x001f) == STAT   )  )
    return 1;
  else
    return 0;
}
//////////////////////////////////////////////////////////////////////////
// Print some info to screen
void TCaen830::Info()
{
  printf("Firmware : 0x%x\n", GetShort( 0x1132, 0x00ff));  
  
  printf("GEO Adress : 0x%x\n", GetShort( 0x1110, 0x001f));  

  printf("Interrupt Level: 0x%x\n", GetShort(0x1112, 0x0007));  

  printf("Interrupt Vector: 0x%x\n", GetShort(0x1114, 0x00ff));  

  printf("Almost Full Level: 0x%x\n", GetShort(0x112c ));  

  printf("Trigger counter: 0x%x\n", GetUInt(0x1128));  

  printf("Board id : %d \n", (GetShort(0x803a,0x00ff)<<8)+GetShort(0x803e,0x00ff));

  return;
}
//////////////////////////////////////////////////////////////////////////
// Print some status to screen
void TCaen830::PrintStatus()
{
  UShort_t cont, stat ;
  UInt_t dwell;

  dwell= GetUInt(0x1104);
  printf("Dwell time: %d ns\n", (dwell*400));

  stat = GetShort( 0x110e, 0x00ff);
  printf("Status Register: 0x%x \n", stat);

  if ( stat & 0x0001 )
    printf("Status Reg \t: D_READY\n");
  else
    printf("Status Reg \t: NO D_READY\n");

  if ( stat & (0x0001 << 1) )
    printf("Status Reg \t: ALMOST FULL\n");
  else
    printf("Status Reg \t: NOT ALMOST FULL\n");

  if ( stat & (0x0001 << 2) )
    printf("Status Reg \t: FULL\n");
  else
    printf("Status Reg \t: NOT FULL\n");

  if ( stat & (0x0001 << 3) )
    printf("Status Reg \t: GLOBAL D_READY\n");
  else
    printf("Status Reg \t: NOT GLOBAL D_READY\n");

  if ( stat & (0x0001 << 4) )
    printf("Status Reg \t: GLOBAL BUSY\n");
  else
    printf("Status Reg \t: NOT GLOBAL BUSY\n");

  if ( stat & (0x0001 << 5) )
    printf("Status Reg \t: ALL TERM ON\n");
  else
    printf("Status Reg \t: NOT ALL TERM ON\n");

  if ( stat & (0x0001 << 6) )
    printf("Status Reg \t: ALL TERM OFF\n");
  else
    printf("Status Reg \t: NOT ALL TERM OFF\n");

  if ( stat & (0x0001 << 7) )
    printf("Status Reg \t: BERR FLAG\n");
  else
    printf("Status Reg \t: NO BERR FLAG\n");


  cont = GetShort( 0x1108, 0x00ff);
  printf("Control Register: 0x%x \n", cont);

  if ( ! (cont & 0x0003) )
    printf("Control Reg \t: Trigger Disabled\n");
  else if ((cont & 0x0003) == 0x0001)
    printf("Control Reg \t: Trigger random\n");
  else
    printf("Control Reg \t: Periodical Trigger \n");

  if ( cont & (0x0001 << 2) )
    printf("Control Reg \t: 26-bit Data word format\n");
  else
    printf("Control Reg \t: 32-bit Data word format\n");

  if ( cont & (0x0001 << 3) )
    printf("Control Reg \t: Test mode ON\n");
  else
    printf("Control Reg \t: Test mode OFF\n");

  if ( cont & (0x0001 << 4) )
    printf("Control Reg \t: BERR enabled\n");
  else
    printf("Control Reg \t: BERR disabled\n");

  if ( cont & (0x0001 << 5) )
    printf("Control Reg \t: Header writing enabled\n");
  else
    printf("Control Reg \t: Header writing disabled\n");

  if ( cont & (0x0001 << 6) )
    printf("Control Reg \t: MEB cleared after CLEAR signal\n");
  else
    printf("Control Reg \t: MEB not cleared after CLEAR signal\n");

  if ( cont & (0x0001 << 7) )
    printf("Control Reg \t: Automatic reset enabled\n");
  else
    printf("Control Reg \t: Automatic reset disabled\n");

  return;
}
/////////////////////////////////////////////////////////////////////////
void TCaen830::bitprint( UInt_t buffer)
{
  int i = 31;
  while( i >= 0)
    {
      if ( (buffer >> i) & 0x00000001 )
        printf("1");
      else
        printf("0");
      if ( (i % 8 == 0) && (i != 0))
        printf("|");
      i--;
    }
  printf("\n");
}
//////////////////////////////////////////////////////////////////////////
Short_t TCaen830::Test( UInt_t testValue)
{
  // Test based on section 3.5 in the manual

  UShort_t lowWord  = testValue & 0xffff;
  UShort_t highWord = (testValue >> 16) & 0xffff;
  UShort_t ch[32];
  UInt_t value[32];
  
  // Set module in test mode
  SetShort( 0x1108, 0x0008);

  SetShort( 0x1090, lowWord);   // channel 0-15 low
  SetShort( 0x1094, highWord);  // channel 0-15 high

  SetShort( 0x10a0, lowWord);   // channel 16-31 low
  SetShort( 0x10a4, highWord);  // channel 16-31 high

  GetData( ch, value);

  for ( int i = 0; i < 32; i++)
    if ( value[i] != (UInt_t )((highWord)<<16 | (lowWord)) )
      {
	printf("Module %s at 0x%x : file %s line %d\n", description, hwaddress, __FILE__, __LINE__);
	printf("Test() falied\n");
	return 1;
      }
 
  // Set module in non test mode
  SetShort( 0x110c, 0x0008);

  return 0;
}
//////////////////////////////////////////////////////////////////////////
void TCaen830::PrintChannels()
{
  UShort_t ch[32];
  UInt_t value[32];
  
  GetData( ch, value);

  for ( int i = 0; i < 32; i++)
    printf("ch : %d\t value : %x\n", ch[i], value[i]);

  return;
}
//////////////////////////////////////////////////////////////////////////
