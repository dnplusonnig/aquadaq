/////////////////////////////////////////////////////
// 
// Mapping and other useful common functions
//
// Magnus Lundin 2007-01-23
//
////////////////////////////////////////////////////
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <fcntl.h>

#include "Utils.h"


Short_t Map( char *memdev, UInt_t hwadd, UInt_t size, volatile UShort_t *base16 = NULL, 
	     volatile UInt_t *base32 = NULL)
{
  int fd16 = 0, fd32 = 0;
  volatile void *tmp16 = NULL;
  volatile void *tmp32 = NULL;

  if ( base16 != NULL)
    {
      if (  (fd16 = open( memdev, O_RDWR | O_SYNC)) == -1) 
	{
	  printf("(16) Module at 0x%x\n", hwadd);
	  printf("open() failed: %s\n", strerror(errno));
	  exit(EXIT_FAILURE);
	}
      if ((tmp16 = (volatile void *)mmap(NULL, size, 
		 PROT_WRITE, MAP_SHARED, fd16, hwadd)) == MAP_FAILED)
	{
	  printf("(16) Module at 0x%x\n", hwadd);
	  printf("mmap() falied: %s\n", strerror(errno));
	  exit(EXIT_FAILURE);
	}
      if ( tmp16 == (void *)-1 )
	{
	  printf("(16) Module at 0x%x\n", hwadd);
	  exit(-1);
	}
      base16 = (volatile UShort_t *)((char *)1tmp16);  
    }

  if ( base32 != NULL)
    {
      if (  (fd32 = open( memdev, O_RDWR | O_SYNC)) == -1) 
	{
	  printf("(32) Module at 0x%x\n", hwadd);
	  printf("open() failed: %s\n", strerror(errno));
	  exit(EXIT_FAILURE);
	}
      if ((tmp32 = (volatile void *)mmap(NULL, size, 
		 PROT_WRITE, MAP_SHARED, fd32, hwadd)) == MAP_FAILED)
	{
	  printf("(32) Module at 0x%x\n", hwadd);
	  printf("mmap() falied: %s\n", strerror(errno));
	  exit(EXIT_FAILURE);
	}
      if ( tmp32 == (void *)-1 )
	{
	  printf("(32) Module at 0x%x\n", hwadd);
	  exit(-1);
	}
      base32 = (volatile UInt_t *)((char *)tmp32);  
    }

  return 0;
}






