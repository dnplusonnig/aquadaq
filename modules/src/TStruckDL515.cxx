///////////////////////////////////////////////////////////////////
//
// Struck DL515 Flash ADC
// Dahn Nilsson 1999-10-12
///////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <fcntl.h>

#include "TStruckDL515.h"

ClassImp( TStruckDL515 )

#ifdef SIS
const char memdev[] = "/dev/sis110024d16";
#endif
#ifdef SBS
const char memdev[] = "/dev/vmedrv24d16";
#endif


static int init = 0;

TStruckDL515::TStruckDL515()
{
  Init();
}

TStruckDL515::TStruckDL515(int sum)
{
  Init();

  fSum = sum;
}
TStruckDL515::~TStruckDL515( )
{
  if ( --init )
    return;

  munmap( (char *)fbase, DL515SIZE);
  close( fdevmem );
}


void TStruckDL515::Init( )
{
  if( init++ )
    return;

  if (  (fdevmem = open( memdev, O_RDWR | O_SYNC)) == -1) 
    {
      perror("open /dev/mem");
      return;
    }

  fbase = (volatile void *)mmap(NULL, DL515SIZE, PROT_WRITE, MAP_SHARED, fdevmem,
				DL515BASE);

  if ( fbase == (void *)-1 )
    {
      perror("Error mmapping");
      return;
    }
  fSum = 0;

  fData     = (volatile UInt_t *)fbase;
  fStart    = (volatile UInt_t *)((char *)fbase + 0x8000);
  fStop     = (volatile UInt_t *)((char *)fbase + 0x8004);
  fEOCTrigg = (volatile UInt_t *)((char *)fbase + 0x8008);
  fAutoStop = (volatile UInt_t *)((char *)fbase + 0x800C);
  fStatus   = (volatile UInt_t *)((char *)fbase + 0x8000);
  
  *fAutoStop = 0;
  *fStop = 1;
  *fAutoStop = AutoStop = 1;
  *fEOCTrigg = 1;
}

Int_t TStruckDL515::ReadArray( Short_t *Array, Int_t Channel, Int_t count)
{
  // Read an array of count values into Array
  // from channel starting on 0.
  // Call Load() first.
  
  int i, j, max;
  UChar_t *fRead = (UChar_t *)fStore;

  if( Channel < 0 || Channel > 3)
    {
      fprintf( stderr, "Channel (%d) out of range (0-3) in TStruckDL515::ReadArray\n", Channel );
      return 0;
    }
  
  Channel = 3 - Channel;

  max = count > fLast ? fLast : count;

  if( AutoStop )
    {
      for( i = j = 0; i < max; i++, j += 4)
	*Array++ = *(fRead + j + Channel);
    }
  else
    {
      int start = (fLast - max)<<2;

      for( i = 0, j = start; i < max; i++, j += 4)
	*Array++ = *(fRead + j + Channel);
    }


  return i;
}

Int_t TStruckDL515::ReadLongArray( UInt_t *Array, Int_t count)
{
  // Read an array of count values into Array
  // from channel starting on 0.
  
  int i, max;

  max = count > fNoChannels ? fNoChannels : count;

  for( i = 0; i < max ; i++ )
    *Array++ = fData[i];

  return i;
}

// magnus was inline
void TStruckDL515::Load()
{
  fPos = *fStatus & 0x3ff;

  fLast = fNoChannels;

  if( AutoStop )
    {
      if( fSum )
	{
	  int k;

	  for( int i = k = 0; i < fNoChannels ; i += fSum, k++ )
	      fStore[k] = fData[i]; // Pick every fSum value (fStore limited to 255 counts)

	  fLast = k;
	}
      else
	{
	  for( int i = 0; i < fNoChannels ; i++ )
	    fStore[i] = fData[i];
	}
    }
  else
    {
      int i,j;
      int start = (fPos + 1)<<1;
      
      if( start == fNoChannels )
	start = 0;
	  
      if( fSum )
	{
	  int k, l;

	  for( i = k = 0, j = start; j < fNoChannels; i += fSum, k++, j++ ) // Tail end
	    for( l = 0; l < fSum; l++)
	      fStore[k] = fData[j+l];
	  
	  for( j = 0; i < fNoChannels; i += fSum, k++, j++) // Get the rest from 0
	    for( l = 0; l < fSum; l++)
	      fStore[k] = fData[j+l];

	  fLast = k;

	}
      else
	{
	  for( i = 0, j = start; j < fNoChannels; i++, j++ ) // Tail end
	    fStore[i] = fData[j];
	  
	  for( j = 0; i < fNoChannels; i++, j++) // Get the rest from 0
	    fStore[i] = fData[j];
	}
    }
}

Bool_t TStruckDL515::Test(int l)
{

  return 1;
}

void TStruckDL515::Print( )
{

  for( int i = 0; i < fNoChannels; i++)
    {
      printf( "Channel: %d, value: 0x%08x ( 0x%02x  0x%02x  0x%02x  0x%02x )\n",
	      i, fData[i],
	      0xff & fData[i]>>24, 0xff & fData[i]>>16,  0xff & fData[i]>>8, 0xff & fData[i] );
    }
  printf( "Status: 0x%x\n", Status());
}



