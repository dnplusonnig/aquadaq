///////////////////////////////////////////////////////////////////
//
// Caen 1488 64 channel QDC
// Magnus Lundin 05-05-28 
//
///////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <string.h>

#include "TCaen1465.h"

ClassImp( TCaen1465 )

const UShort_t TCAEN1465SIZE = 0xffff;

#ifdef SIS
const char memdev[] = "/dev/sis110032d16";
#endif
#ifdef SBS
const char memdev[] = "/dev/vmedrv32d16";
#endif


TCaen1465::TCaen1465( UInt_t BASE, UShort_t debugflag)
{
  hwaddress = BASE;
  debug = debugflag;
  Init( BASE);

  Reset();

  SetShort( 0x0006, 0x0000);  // General control

  SetShort( 0x0080, 0x0000);  // Set the high threshold
  SetShort( 0x0082, 0x0000);  // Set the low threshold

  SetShort( 0x0084, 0x0000);  // Set the pedestal to 0

  SetShort( 0x0100, 0x000f);  // Enable channels 0-31
  SetShort( 0x0180, 0x000f);  // Enable channels 32-63

  SetShort( 0x0104, 0x0);     // No zero/overflow suppression, no montoring 
  SetShort( 0x0184, 0x0);     // No zero/overflow suppression, no montoring 
  SetShort( 0x0108, 0x0001);  // Disable montoring
  SetShort( 0x0188, 0x0001);  // Disable montoring
 
  SetRange("high");   
  
  SetShort( 0x010A, 0x1);     // Clear event counter
  SetShort( 0x3000, 0x0000);  // Trigger counter mode
}

TCaen1465::~TCaen1465()
{
  munmap( (char *)fbase, TCAEN1465SIZE);
  close( ffd);
}

//////////////////////////////////////////////////////////////////////////
//
// Generic write/read access to the 16 bit memory area (the control section)
//
void TCaen1465::SetShort( UShort_t address, UShort_t value)
{
  *(fbase16 + address/2) = value;
  return;
}
UShort_t TCaen1465::GetShort( UShort_t address)
{
 return ( *(fbase16 + address/2) );  
}
UShort_t TCaen1465::GetShort( UShort_t address, UShort_t bitmask)
{
  return (bitmask & (*(fbase16 + address/2)));
}

//
// 32 bit access to the MEB
//
UInt_t TCaen1465::GetLong( UShort_t address)
{
  return ( *(fbase32 + address/4));
}
//////////////////////////////////////////////////////////////////////////
void TCaen1465::SetBit( UShort_t address, UShort_t bitNo, UShort_t value)
{
  UShort_t reg;

  if ( bitNo > 15)
    {
      errormessage("bitNo needs to be in the range 0 - 15");
      return;
    }
  if ( (value > 1) )
    {
      errormessage("value needs to be either 1 or 0");
      return;
    }

  // Get the old value 
  reg = GetShort( address);
  
  // Calc the new value
  if ( value == 1)
    reg = reg | (1 << bitNo); 
  else
    reg = reg & ~(1 << bitNo); 

  SetShort( address, reg);
  return;
}
//////////////////////////////////////////////////////////////////////////
void TCaen1465::Init( UInt_t BASE)
{
  if ((ffd = open( memdev, O_RDWR)) == -1) {
    errormessage("open() failed"); //printf("%s\n", strerror(errno));
    exit(EXIT_FAILURE);
  }
    
  if ((fbase = mmap(0, TCAEN1465SIZE, PROT_WRITE, MAP_SHARED, ffd, BASE)) == MAP_FAILED) {
    errormessage("mmap() falied"); // printf("%s\n", strerror(errno));
    exit(EXIT_FAILURE);
  }

  fbase = (volatile unsigned short *)fbase;

  if ( fbase == (void *)-1 )
    {
      errormessage("");
      exit(-1);
    }

  fbase16 = (volatile UShort_t *)((char *)fbase);
  fbase32 = (volatile UInt_t *)((char *)fbase);  

  PrintHardwareStatus();
  PrintModuleIdentifierWords();

  return;
}
//////////////////////////////////////////////////////////////////////////
void TCaen1465::PrintHardwareStatus()
{
  UShort_t status;
  status = GetShort( 0x0008);

  if ( (status & 0x0004) == 0)
    printf("Hardware status: CTRL BUS TERM ON\n");

  if ( (status & 0x0008) == 0)
    printf("Hardware status: CTRL BUS TERM OFF\n");

  printf("Hardware status: BOARD NO. %d\n", (status & 0x1f00) >> 8);

  return;
}
//////////////////////////////////////////////////////////////////////////
void TCaen1465::PrintModuleIdentifierWords()
{
  UShort_t status;

  status = GetShort( 0x00FC);
  printf("PrintModuleIdentifierWords: Module type %d\n", status & 0x03ff);  
  printf("PrintModuleIdentifierWords: Manufacturer No %d\n", (status & 0xfc00) >> 10);  

  status = GetShort( 0x00FE);
  printf("PrintModuleIdentifierWords: Serial No %d\n", status & 0x0fff);  
  printf("PrintModuleIdentifierWords: Version %d\n", (status & 0xf000) >> 12);  
  return;
}
//////////////////////////////////////////////////////////////////////////
void TCaen1465::PrintGeneralControl()
{
  UShort_t status;
  status = GetShort( 0x0006);

  printf("General control: AUTOINC_ENABLE %d\n", status & 0x0001);  
  printf("General control: TEST           %d\n", (status & 0x0004) >> 2);  
  printf("General control: VETO_VME       %d\n", (status & 0x0008) >> 3);  
  printf("General control: BERR_ENABLE    %d\n", (status & 0x0010) >> 4);  
  printf("General control: STR_MODE       %d\n", (status & 0x0020) >> 5);  
  printf("General control: STP_MODE       %d\n", (status & 0x0040) >> 6);  
  return;
}
//////////////////////////////////////////////////////////////////////////
void TCaen1465::SetRange( const char *s)
{
  if ( strstr( s, "low") )
    {
      SetBit( 0x0102, 7, 1);
      SetBit( 0x0102, 8, 1);
      SetBit( 0x0182, 7, 1);
      SetBit( 0x0182, 8, 1);
      printf("low range selected\n");
    }
  if ( strstr( s, "high") )
    {
      SetBit( 0x0102, 7, 0);
      SetBit( 0x0102, 8, 1);
      SetBit( 0x0182, 7, 0);
      SetBit( 0x0182, 8, 1);
      printf("high range selected\n");
    }
  if ( strstr( s, "auto") )
    {
      SetBit( 0x0102, 7, 0);
      SetBit( 0x0102, 8, 0);
      SetBit( 0x0182, 7, 0);
      SetBit( 0x0182, 8, 0);
      printf("auto range selected\n");
    }
  return;
}
//////////////////////////////////////////////////////////////////////////
void TCaen1465::SetHighHexThreshold( UShort_t thres)
{
  // Check what range-mode the module is in
  // high range : 0xff <=> 3840 counts, low range : 0x20 <=> 3840 counts
  UShort_t mode = 0x0180 & GetShort( 0x102);
  UShort_t value;

  if ( mode == 0x0100)  // high range
    {
      value = (thres / 15) + 1;
      if ( value > 0xff)
	value = 0xff;
      printf("value is %d\n", value);
      SetShort( 0x0080, value);
    }
  if ( mode == 0x0180)  // low range  
    {
      value = (thres / 120);
      if ( value > 0xff)
	value = 0xff;
      SetShort( 0x0080, value);
    }
  return;
}
//////////////////////////////////////////////////////////////////////////
UShort_t TCaen1465::GetHighHexThreshold()
{
  // Check what range-mode the module is in
  // high range : 0xff <=> 3840 counts, low range : 0x20 <=> 3840 counts
  UShort_t mode = 0x0180 & GetShort( 0x102);

  if ( mode == 0x0100)  // high range
    return 15 * (0x00ff & GetShort( 0x0080));
  if ( mode == 0x0180)  // low range  
    return 120 * (0x00ff & GetShort( 0x0080));
  else
    {
      printf("module is in auto-mode\n");
      return 0;
    }
}
//////////////////////////////////////////////////////////////////////////
void TCaen1465::SetLowHexThreshold( UShort_t thres)
{
  // Check what range-mode the module is in
  // high range : 0xff <=> 3840 counts, low range : 0x20 <=> 3840 counts
  UShort_t mode = 0x0180 & GetShort( 0x102);
  UShort_t value;

  if ( mode == 0x0100)  // high range
    {
      value = (thres / 15);
      if ( value > 0xff)
	value = 0xff;
      SetShort( 0x0082, value);
      printf("value is %d\n", value);
    }
  if ( mode == 0x0180)  // low range  
    {
      value = (thres / 120);
      if ( value > 0xff)
	value = 0xff;
      SetShort( 0x0082, value);
    }
  return;
}
//////////////////////////////////////////////////////////////////////////
UShort_t TCaen1465::GetLowHexThreshold()
{
  // Check what range-mode the module is in
  // high range : 0xff <=> 3840 counts, low range : 0x20 <=> 3840 counts
  UShort_t mode = 0x0180 & GetShort( 0x102);

  if ( mode == 0x0100)  // high range
    return 15 * (0x00ff & GetShort( 0x0082));
  if ( mode == 0x0180)  // low range  
    return 120 * (0x00ff & GetShort( 0x0082));
  else
    {
      printf("module is in auto-mode\n");
      return 0;
    }
}
//////////////////////////////////////////////////////////////////////////
UShort_t TCaen1465::IsHeader( UInt_t buffer)
{
  if (buffer & 0x80000000) 
    return 1;
  else 
    return 0;
}
//////////////////////////////////////////////////////////////////////////
UShort_t TCaen1465::IsDatum( UInt_t buffer)
{
  if (!(buffer & 0x80000000) && !(buffer & 0x40000000)) 
    return 1;
  else 
    return 0;
}
//////////////////////////////////////////////////////////////////////////
UShort_t TCaen1465::IsEOB( UInt_t buffer)
{
  if (buffer & 0x40000000) 
    return 1;
  else 
    return 0;
}
//////////////////////////////////////////////////////////////////////////
UShort_t TCaen1465::GetMultiplicity( UInt_t buffer)
{
  UShort_t mult;

  if ( !IsHeader( buffer))
    {
      errormessage("Not a header");
      exit(-1);
    }
  else
    {
      mult = 0x003f & (buffer >> 16);
      if ( mult == 0)
	mult = 64;
      return mult;
    }
}
//////////////////////////////////////////////////////////////////////////
UShort_t TCaen1465::GetChannel( UInt_t buffer)
{
  return ( (buffer >> 16) & 0x3f);
}
//////////////////////////////////////////////////////////////////////////
UShort_t TCaen1465::GetValue( UInt_t buffer)
{
  return ( buffer & 0xfff);
}
//////////////////////////////////////////////////////////////////////////
UShort_t TCaen1465::GetData( UShort_t *ch, UShort_t *val)
{
  UInt_t buffer;
  UShort_t mult, i, address;

  // Check that the modeule is not busy
  if ( Busy())
    {
      errormessage("module busy");
      exit(-1);
    }

  buffer = GetLong( 0x8000);
  if ( debug > 1)
    {
      printf("HEADER : ");
      bitprint( buffer);
    }

  // Check that there is data
  if ( ! DataReady())
    {
      errormessage("no data ready");
      return 0;;
    }



  mult = GetMultiplicity( buffer);  // will exit if not a header

  address = 0x8000;
  for ( i = 0; i < mult; i++)
    {
      address = address + 4;
      buffer = GetLong( address);
      if ( debug > 1)
	{
	  printf("DATUM : ");
	  bitprint( buffer);
	}

      if ( ! IsDatum( buffer))
	{
	  errormessage("not a datum");
	  exit(-1);
	}
      *ch = GetChannel( buffer);
      *val = GetValue( buffer);

      if (debug == 1)
	printf("ch : %d     val : %d\n", *ch, *val);

      ch++;
      val++;
    }
  
  // Check for EOB
  address = address + 4;
  buffer = GetLong( address);
  if (debug > 1)
    {
      printf("EOB : ");
      bitprint( buffer);
    }

  if ( ! IsEOB( buffer))
    {
      errormessage("not an EOB");
      exit(-1);
    }

  IncrementPointer(); // should do this before returning     
 
  // Check that there is no more events
  if ( DataReady())
    {
      errormessage("There is still data in buffer");
      exit(-1);
    }
  
  return mult;
}
//////////////////////////////////////////////////////////////////////////
void TCaen1465::Test()
{
  UShort_t *ch = new UShort_t[64];
  UShort_t *val = new UShort_t[64];
  UShort_t mult, sparseFlag = 0;

  // Check if sparse is enabled
  if ( GetShort( 0x0102) & 0x0010) 
    {
      sparseFlag = 1;
      DisableSparse();
    }

  // Empty the buffer
  while ( DataReady())
    IncrementPointer();

  // Set the test gate width to 6.4 us
  SetShort( 0x0016, 0x00ff);

  // Generate an internal test gate
  SetShort( 0x0014, 0x0001);

  // Wait for the data to become ready
  while ( !DataReady())
    ;

  mult = GetData( ch, val);
  if ( mult != 64)
    {
      printf("failed test, exit");
      exit(-1);
    }

  if ( sparseFlag == 1)
    EnableSparse();

  delete ch;
  delete val;

  return;
}
//////////////////////////////////////////////////////////////////////////
void TCaen1465::bitprint( UInt_t buffer)
{
  int i = 31;
  while( i >= 0)
    {
      if ( (buffer >> i) & 0x00000001 )
    	printf("1");
      else
	printf("0");
      if ( (i % 8 == 0) && (i != 0))
	printf("|");
      i--;
    }
  printf("\n");
}
//////////////////////////////////////////////////////////////////////////
