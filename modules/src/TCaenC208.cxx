////////////////////////////////////////////////////////////////////////////
// 
// Caen C208 (CAMAC)
// 16 channel programmable CFD
// John Annand 19/02/14 Modify for TSIS1100 interface class
// 
///////////////////////////////////////////////////////////////////////////

#include "TCaenC208.h"

ClassImp( TCaenC208 )

const char description[] = "Caen C208 Discriminator";  


TCaenC208::TCaenC208(TCamacBranch *cbd, int c, int n): 
fCBD( cbd ), fCrate(c), fSlot(n)
{
  InitUnit();
  WriteInhibit( 0x0);   
  WriteThresholds( 0x0005);   // Set as low as possible
}

TCaenC208::TCaenC208(TCamacCrate *C, int n):
fCBD( C->GetCBD() ), fCrate( C->Crate() ), fSlot(n)
{
  InitUnit();
  WriteInhibit( 0x0);     
  WriteThresholds( 0x0005);   // Set as low as possible
}

TCaenC208::~TCaenC208( )
{
}

void TCaenC208::InitUnit()
{
  if( ! fCBD->Online( fCrate ) )
    {
      printf("Camac module %s in crate %d slot %d: file %s line %d\n", 
	     description, fCrate, fSlot, __FILE__, __LINE__);            
      fprintf( stderr, "Warning: Crate is not online\n");
    }

  fReadInhibit = fCBD->cnaf( fCrate, fSlot, 0, 1);
  fReadOutputWidth = fCBD->cnaf( fCrate, fSlot, 1, 1);
  fReadDeadtime = fCBD->cnaf( fCrate, fSlot, 2, 1);
  fReadMajorityThreshold = fCBD->cnaf( fCrate, fSlot, 3, 1);

  fWriteInhibit = fCBD->cnaf( fCrate, fSlot, 0, 17);
  fWriteOutputWidth = fCBD->cnaf( fCrate, fSlot, 1, 17);
  fWriteDeadtime = fCBD->cnaf( fCrate, fSlot, 2, 17);
  fWriteMajorityThreshold = fCBD->cnaf( fCrate, fSlot, 3, 17);

  for( int i = 0; i < fNoChannels ; i++)
    {
      fReadThreshold[i] = fCBD->cnaf( fCrate, fSlot, i, 0);  
      fWriteThreshold[i] = fCBD->cnaf( fCrate, fSlot, i, 16);  
    }

  return;
}


void TCaenC208::WriteThresholds( UShort_t val)
{
  for ( UShort_t i = 0; i < fNoChannels; i++)
    WriteThreshold( i, val);
  return;
}

void TCaenC208::PrintThresholds()
{
  for ( UShort_t i = 0; i < fNoChannels; i++)
    fprintf( stderr, "ch %d \t threshold %u\n", i, ReadThreshold(i));

  return;
}

