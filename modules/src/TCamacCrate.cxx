/////////////////////////////////////////////////////
// 
// Camac crate class
//
// Dahn Nilsson 1999-03-02
// John Annand 19/02/14 Modify for TSIS1100 interface class
////////////////////////////////////////////////////

#include "TCamacCrate.h"

ClassImp( TCamacCrate )

TCamacCrate::TCamacCrate(TCamacBranch *cbd, const int c): fCrate(c), fCBD(cbd)
{
  if( ! fCBD->Online( fCrate ) )
    fprintf( stderr, "Warning: Crate %d is not online!\n", fCrate);

  fClear = fCBD->cnaf( fCrate, 28, 9, 26);
  fReset = fCBD->cnaf( fCrate, 28, 8, 26);
  fSetInhibit = fCBD->cnaf( fCrate, 30, 9, 26);
  fClearInhibit = fCBD->cnaf( fCrate, 30, 9, 24);
  fTestInhibit = fCBD->cnaf( fCrate, 30, 9, 27);
}

TCamacCrate::~TCamacCrate( )
{
}

Bool_t TCamacCrate::Inhibit( Bool_t b )
{
  UShort_t dum;
  if( b )
    dum = fCBD->Read(fSetInhibit);
  else
    dum = fCBD->Read(fClearInhibit);
  dum = fCBD->Read(fTestInhibit);          // Return True if inhibit set

  return fCBD->Q();
}

Bool_t TCamacCrate::Inhibit()
{
  volatile Short_t dum;

  dum = fCBD->Read(fTestInhibit);

  return fCBD->Q();
}

