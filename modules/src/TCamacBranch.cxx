////////////////////////////////////////////////////////////
//
// Implementation of the class TCamacBranch.
//
// Dahn Nilsson 1999-03-02
// Magnus Lundin 2004-05-27 minor modifications
// Magnus Lundin 2006-10-22 polling is optional, __GNUC__ is 3 on old and 4 on new machines 
// John Annand 19/02/14 Modify for TSIS1100 interface class
//
///////////////////////////////////////////////////////////

//--------------------------------------------------------
// TCamacBranch
//
// This class implements the CAMAC branch handler.
// It also opens the /dev/vmedrv device and mmaps the
// CAMAC adress space.

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <fcntl.h>

#include "TSIS1100.h"
#include "TCamacBranch.h"

#define IRQ 2
#define IRQ_VECTOR 0x007c;
#define IRQ_TIMEOUT_SEC 5;

ClassImp( TCamacBranch )

static int init = 0;
//struct vmedrv_interrupt_property_t interrupt_property;

///////////////////////////////////////////////////////////////////
TCamacBranch::TCamacBranch(TSIS1100* sis, int b)
{
  // Create a new branch with number b.
  if( init++ )
    return;
  fSIS = sis;
  fbase = CBDBRANCH(b);
  fcsr = cnaf(0, 29, 0, 0);
  fbtb = cnaf( 0, 29, 0, 9);
  fifr = cnaf( 0, 29, 0, 4);

#if __GNUC__ == 3
  if ( fInitInterrupt() != 0)
    {
      perror("fInitInterrupt");
      exit(1);
    }
#endif

}
///////////////////////////////////////////////////////////////////
int TCamacBranch::fInitInterrupt()     // enable interrupts
{
  //
  // Do some ioctls and set up some registers on the 616 
  // Need 16 bit access ?
  //
  /*
  if (( fdevmem16 = open( memdev16, O_RDWR)) == -1) 
    {
      perror("error open memdev16");
      exit(1);
    }

  interrupt_property.irq = IRQ;
  interrupt_property.vector = IRQ_VECTOR;
  interrupt_property.signal_id = 0;
  if (ioctl(fdevmem16, VMEDRV_IOC_REGISTER_INTERRUPT, &interrupt_property) == -1) 
    {
      printf("Error in %s at %d\n", __FILE__, __LINE__);
      //fprintf(stderr, "ERROR: ioctl(REGISTER_INTERRUPT): %s\n", strerror(errno));
      exit(EXIT_FAILURE);
    }
  
  if (ioctl(fdevmem16, VMEDRV_IOC_ENABLE_INTERRUPT) == -1) {
    printf("Error in %s at %d\n", __FILE__, __LINE__);
    //    fprintf(stderr, "ERROR: ioctl(ENABLE_INTERRUPT): %s\n", strerror(errno));
    exit(EXIT_FAILURE);
  }
  interrupt_property.timeout = IRQ_TIMEOUT_SEC;

  // mask external interrupts
  SetCsr( *fcsr & 0xfff0);

  // reset pending external interrupts
  SetIfr( 0);  
  */
  return 0;
}
///////////////////////////////////////////////////////////////////
int TCamacBranch::fWait()
{
  int result = 0;
  /*
  result = ioctl(fdevmem16, VMEDRV_IOC_WAIT_FOR_INTERRUPT, &interrupt_property);
  if (result > 0) {
    // reset pending external interrupts
    SetIfr(0);  
    return result;
  }
  else if (errno == ETIMEDOUT) {
    printf("timed out.\n");
    result = -1;
  }
  else if (errno == EINTR) {
    printf("interrupted.\n");
    result = -1;
  }
  else {
    printf("Error in %s at %d\n", __FILE__, __LINE__);
    //    fprintf(stderr, "ERROR: ioctl(WAIT_FOR_INTERRUPT): %s\n", strerror(errno));
    exit(EXIT_FAILURE);
  }
  
  // reset pending external interrupts
  SetIfr(0);  
  */
  return result;
}
///////////////////////////////////////////////////////////////////
int TCamacBranch::fCleanUpInterrupt()
{
  /*
  if (ioctl(fdevmem16, VMEDRV_IOC_DISABLE_INTERRUPT) == -1) {
    printf("Error in %s at %d\n", __FILE__, __LINE__);
    //    fprintf(stderr, "ERROR: ioctl(DISABLE_INTERRUPT): %s\n", strerror(errno));
    exit(EXIT_FAILURE);
  }
  
  if (ioctl(fdevmem16, VMEDRV_IOC_UNREGISTER_INTERRUPT, &interrupt_property) == -1) {
    printf("Error in %s at %d\n", __FILE__, __LINE__);
    //    fprintf(stderr, "ERROR: ioctl(UNREGISTER_INTERRUPT): %s\n", strerror(errno));
    exit(EXIT_FAILURE);
  }
  
  close(fdevmem16);
  */  
  return 0;
}
///////////////////////////////////////////////////////////////////
// Waits for an interrupt (IT2) on the CBD
int TCamacBranch::Wait()   
{ 
  return fWait(); 
}
///////////////////////////////////////////////////////////////////
// Polls for a signal on IT2
Short_t TCamacBranch::Poll( UShort_t secs)   
{ 
  time_t start, now;
  Short_t ret = 1;

  // Mask (disable) external interrupts
  SetCsr( Read(fcsr) & 0xfff3);

  start = time(NULL);
  now = start;

  // Poll 
  while( (ret = !(Read(fcsr) & 0x0002)) 
         && (difftime(now, start) < (double)secs) ) 
    now = time(NULL); 

  // Reset
  if ( ret == 0)
    SetIfr( 0x0000);

  return ret; 
}
///////////////////////////////////////////////////////////////////
// Output pulse on ack4
Short_t TCamacBranch::ToggleAck4()   
{
  fSIS->WA24D16(fifr,1);
  fSIS->WA24D16(fifr,0);
  return 0;
}              
///////////////////////////////////////////////////////////////////
TCamacBranch::~TCamacBranch( )
{
  if ( --init )
    return;

#if __GNUC__ == 3
  fCleanUpInterrupt();  
#endif
}
///////////////////////////////////////////////////////////////////
void TCamacBranch::Print()
{
  Short_t CSR = Read(fcsr);
  Short_t BTB = Read(fbtb);

  printf("\n");

  if( CSR & (1 << 15))
    printf( "Q response valid\n");
  else
    printf( "Q response NOT valid\n");

  if( CSR & (1 << 14))
    printf( "X response valid\n");
  else
    printf( "X response NOT valid\n");

  if( CSR & (1 << 13))
    printf( "TO: Time out during last CAMAC cycle\n");


  if( CSR & (1 << 11))
    printf( "MNOX: No X gives DTACK\n");
  else
    printf( "MNOX: No X gives bus error\n");

  if( CSR & (1 << 5))
    printf( "MTO: No time out on board\n");
  else
    printf( "MTO: Time out active\n");

  if( CSR & (1 << 4))
    printf( "MLAM: LAM interrupts are not masked\n");
  else
    printf( "MLAM: LAM interrupts are masked\n");

  if( CSR & (1 << 3))
    printf( "MIT2: IRQ2 interrupts are not masked\n");
  else
    printf( "MIT2: IRQ2 interrupts are masked\n");


  if( CSR & (1 << 2))
    printf( "MIT4: IRQ4 interrupts are not masked\n");
  else
    printf( "MIT4: IRQ4 interrupts are masked\n");

  if( CSR & (1 << 1))
    printf( "IT2: IRQ2 interrupt is active\n");

  if( CSR & 0x1 )
    printf( "IT4: IRQ4 interrupt is active\n");

  printf("\n");

  for( int i = 1; i < 7; i++)
    if( BTB & ( 1<<i ) )
      printf("Crate %d is online\n", i);
}

UShort_t TCamacBranch::Read(UInt_t addr) {
  return fSIS->RA24D16(addr);
}
void TCamacBranch::Write(UInt_t addr, UShort_t val){
  fSIS->WA24D16(addr,val);
}







