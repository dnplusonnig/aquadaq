
#include "TSilena4418V.h"

ClassImp( TSilena4418V )

TSilena4418V::TSilena4418V(TCamacBranch *cbd, int c, int n): 
fCBD( cbd ), fCrate(c), fSlot(n)
{
  InitUnit();
}

TSilena4418V::TSilena4418V(TCamacCrate *C, int n):
fCBD( C->GetCBD() ), fCrate( C->Crate() ), fSlot(n)
{
  InitUnit();
}

TSilena4418V::~TSilena4418V( )
{
}

void TSilena4418V::InitUnit()
{
  if( ! fCBD->Online( fCrate ) )
    fprintf( stderr, "Warning: Crate %d is not online!\n", fCrate);

  fClear = (volatile Short_t *)fCBD->cnaf( fCrate, fSlot, 0, 9);
  fTest = (volatile Short_t *)fCBD->cnaf( fCrate, fSlot, 0, 25);

  for( int i = 0; i < fNoChannels ; i++)
    {
      pRead[i] = (volatile Short_t *)fCBD->cnaf( fCrate, fSlot, i, 0);
      pWrite[i] = (volatile Short_t *)fCBD->cnaf( fCrate, fSlot, i, 17);
    }

  WriteStatus( 0x0a00 );
}

Int_t TSilena4418V::ReadArray( Short_t *Array, Int_t count)
{
  // Read an array of count values into Array
  // from channel starting on 0.
  
  int i;

  for( i = 0; i < count; i++)
    *Array++ = *(Short_t *)pRead[i];

  return i;
}

void TSilena4418V::Test(int l)
{
  volatile Short_t dum;

  dum = *fTest;
}

void TSilena4418V::Print( )
{
  for( int i = 0; i < fNoChannels; i++)
    printf( "Channel: %d, value: %d\n", i, operator[](i));
}
