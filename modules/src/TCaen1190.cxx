///////////////////////////////////////////////////////////////////
//
// Caen 1190 64 channel multihit TDC
// Magnus Lundin 2008-06-02
// John Annand 19/02/14 Modify for TSIS1100 interface class
//
///////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <fcntl.h>

#include "TSIS1100.h"
#include "TCaen1190.h"

ClassImp( TCaen1190 )

const char description[] = "Caen 1190 Multihit TDC";
const UShort_t CAEN1190SIZE = 0xffff;

/////////////////////////////////////////////////////////////////////////
TCaen1190::TCaen1190( TSIS1100* sis, UInt_t BASE, UShort_t threshold, UShort_t mult,
		      UShort_t debugflag )
{
  fSIS = sis;
  hwaddress = BASE;
  multLimit = mult;
  debug = debugflag;      
  fbase = BASE;

  Test();

  SoftwareClear();

  Info();
  PrintStatus();

  //
  // Set trigger match mode
  //
  SetOpCode( 0x0, 0);

  SetOpCode( 0x2, 0);
  if ( GetOpCode() != 1)
    {
      printf("Module %s at 0x%x : file %s line %d\n", description, hwaddress, __FILE__, __LINE__);
      printf("Unable to set trigger match mode\n");
      exit(-1);
    }


  //
  // Set window width
  //
  UShort_t windowWidth = 0x30; // 0x30 <=> 1200 ns
  SetOpCode( 0x10, 0);
  SetOpCode( 0x0, windowWidth);  

  //
  // Set window offset (to 0)
  // (0xffb)
  UShort_t windowOffset = 0xffb;
  SetOpCode( 0x11, 0);
  SetOpCode( 0x0, windowOffset);

  //
  // Set extra search margin (to 8)
  //
  UShort_t searchMargin = 0x8;
  SetOpCode( 0x12, 0);
  SetOpCode( 0x0, searchMargin);

  //
  // Set reject margin (to 1)
  //
  UShort_t rejectMargin = 0x1;
  SetOpCode( 0x13, 0);
  SetOpCode( 0x0, rejectMargin);

  //
  // Enable subtraction of trigger time (we measure relative to the trigger!)
  //
  SetOpCode( 0x14, 0);

  //
  // Check the trigger configuration
  //
  SetOpCode( 0x16, 0);

  if ( GetOpCode() != windowWidth) // window width
    {
      printf("Module %s at 0x%x : file %s line %d\n", description, hwaddress, __FILE__, __LINE__);
      printf("Unable to set trigger window width\n");
      exit(-1);
    }
  if ( GetOpCode() != windowOffset) // window offset
    {
      printf("Module %s at 0x%x : file %s line %d\n", description, hwaddress, __FILE__, __LINE__);
      printf("Unable to set trigger window offset\n");
      exit(-1);
    }
  if ( GetOpCode() != searchMargin) // search margin
    {
      printf("Module %s at 0x%x : file %s line %d\n", description, hwaddress, __FILE__, __LINE__);
      printf("Unable to set trigger search margin\n");
      exit(-1);
    }
  if ( GetOpCode() != rejectMargin) // reject margin
    {
      printf("Module %s at 0x%x : file %s line %d\n", description, hwaddress, __FILE__, __LINE__);
      printf("Unable to set trigger window reject margin\n");
      exit(-1);
    }
  if ( GetOpCode() != 1) // trigger time subtraction
    {
      printf("Module %s at 0x%x : file %s line %d\n", description, hwaddress, __FILE__, __LINE__);
      printf("Unable to set trigger time subtraction\n");
      exit(-1);
    }

  //
  // Time is defined by the leading edge of the stops
  // 
  UShort_t detectionConfiguration = 0x2; // leading edge

  SetOpCode( 0x22, 0);
  SetOpCode( 0x0, detectionConfiguration); 

  SetOpCode( 0x23, 0);
  if ( GetOpCode() != detectionConfiguration) // detection config
    {
      printf("Module %s at 0x%x : file %s line %d\n", description, hwaddress, __FILE__, __LINE__);
      printf("Unable to set detection configuration\n");
      exit(-1);
    }

  //
  // The time resolution
  // 
  UShort_t timeResolution = 0x2; // 0x2 100 ps, 0x1 200 ps

  SetOpCode( 0x24, 0);
  SetOpCode( 0x0, timeResolution); 

  SetOpCode( 0x26, 0);
  if ( GetOpCode() != timeResolution) // time resolution
    {
      printf("Module %s at 0x%x : file %s line %d\n", description, hwaddress, __FILE__, __LINE__);
      printf("Unable to set time resolution\n");
      exit(-1);
    }

  //
  // The dubble pulse resolution
  // 
  UShort_t dubblePulseRes = 0x0; // 5 ns

  SetOpCode( 0x28, 0);
  SetOpCode( 0x0, dubblePulseRes); 

  SetOpCode( 0x29, 0);
  if ( GetOpCode() != dubblePulseRes) // dubble pulse res
    {
      printf("Module %s at 0x%x : file %s line %d\n", description, hwaddress, __FILE__, __LINE__);
      printf("Unable to set dubble pulse resolution\n");
      exit(-1);
    }

  //
  // Limit the number of stops (per module, not per channel)
  // 
  UShort_t stops = 0x9; // no limit (8: 128)

  SetOpCode( 0x33, 0);
  SetOpCode( 0x0, stops); 

  SetOpCode( 0x34, 0);
  if ( GetOpCode() != stops) // 
    {
      printf("Module %s at 0x%x : file %s line %d\n", description, hwaddress, __FILE__, __LINE__);
      printf("Unable to set max number of stops\n");
      exit(-1);
    }

  //
  // Disable tdc header and trailer
  //
  SetOpCode( 0x31, 0);

  SetOpCode( 0x32, 0);
  if ( GetOpCode() != 0) // disable header and trailer
    {
      printf("Module %s at 0x%x : file %s line %d\n", description, hwaddress, __FILE__, __LINE__);
      printf("Unable to disable header and trailer\n");
      exit(-1);
    }

  //
  // Enable tdc error mark
  //
  SetOpCode( 0x35, 0);

  //
  // Enable all channels
  //
  SetOpCode( 0x42, 0);

  return;
}
/////////////////////////////////////////////////////////////////////////
TCaen1190::~TCaen1190( )
{
}
/////////////////////////////////////////////////////////////////////////
void TCaen1190::SetShort( UShort_t address, UShort_t value)
{
  fSIS->WA32D16(fbase+address,value ) ;
  return;
}
/////////////////////////////////////////////////////////////////////////
UShort_t TCaen1190::GetShort( UShort_t address)
{
  return fSIS->RA32D16(fbase+address) ;
}
/////////////////////////////////////////////////////////////////////////
void TCaen1190::SetLong( UShort_t address, UInt_t value)
{
  fSIS->WA32D32(fbase+address,value) ;
}
/////////////////////////////////////////////////////////////////////////
UInt_t TCaen1190::GetLong( UShort_t address)
{
  return fSIS->RA32D32(fbase+address) ;
}
/////////////////////////////////////////////////////////////////////////
Short_t TCaen1190::SetOpCode( UShort_t com, UShort_t obj)
{
  int i = 0;
  while ( GetShort( 0x1030) != 1 && i++ < 500)
    usleep(1000);

  // Check that it is ok to write
  if ( GetShort( 0x1030) != 1)
    {
      printf("Module %s at 0x%x : file %s line %d\n", description, hwaddress, __FILE__, __LINE__);
      printf("Not ok to set opcode\n");
      return -1;
    }

  UShort_t val = (com << 8) + obj;

  SetShort( 0x102e, val);

  return 0;
}
/////////////////////////////////////////////////////////////////////////
Short_t TCaen1190::GetOpCode()
{
  int i = 0;
  while ( GetShort( 0x1030) != 2 && i++ < 500)
    usleep(1000);

  // Check that it is ok to read
  if ( GetShort( 0x1030) != 2)
    {
      printf("Module %s at 0x%x : file %s line %d\n", description, hwaddress, __FILE__, __LINE__);
      printf("Not ok to get opcode\n");
      return -1;
    }

  return GetShort( 0x102e);
}
//////////////////////////////////////////////////////////////////////////
void TCaen1190::SetWindowWidthNS( Double_t wwns)
{
  UShort_t ww = (UShort_t)wwns/25;

  printf("wwns %f 0x%x\n", wwns, ww);

  SetOpCode( 0x10, 0);
  SetOpCode( 0x0, ww);  

  return;
}
//////////////////////////////////////////////////////////////////////////
void TCaen1190::SetWindowOffsetNS( Double_t wons)
{
  UShort_t neg = 0;
  UShort_t wo;

  if ( wons < 0)
    {
      neg = 1;
      wons = -1.0 * wons;
    }

  wo = (UShort_t)wons/25;
  wo = 0x0fff & (~wo);
  if ( neg)
    wo = wo | 0x800;

  printf("wons %f 0x%x\n", wons, wo);

  SetOpCode( 0x11, 0);
  SetOpCode( 0x0, wo);

  return;
}
//////////////////////////////////////////////////////////////////////////
UShort_t TCaen1190::GetData( UShort_t *ch, UInt_t *val)
{
  UInt_t buffer;
  UShort_t mult = 0;

  // Check that there is data
  if ( ! DataReady())
    {
      if ( debug > 1)
	{
	  printf("Module %s at 0x%x : file %s line %d\n", description, hwaddress, __FILE__, __LINE__);
	  printf("No data ready\n");
	}
      return 0;
    }
  buffer = fSIS->RA32D32(fbase);
  //buffer = *fbase3232;
  if ( debug > 1)
    {
      printf("HEADER : ");
      bitprint( buffer);
    }

  // Check for header signature
  if ( ((buffer >> 27) & 0x001f) != 0x0008)
    {
      printf("Module %s at 0x%x : file %s line %d\n", description, hwaddress, __FILE__, __LINE__);
      printf("Not a header\n");
      return 0;
    }

  // The actual data and finally the trailer
  //while (  (mult < multLimit*64) && (buffer = *fbase3232) )
  while (  (mult < multLimit*64)  )
    {
      buffer = fSIS->RA32D32(fbase) ;
      if( !buffer ) break;
      if ( debug > 1)
	{
	  printf("DATA/TRAILER : ");
	  bitprint( buffer);
	}
      
      // data
      if ( ((buffer >> 27) & 0x001f) == 0x0000)
	{
	  mult++;
	  *val++ = buffer & 0x0007ffff;
	  *ch++ = (buffer >> 19) & 0x007f;
	}
      // trailer
      else if ( ((buffer >> 27) & 0x001f) == 0x0010)
	break;
      else
	{
	  printf("Module %s at 0x%x : file %s line %d\n", description, hwaddress, __FILE__, __LINE__);
	  bitprint( buffer);
	  printf("No data or trailer found\n");
	  break;
	}
    }

  // Check if we hit the limit
  if ( mult >= multLimit*64)
    {
      // flush data until trailer 
      buffer = fSIS->RA32D32(fbase);
      //buffer = *fbase3232;
      while (((buffer >> 27) & 0x001f) != 0x0010)
	{
	  buffer = fSIS->RA32D32(fbase);
	  printf(".");
	}
      if (((buffer >> 27) & 0x001f) != 0x0010)
	{
	  printf("Module %s at 0x%x : file %s line %d\n", description, hwaddress, __FILE__, __LINE__);
	  bitprint( buffer);
	  printf("Exceeded mult limit and no trailer found\n");
	}
    }

  // Check that there is no more events
  if ( DataReady())
    {
      printf("Module %s at 0x%x : file %s line %d\n", description, hwaddress, __FILE__, __LINE__);
      printf("There is still data in buffer, reset data and continue\n");
      while( DataReady())
	{
	  buffer = fSIS->RA32D32(fbase);
	  bitprint( buffer);
	}

      SoftwareClear();
    }
  
  return mult;
}
//////////////////////////////////////////////////////////////////////////
void TCaen1190::Info()
{
  printf("firmware : 0x%x\n", 0x00ff & GetShort( 0x1026));  
  
  printf("geo : 0x%x\n", 0x001f & GetShort( 0x100e));  

  printf("interrupt level: 0x%x\n", 0x0007 & GetShort( 0x100a));  

  printf("interrupt vector: 0x%x\n", 0x00ff & GetShort( 0x100c));  

  printf("event counter: 0x%x\n", 0xffff & GetLong( 0x101c));  

  return;
}
//////////////////////////////////////////////////////////////////////////
void TCaen1190::Test()
{
  // Set the testreg  
  UInt_t wpattern = 0xffff;
  SetLong( 0x1028, wpattern);
  UInt_t rpattern = GetLong( 0x1028);
  if ( rpattern != wpattern)
    {
      printf("Module %s at 0x%x : file %s line %d\n", description, hwaddress, __FILE__, __LINE__);
      printf("Test failed. Wrote: 0x%x Read: 0x%x\n", wpattern, rpattern );
      exit(-1);
    }

  wpattern = 0x0000;
  SetLong( 0x1028, wpattern);
  rpattern = GetLong( 0x1028);
  if ( rpattern != wpattern)
    {
      printf("Module %s at 0x%x : file %s line %d\n", description, hwaddress, __FILE__, __LINE__);
      printf("Test failed. Wrote: 0x%x Read: 0x%x\n", wpattern, rpattern );
      exit(-1);
    }

  return;
}
//////////////////////////////////////////////////////////////////////////
void TCaen1190::PrintStatus()
{
  UShort_t control, status;

  control = 0x01ff & GetShort( 0x1000);
  printf("control 0x%x\n", control);

  printf("control: BERR EN                      %d\n", control & 0x0001);
  printf("control: TERM                         %d\n", (control >> 1) & 0x0001);
  printf("control: TERM SW                      %d\n", (control >> 2) & 0x0001);
  printf("control: EMPTY EVENT                  %d\n", (control >> 3) & 0x0001);
  printf("control: ALIGN 64                     %d\n", (control >> 4) & 0x0001);
  printf("control: COMP TABLE                   %d\n", (control >> 5) & 0x0001);
  printf("control: TEST FIFO ENABLE             %d\n", (control >> 6) & 0x0001);
  printf("control: READ COMP SRAM ENABLE        %d\n", (control >> 7) & 0x0001);
  printf("control: EVENT FIFO ENABLE            %d\n", (control >> 8) & 0x0001);
  printf("control: EXTD TRIGGER TIME TAG ENABLE %d\n", (control >> 9) & 0x0001);


  status = 0xffff & GetShort( 0x1002);
  printf("status 0x%x\n", status);

  printf("status: DATA_READY                    %d\n", status & 0x0001);
  printf("status: ALM_FULL                      %d\n", (status >> 1) & 0x0001);
  printf("status: FULL                          %d\n", (status >> 2) & 0x0001);
  printf("status: TRG MATCH                     %d\n", (status >> 3) & 0x0001);
  printf("status: HEADER EN                     %d\n", (status >> 4) & 0x0001);
  printf("status: TERM ON                       %d\n", (status >> 5) & 0x0001);
  printf("status: ERROR 0                       %d\n", (status >> 6) & 0x0001);
  printf("status: ERROR 1                       %d\n", (status >> 7) & 0x0001);
  printf("status: ERROR 2                       %d\n", (status >> 8) & 0x0001);
  printf("status: ERROR 3                       %d\n", (status >> 9) & 0x0001);
  printf("status: BERR FLAG                     %d\n", (status >> 10) & 0x0001);
  printf("status: PURGED                        %d\n", (status >> 11) & 0x0001);
  printf("status: RES_0                         %d\n", (status >> 12) & 0x0001);
  printf("status: RES_1                         %d\n", (status >> 13) & 0x0001);
  printf("status: PAIR MODE                     %d\n", (status >> 14) & 0x0001);
  printf("status: TRIGGER LOST                  %d\n", (status >> 15) & 0x0001);
  return;
}
/////////////////////////////////////////////////////////////////////////
void TCaen1190::bitprint( UInt_t buffer)
{
  int i = 31;
  while( i >= 0)
    {
      if ( (buffer >> i) & 0x00000001 )
    	printf("1");
      else
	printf("0");
      if ( (i % 8 == 0) && (i != 0))
	printf("|");
      i--;
    }
  printf("\n");
}
//////////////////////////////////////////////////////////////////////////


