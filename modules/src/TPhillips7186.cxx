
#include "TPhillips7186.h"

#define DUMMYWRITE 99

ClassImp( TPhillips7186 );

TPhillips7186::TPhillips7186(TCamacBranch *cbd, int c, int n):
fCBD( cbd ), fCrate(c), fSlot(n)
{

  if( ! cbd->Online( fCrate ) )
    fprintf( stderr, "Warning: Crate %d is not online!\n", fCrate);

  fNoChannels = 16;

  fClear = (volatile Short_t *)cbd->cnaf( fCrate, fSlot, 0, 9);
  fTest = (volatile Short_t *)cbd->cnaf( fCrate, fSlot, 0, 25);
  fHit = (volatile Short_t *)cbd->cnaf( fCrate, fSlot, 1, 6);
  fSparse = (volatile Short_t *)cbd->cnaf( fCrate, fSlot, 0, 4);

  for( int i = 0; i < fNoChannels ; i++)
    {
      pRead[i] = (volatile Short_t *)cbd->cnaf( fCrate, fSlot, i, 0);
    }
  Reset();
}

TPhillips7186::TPhillips7186(TCamacCrate *C, int n):
fCBD( C->GetCBD() ), fCrate( C->Crate() ), fSlot(n)
{

  if( ! fCBD->Online( fCrate ) )
    fprintf( stderr, "Warning: Crate %d is not online!\n", fCrate);

  fNoChannels = 16;

  fClear = (volatile Short_t *)fCBD->cnaf( fCrate, fSlot, 0, 9);
  fTest = (volatile Short_t *)fCBD->cnaf( fCrate, fSlot, 0, 25);
  fHit = (volatile Short_t *)fCBD->cnaf( fCrate, fSlot, 1, 6);
  fSparse = (volatile Short_t *)fCBD->cnaf( fCrate, fSlot, 0, 4);

  for( int i = 0; i < fNoChannels ; i++)
    {
      pRead[i] = (volatile Short_t *)fCBD->cnaf( fCrate, fSlot, i, 0);
    }
  Reset();
}

TPhillips7186::~TPhillips7186( )
{
}

Int_t TPhillips7186::ReadArray( Short_t *Array, Int_t count)
{
  // Read an array of count values into Array
  // from channel starting on 0.
  
  int i;

  for( i = 0; i < count; i++)
    *Array++ = *(Short_t *)pRead[i];

  return i;
}

Int_t TPhillips7186::ReadSparseArray( UShort_t *Array)
{
  // Read a sparse array of count values into Array
  // from channel starting on 0.
  
  int i = 0;

  *Array++ = *(UShort_t *)fSparse;
  while( Q() && i < fNoChannels )
    {
      *Array++ = *(UShort_t *)fSparse;
      i++;
    }

  return i;
}

Bool_t TPhillips7186::SetPedestal( int ch, int ped)
// Set pedestal for channel ch
{
  
  if( ped < -4096 || ped > 4095 )
    {
      fprintf( stderr, "Pedestal out of range: %d, crate %d, slot %d, channel %d\n",
	       ped, fCrate, fSlot, ch);
      return 0;
    }

  *func( 17, 0) = DUMMYWRITE;
  
  *func( 20, ch) = (ped < 0 ? 0x2000 + ped : ped);

  return Q();
}

Bool_t TPhillips7186::SetLowerThreshold( int ch, int tr)
// Set lower threshold for channel ch
{
  if( tr < 0 || tr > 0xfff )
    {
      fprintf( stderr, "Threshold out of range: %d, crate %d, slot %d, channel %d\n",
	       tr, fCrate, fSlot, ch);
      return 0;
    }

  *func( 17, 1) = DUMMYWRITE;
  
  *func( 20, ch) = tr;

  return Q();
}

Bool_t TPhillips7186::SetUpperThreshold( int ch, int tr)
// Set upper threshold for channel ch
{
  if( tr < 0 || tr > 0xfff )
    {
      fprintf( stderr, "Threshold out of range: %d, crate %d, slot %d, channel %d\n",
	       tr, fCrate, fSlot, ch);
      return 0;
    }

  *func( 17, 2) = DUMMYWRITE;
  
  *func( 20, ch) = tr;

  return Q();
}

Short_t TPhillips7186::GetPedestal( int ch)
// Get pedestal for channel ch
{
  Short_t ped;

  *func( 17, 0) = DUMMYWRITE;
  
  ped = *func( 1, ch);

  return (ped & 0x1000 ? ped - 0x2000 : ped);
}

Short_t TPhillips7186::GetLowerThreshold( int ch)
// Get lower threshold for channel ch
{

   *func( 17, 1) = DUMMYWRITE;
  
  return (0xfff & *func( 1, ch));
}

Short_t TPhillips7186::GetUpperThreshold( int ch)
// Get upper threshold for channel ch
{

  *func( 17, 2) = DUMMYWRITE;
  
  return (0xfff & *func( 1, ch));
}

Bool_t TPhillips7186::EnablePedestal(int b)
// Enable pedestal, b=0 disables
{
  if(  b )
    return WriteFunc( 19, 0, 1);
  else
    return WriteFunc( 23, 0, 1);
}

Bool_t TPhillips7186::EnableLowerThreshold(int b)
// Enable lower threshold, b=0 disables
{
  if(  b )
    return WriteFunc( 19, 0, 2);
  else
    return WriteFunc( 23, 0, 2);
}

Bool_t TPhillips7186::EnableUpperThreshold(int b)
// Enable upper threshold, b=0 disables
{
  if(  b )
    return WriteFunc( 19, 0, 4);
  else
    return WriteFunc( 23, 0, 4);
}


Bool_t TPhillips7186::Reset()
{
  
  for ( int i = 0; i < 4; i++)
    if( *func( 11, i) )
      ;

  if( !Q() )
    {
      if( ! X() )
	{
	  fprintf( stderr, "No X response after reset of Camac module. Not plugged in?\n");
	  fprintf( stderr, "\tCrate %d, Slot %d.\n", fCrate, fSlot);
	}
      else
	{
	  fprintf( stderr, "No Q response after reset of Camac module.\n");
	  fprintf( stderr, "\tCrate %d, Slot %d.\n", fCrate, fSlot);
	}
      return 0;
    }
  else
    return 1;
}

Bool_t TPhillips7186::DigitalTest(int pattern)
{

  if( pattern > 3 || pattern < 0 )
    {
      fprintf( stderr, "Error: TPhillips7186::DigitalTest(pattern) pattern should be between 0 and 3");
      return 0;
    }

  Clear();
  if( *func( 26, 0) )		// Enable LAM
    ;

  *func( 17, 4) = 0;

  if( ! Q() )
    printf("Could not select test registers!\n");

  *func( 20, pattern) = pattern;

  if( ! Q() )
    printf("Could not select test pattern!\n");

  if( *func( 25, 0) )
    ;

  if( *func( 8, 0) )		// Test LAM
    ;

  for( int i = 0; !Q() && i < 50; i++)
    if( *func( 8, 0) )
      ;
  
  if( !Q() )
    {
      fprintf( stderr, "Pattern test Failed! Crate %d, Slot %d\n", fCrate, fSlot);
      fprintf( stderr, "No LAM detected.\n");

      return 0;
    }
  else
    {
      return 1;
    }
}

Bool_t TPhillips7186::ScaleTest(int scale)
{
  
  if( scale > 2 || scale < 1 )
    {
      fprintf( stderr, "Error: TPhillips7186::ScaleTest(scale) scale needs to be 1 or 2");
      return 0;
    }

  Clear();

  if( *func( 25, scale) )
    ;

  return Q();
}

Bool_t TPhillips7186::Test(int l)
{
  Bool_t ret = 1;

  if( !Reset() )
    {
      printf("Reset Failed in Test()\n");
      return 0;
    }
  

  if ( l > 2 )
    printf("Running test level %d, on module in crate %d, slot %d.\n", l, fCrate, fSlot);

  if ( l > 2 )
    printf("Running digital test. Pattern = %d\n", 0);

  if( DigitalTest(0) )
    {
      Short_t dum;

      for( int i = 0; i < fNoChannels; i++ )
	{
	  dum = 0xfff & operator[](i);
	  
	  if( dum != 0x249 )
	    {
	      fprintf( stderr, "Pattern test Failed! Crate %d, Slot %d, Channel %d\n",
		       fCrate, fSlot, i);
	      fprintf( stderr, "Expected 0x249 but read 0x%x\n", dum);
	      ret = 0;
	    }
	}
    }
  else
    ret = 0;

  if ( l > 2 )
    printf("Running digital test. Pattern = %d\n", 1);

  if( DigitalTest(1) )
    {
      for( int i = 0; i < fNoChannels; i++ )
	{
	  Short_t dum;
	  dum = 0xfff & operator[](i);
	  
	  if( dum != 0x492 )
	    {
	      fprintf( stderr, "Pattern test Failed! Crate %d, Slot %d, Channel %d\n",
		       fCrate, fSlot, i);
	      fprintf( stderr, "Expected 0x492 but read 0x%x\n", dum);
	      ret = 0;
	    }
	}
    }
  else
    ret = 0;

  if ( l > 2 )
    printf("Running digital test. Pattern = %d\n", 2);

  if( DigitalTest(2) )
    {
      Short_t dum;

      for( int i = 0; i < fNoChannels; i++ )
	{
	  dum = 0xfff & operator[](i);
	  
	  if( dum != 0x924 )
	    {
	      fprintf( stderr, "Pattern test Failed! Crate %d, Slot %d, Channel %d\n",
		       fCrate, fSlot, i);
	      fprintf( stderr, "Expected 0x924 but read 0x%x\n", dum);
	      ret = 0;
	    }
	}
    }
  else
    ret = 0;

  if ( l > 2 )
    printf("Running digital test. Pattern = %d\n", 3);

  if( DigitalTest(3) )
    {
      Short_t dum;

      for( int i = 0; i < fNoChannels; i++ )
	{
	  dum = 0xfff & operator[](i);
	  
	  if( dum != 0xfff )
	    {
	      fprintf( stderr, "Pattern test Failed! Crate %d, Slot %d, Channel %d\n",
		       fCrate, fSlot, i);
	      fprintf( stderr, "Expected 0xfff but read 0x%x\n", dum);
	      ret = 0;
	    }
	}
    }
  else
    ret = 0;

  return ret;
}

void TPhillips7186::Print( )
{
  Short_t dum;

  for( int i = 0; i < fNoChannels; i++)
    {
      dum = 0xfff & operator[](i);
      printf( "Channel: %d, value: 0x%x (%d)\n", i, dum, dum);
    }
}
