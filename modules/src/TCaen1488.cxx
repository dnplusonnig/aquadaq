///////////////////////////////////////////////////////////////////
//
// Caen 1488 32 channel TDC
// Magnus Lundin 2002-07-19 (loosly based on TStruckDL515)
//
///////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <string.h>

#include "TCaen1488.h"

ClassImp( TCaen1488 )

const char description[] = "Caen 1488 TDC";
const UShort_t TCAEN1488SIZE = 0xffff;	

#if BUSADAPTER == SIS
const char memdev[] = "/dev/sis110032d16";
#elif BUSADAPTER == SBS
const char memdev[] = "/dev/vmedrv32d16";
#endif

//////////////////////////////////////////////////////////////////////////
TCaen1488::TCaen1488( UInt_t BASE, UShort_t debugflag)
{
  Init( BASE);

  debug = debugflag;
  hwaddress = BASE;

  Reset();

  SetShort( 0x0006, 0x0024);  // General control (comm start) 

  SetRange( 0xf0);
 
  SetShort( 0x0100, 0x000f);  // Enable channels 0-31
  SetShort( 0x0180, 0x000f);  // Enable channels 32-63
 
  SetShort( 0x010A, 0x1);     // Clear event counter
  SetShort( 0x3000, 0x0000);  // Trigger counter mode


  EnableZeroOverflowSupression();
  SetLowThreshold(0);
  SetHighThreshold(4000);


  // SetShort( 0x0000, 0x0008);  // Interrupt stuff
  // SetShort( 0x0002, 0x0001);  // Interrupt stuff

  //
  // TEST mode 
  //
  // SetShort( 0x0006, 0x0060); // switch to test mode
  // SetShort( 0x0016, 0x0004); // delay 100 ns
  // SetShort( 0x0014, 0x0001); // start/stop generation

  if ( debug > 1)
    {
      PrintHardwareStatus();
      PrintModuleIdentifierWords();
      PrintGeneralControl();
    }
}
//////////////////////////////////////////////////////////////////////////
TCaen1488::~TCaen1488()
{
  munmap( (char *)fbase, TCAEN1488SIZE);
  close( ffd);
}
//////////////////////////////////////////////////////////////////////////
// Generic write/read access to the 16 bit memory area (the control section)
void TCaen1488::SetShort( UShort_t address, UShort_t value)
{
  *(fbase16 + address/2) = value;
  return;
}
UShort_t TCaen1488::GetShort( UShort_t address)
{
 return ( *(fbase16 + address/2) );  
}
UShort_t TCaen1488::GetShort( UShort_t address, UShort_t bitmask)
{
  return (bitmask & (*(fbase16 + address/2)));
}

// 32 bit access to the MEB
UInt_t TCaen1488::GetLong( UShort_t address)
{
  return ( *(fbase32 + address/4));
}
//////////////////////////////////////////////////////////////////////////
void TCaen1488::Init( UInt_t BASE)
{
  if ((ffd = open( memdev, O_RDWR)) == -1) 
    {
      printf("Module %s at 0x%x : file %s line %d\n", description, hwaddress, __FILE__, __LINE__);
      fprintf(stderr, "ERROR: open(): %s\n", strerror(errno));
      exit(EXIT_FAILURE);
    }
    
  if ((fbase = mmap(0, TCAEN1488SIZE, PROT_WRITE, MAP_SHARED, ffd, BASE)) == MAP_FAILED) 
    {
      printf("Module %s at 0x%x : file %s line %d\n", description, hwaddress, __FILE__, __LINE__);
      fprintf(stderr, "ERROR: mmap(): %s\n", strerror(errno));
      exit(EXIT_FAILURE);
    }

  fbase = (volatile unsigned short *)fbase;

  if ( fbase == (void *)-1 )
    {
      printf("Module %s at 0x%x : file %s line %d\n", description, hwaddress, __FILE__, __LINE__);
      fprintf( stderr, "Error mmapping\n");
      exit(EXIT_FAILURE);
    }

  fbase16 = (volatile UShort_t *)((char *)fbase);
  fbase32 = (volatile UInt_t *)((char *)fbase);  

  return;
}
//////////////////////////////////////////////////////////////////////////
void TCaen1488::EnableZeroOverflowSupression()
{
  SetShort( 0x0102, 0x0010);  // Enable zero/overflow suppression  
  SetShort( 0x0182, 0x0010);  // Enable zero/overflow suppression  
  return ;
}
//////////////////////////////////////////////////////////////////////////
void TCaen1488::SetHighThreshold( UShort_t ht)
{
  if ( ht > 0x0fff)
    {
      printf("Module %s at 0x%x : file %s line %d\n", description, hwaddress, __FILE__, __LINE__);
      printf("The high threshold is out of range\n");
      return;
    }
  //
  // Set the upper 8 bits of the threshold
  // 0xff <=> 3840 counts
  // 
  ht = (ht >> 4) & 0x00ff;
  SetShort( 0x0080, ht);  // Set the high threshold, 0xff <=> 3840 counts
  return;
}
//////////////////////////////////////////////////////////////////////////
void TCaen1488::SetLowThreshold( UShort_t lt)
{
  if ( lt > 0x0fff)
    {
      printf("Module %s at 0x%x : file %s line %d\n", description, hwaddress, __FILE__, __LINE__);
      printf("The low threshold is out of range\n");
      return;
    }
  //
  // Set the upper 8 bits of the threshold
  // 0xff <=> 3840 counts
  // 
  lt = (lt >> 4) & 0x00ff;
  SetShort( 0x0082, lt);  
  return;
}
//////////////////////////////////////////////////////////////////////////
void TCaen1488::SetRange( UShort_t r)
{ 
  if ( r > 0x00ff)
    {
      printf("Module %s at 0x%x : file %s line %d\n", description, hwaddress, __FILE__, __LINE__);
      printf("The selected range is out of range\n");
      return;
    }
  SetShort( 0x0084, r);
  return;
}
//////////////////////////////////////////////////////////////////////////
void TCaen1488::PrintHardwareStatus()
{
  UShort_t status;
  status = GetShort( 0x0008);

  if ( (status & 0x0004) == 0)
    printf("Hardware status: CTRL BUS TERM ON\n");

  if ( (status & 0x0008) == 0)
    printf("Hardware status: CTRL BUS TERM OFF\n");

  printf("Hardware status: BOARD NO. %d\n", (status & 0x1f00) >> 8);

  return;
}
//////////////////////////////////////////////////////////////////////////
void TCaen1488::PrintModuleIdentifierWords()
{
  UShort_t status;

  status = GetShort( 0x00FC);
  printf("PrintModuleIdentifierWords: Module type %d\n", status & 0x03ff);  
  printf("PrintModuleIdentifierWords: Manufacturer No %d\n", (status & 0xfc00) >> 10);  

  status = GetShort( 0x00FE);
  printf("PrintModuleIdentifierWords: Serial No %d\n", status & 0x0fff);  
  printf("PrintModuleIdentifierWords: Version %d\n", (status & 0xf000) >> 12);  
  return;
}
//////////////////////////////////////////////////////////////////////////
void TCaen1488::PrintGeneralControl()
{
  UShort_t status;
  status = GetShort( 0x0006);

  printf("General control: AUTOINC_ENABLE %d\n", status & 0x0001);  
  printf("General control: TEST           %d\n", (status & 0x0004) >> 2);  
  printf("General control: VETO_VME       %d\n", (status & 0x0008) >> 3);  
  printf("General control: BERR_ENABLE    %d\n", (status & 0x0010) >> 4);  
  printf("General control: STR_MODE       %d\n", (status & 0x0020) >> 5);  
  printf("General control: STP_MODE       %d\n", (status & 0x0040) >> 6);  
  return;
}
//////////////////////////////////////////////////////////////////////////
void TCaen1488::PrintAcquisitionStatus()
{
  UShort_t status;
  status = GetShort( 0x3006);

  printf("Acquisition status: DATA_READY       %d\n", (status & 0x0800) >> 11);  
  printf("Acquisition status: DATA_MEMORY_FULL %d (1 : NOT full, 0 : full)\n", (status & 0x1000) >> 12);  
  printf("Acquisition status: BUSY             %d\n", (status & 0x4000) >> 14);  
  return;
}
//////////////////////////////////////////////////////////////////////////
UShort_t TCaen1488::IsHeader( UInt_t buffer)
{
  if (buffer & 0x80000000) 
    return 1;
  else 
    return 0;
}
//////////////////////////////////////////////////////////////////////////
UShort_t TCaen1488::IsDatum( UInt_t buffer)
{
  if (!(buffer & 0x80000000) && !(buffer & 0x40000000)) 
    return 1;
  else 
    return 0;
}
//////////////////////////////////////////////////////////////////////////
UShort_t TCaen1488::IsEOB( UInt_t buffer)
{
  if (buffer & 0x40000000) 
    return 1;
  else 
    return 0;
}
//////////////////////////////////////////////////////////////////////////
UShort_t TCaen1488::GetMultiplicity( UInt_t buffer)
{
  if ( !IsHeader( buffer))
    {
      printf("Module %s at 0x%x : file %s line %d\n", description, hwaddress, __FILE__, __LINE__);
      perror("Not a header, exit");
      exit(-1);
    }
  else
    return (0x003f & (buffer >> 16));
}
//////////////////////////////////////////////////////////////////////////
UShort_t TCaen1488::GetChannel( UInt_t buffer)
{
  return ( (buffer >> 16) & 0x3f);
}
//////////////////////////////////////////////////////////////////////////
UShort_t TCaen1488::GetValue( UInt_t buffer)
{
  return ( buffer & 0xfff);
}
//////////////////////////////////////////////////////////////////////////
UShort_t TCaen1488::GetData( UShort_t *ch, UShort_t *val)
{
  UInt_t buffer;
  UShort_t mult, i, address;

  // Check that the modeule is not busy
  if ( Busy())
    {
      if ( debug >= 1)
        {
          printf("Module %s at 0x%x : file %s line %d\n", description, hwaddress, __FILE__, __LINE__);
          printf("Module is busy\n");
        }
      return 0;      
    }    

  // Check that there is data
  if ( ! DataReady())
    {
      if ( debug > 1)
        {
          printf("Module %s at 0x%x : file %s line %d\n", description, hwaddress, __FILE__, __LINE__);
          printf("Buffer is empty\n");
        }
      return 0;      
    }

  // Get the multiplicity
  buffer = GetLong( 0x8000);
  mult = GetMultiplicity( buffer);  // will exit if not a header
  //
  // If all 64 channels were converted, the multiplicity is 0
  // Check if the next datum is channel data
  //
  if ( mult == 0)
    {
      buffer = GetLong( 0x8000 + 0x0004);
      if ( IsDatum( buffer))
	mult = 64;
    }
  if ( debug > 1)
    printf("Multiplicity is %d\n", mult);

  address = 0x8000;
  for ( i = 0; i < mult; i++)
    {
      address = address + 4;
      buffer = GetLong( address);
      if ( ! IsDatum( buffer))
	{
	  printf("Module %s at 0x%x : file %s line %d\n", description, hwaddress, __FILE__, __LINE__);
	  printf("Not a datum, exit\n");
	  exit(-1);
	}
      *ch = GetChannel( buffer);
      *val = GetValue( buffer);
      if ( debug > 1)
	printf("ch : %d     val : %d\n", *ch, *val);
      ch++;
      val++;
    }
  
  // Check for EOB
  address = address + 4;
  buffer = GetLong( address);
  IncrementPointer(); // should do this before returning     
 
  // Check that there is no more events
  if ( DataReady())
    {
      if ( debug >= 1)
	{
	  printf("Module %s at 0x%x : file %s line %d\n", description, hwaddress, __FILE__, __LINE__);
	  printf("Still data in the buffer\n");
	}
    }

  if ( IsEOB( buffer))
    return mult;
  else
    return 0;
}
//////////////////////////////////////////////////////////////////////////
void TCaen1488::bitprint( UInt_t buffer)
{
  int i = 31;
  while( i >= 0)
    {
      if ( (buffer >> i) & 0x00000001 )
        printf("1");
      else
        printf("0");
      if ( (i % 8 == 0) && (i != 0))
        printf("|");
      i--;
    }
  printf("\n");
}
//////////////////////////////////////////////////////////////////////////
