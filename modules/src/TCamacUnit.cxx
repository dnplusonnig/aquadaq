
#include "TCamacUnit.h"

ClassImp( TCamacUnit )

TCamacUnit::TCamacUnit(TCamacBranch *cbd, int c, int n, int ch ): 
fCBD( cbd ), fCrate(c), fSlot(n)
{

  if( ! cbd->Online( fCrate ) )
    fprintf( stderr, "Warning: Crate %d is not online!\n", fCrate);

  fNoChannels = ch;

  fClear = (volatile Short_t *)cbd->cnaf( fCrate, fSlot, 0, 9);
  fTest = (volatile Short_t *)cbd->cnaf( fCrate, fSlot, 0, 25);

  for( int i = 0; i < ch ; i++)
    {
      pRead[i] = (volatile Short_t *)cbd->cnaf( fCrate, fSlot, i, 0);
      pReadClear[i] = (volatile Short_t *)cbd->cnaf( fCrate, fSlot, i, 2);
      pWrite[i] = (volatile Short_t *)cbd->cnaf( fCrate, fSlot, i, 17);
    }
}

TCamacUnit::TCamacUnit(TCamacCrate *C, int n, int ch ):
fCBD( C->GetCBD() ), fCrate( C->Crate() ), fSlot(n)
{

  if( ! fCBD->Online( fCrate ) )
    fprintf( stderr, "Warning: Crate %d is not online!\n", fCrate);

  fNoChannels = ch;

  fClear = (volatile Short_t *)fCBD->cnaf( fCrate, fSlot, 0, 9);
  fTest = (volatile Short_t *)fCBD->cnaf( fCrate, fSlot, 0, 25);

  for( int i = 0; i < ch ; i++)
    {
      pRead[i] = (volatile Short_t *)fCBD->cnaf( fCrate, fSlot, i, 0);
      pReadClear[i] = (volatile Short_t *)fCBD->cnaf( fCrate, fSlot, i, 2);
      pWrite[i] = (volatile Short_t *)fCBD->cnaf( fCrate, fSlot, i, 17);
    }
}

TCamacUnit::~TCamacUnit( )
{
}

Int_t TCamacUnit::ReadArray( Short_t *Array, Int_t count)
{
  // Read an array of count values into Array
  // from channel starting on 0.
  
  int i;

  for( i = 0; i < count; i++)
    *Array++ = *(Short_t *)pRead[i];

  return i;
}

Short_t& TCamacUnit::operator[](int n )
{
  return *(Short_t *)pRead[n];
}

Bool_t TCamacUnit::Write(int val, int n = 0)
{

  *pWrite[n] = val;

  return Q();
}

void TCamacUnit::Test(int l)
{
  volatile Short_t dum;

  dum = *fTest;
}

void TCamacUnit::Print( )
{
  for( int i = 0; i < fNoChannels; i++)
    printf( "Channel: %d, value: %d\n", i, operator[](i));
}
