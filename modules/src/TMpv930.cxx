///////////////////////////////////////////////////////////////////
//
// MPV930 input/output unit
// Magnus Lundin 2004-05-17
//
///////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <errno.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <string.h>
#if BUSADAPTER == SIS
#elif BUSADAPTER == SBS
#include <vmedrv.h>
#endif


#include "TMpv930.h"

ClassImp( TMpv930 )

TMpv930::TMpv930()
{
  if ((fd = open(MPV930DEVFILE, O_RDWR)) == -1) {
    fprintf(stderr, "ERROR: open(): %s\n", strerror(errno));
    exit(EXIT_FAILURE);
  }
    
  if ((mapped_base = mmap(0, MPV930SIZE, PROT_WRITE, MAP_SHARED, fd, MPV930BASE)) == MAP_FAILED) {
    fprintf(stderr, "ERROR: mmap(): %s\n", strerror(errno));
    exit(EXIT_FAILURE);
  }

  base = (volatile unsigned short *)mapped_base;
}

TMpv930::~TMpv930( )
{
  munmap(mapped_base, MPV930SIZE);
  close(fd);
}

Int_t TMpv930::Output()
{
  // Switch to output mode
  *(base + 1) = 0xff;

  // Set the output
  *(base + 4) = 0xf000;

  // Reset the output
  *(base + 4) = 0x0000;

  return 0;
}
