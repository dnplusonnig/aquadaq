
#include "TCes1320.h"

ClassImp( TCes1320 )

TCes1320::TCes1320(TCamacBranch *cbd, int c, int n): 
fCBD( cbd ), fCrate(c), fSlot(n)
{
  if( ! cbd->Online( fCrate ) )
    fprintf( stderr, "Warning: Crate %d is not online!\n", fCrate);

  pRead      = (volatile UShort_t *)cbd->cnaf( fCrate, fSlot, 0, 0);
  pClear     = (volatile UShort_t *)cbd->cnaf( fCrate, fSlot, 0, 9);
  pSetBits   = (volatile UShort_t *)cbd->cnaf( fCrate, fSlot, 0, 18);
  pUnSetBits = (volatile UShort_t *)cbd->cnaf( fCrate, fSlot, 0, 21);
  pDisablePulse = (volatile UShort_t *)cbd->cnaf( fCrate, fSlot, 0, 24);
  pEnablePulse  = (volatile UShort_t *)cbd->cnaf( fCrate, fSlot, 0, 26);

  DisablePulse();
  Clear();
}

TCes1320::TCes1320(TCamacCrate *C, int n):
fCBD( C->GetCBD() ), fCrate( C->Crate() ), fSlot(n)
{

  if( ! fCBD->Online( fCrate ) )
    fprintf( stderr, "Warning: Crate %d is not online!\n", fCrate);

  pRead      = (volatile UShort_t *)fCBD->cnaf( fCrate, fSlot, 0, 0);
  pClear     = (volatile UShort_t *)fCBD->cnaf( fCrate, fSlot, 0, 9);
  pSetBits   = (volatile UShort_t *)fCBD->cnaf( fCrate, fSlot, 0, 18);
  pUnSetBits = (volatile UShort_t *)fCBD->cnaf( fCrate, fSlot, 0, 21);
  pDisablePulse = (volatile UShort_t *)fCBD->cnaf( fCrate, fSlot, 0, 24);
  pEnablePulse  = (volatile UShort_t *)fCBD->cnaf( fCrate, fSlot, 0, 26);

  DisablePulse();
  Clear();
}

TCes1320::~TCes1320( )
{
}

UShort_t TCes1320::Read()
{
  return *pRead;
}

void TCes1320::Clear()
{
  volatile UShort_t stupid;
  stupid = *pClear;
}

Bool_t TCes1320::SetBits(UShort_t value)
{
  *pSetBits = value;
  return Q();
}

Bool_t TCes1320::UnSetBits(UShort_t value)
{
  *pUnSetBits = value;
  return Q();
}

void TCes1320::ToggleBits(UShort_t value)
{
  *pSetBits = value;
  *pUnSetBits = value;
}

void TCes1320::Print( )
{
  printf( "TSen1320 value: 0x%x\n", Read());
}
