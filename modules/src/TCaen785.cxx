///////////////////////////////////////////////////////////////////
//
// Caen 785 32 channel peak sensing ADC
// Magnus Lundin 2002-07-18 (loosly based on TStruckDL515)
//
///////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <fcntl.h>

#include "TCaen785.h"


ClassImp( TCaen785 )

const char description[] = "Caen 785 ADC";
const UShort_t CAEN785SIZE = 0xffff;

#ifdef SIS
const char memdev[] = "/dev/sis110032d16";
#endif
#ifdef SBS
const char memdev[] = "/dev/vmedrv32d16";
#endif

/////////////////////////////////////////////////////////////////////////
TCaen785::TCaen785( UInt_t BASE, UShort_t threshold, UShort_t debugflag)
{
  hwaddress = BASE;
  debug = debugflag;
  Init( BASE, threshold);
}
/////////////////////////////////////////////////////////////////////////
TCaen785::~TCaen785( )
{
  munmap( (char *)fbase, CAEN785SIZE);
  close( ffd );
}
/////////////////////////////////////////////////////////////////////////
void TCaen785::SetShort( UShort_t address, UShort_t value)
{
  *(fbase16 + address/2) = value;
  return;
}
UShort_t TCaen785::GetShort( UShort_t address)
{
 return ( *(fbase16 + address/2) );  
}
/////////////////////////////////////////////////////////////////////////
void TCaen785::Init( UInt_t BASE, UShort_t threshold)
{
  if (  (ffd = open( memdev, O_RDWR | O_SYNC)) == -1) 
    {
      printf("Module %s at 0x%x : file %s line %d\n", description, hwaddress, __FILE__, __LINE__);
      //      printf("open() failed: %s\n", strerror(errno));
      exit(EXIT_FAILURE);
    }

  if ((fbase = (volatile void *)mmap(NULL, CAEN785SIZE, PROT_WRITE, MAP_SHARED, ffd, BASE)) == MAP_FAILED)
    {
      printf("Module %s at 0x%x : file %s line %d\n", description, hwaddress, __FILE__, __LINE__);
      //      printf("mmap() falied: %s\n", strerror(errno));
      exit(EXIT_FAILURE);
    }

  if ( fbase == (void *)-1 )
    {
      printf("Module %s at 0x%x : file %s line %d\n", description, hwaddress, __FILE__, __LINE__);
      exit(-1);
    }

  fbase16 = (volatile UShort_t *)((char *)fbase);
  fbase32 = (volatile UInt_t *)((char *)fbase);  

  DataReset();
  SoftwareReset();
  SetThresholds( threshold);

  // deactivate none
  DeactivateChannels(0);

  DisregardOverflows();
  //KeepOverflows();

  if ( debug > 1)
    {
      fprintf( stderr, "fbase at %p \n", fbase);
      Info();
      PrintStatus();
    }

  return;
}
/////////////////////////////////////////////////////////////////////////
void TCaen785::SetThreshold( UShort_t channel, UShort_t value)
{
  // Sets the threshold for a given channel. The kill bit is preserved.
  if ( channel > 31 )
    {
      printf("Module %s at 0x%x : file %s line %d\n", description, hwaddress, __FILE__, __LINE__);
      printf("channel > 31\n");
      exit(-1);
    }

  if ( value > 0xfff )
    {
      printf("Module %s at 0x%x : file %s line %d\n", description, hwaddress, __FILE__, __LINE__);
      printf("value out of range\n");
      exit(-1);
    }

  // enable the thresholds
  SetShort( 0x1034, 0x10);

  // Need to right shift the value (section 2.3)
  value = value >> 4;

  // See if the kill bit is set and set the threshold
  // while preserving the kill bit
  if ( GetShort( 0x1080 + channel*2) & 0x100 )
    SetShort( 0x1080 + channel*2, value + 0x100);      
  else
    SetShort( 0x1080 + channel*2, value);      

  return;
}
/////////////////////////////////////////////////////////////////////////
void TCaen785::SetThresholds( UShort_t value)
{
  // Sets the same threshold for all 32 channels
  UShort_t i;

  // set thresholds
  for ( i = 0; i < 32; i++)
    SetThreshold( i, value);

  return;
}  
/////////////////////////////////////////////////////////////////////////
void TCaen785::DeactivateChannels( UInt_t bitmask)
{
  // Set the kill bit (see 4.39) for selected channels. Channels with the
  // kill bit set will not be converted
  
  UShort_t i;
  UShort_t thres;   /* the previously set threshold */

  for ( i = 0; i < 32; i++)
    {
      thres = 0xff & GetShort( 0x1080 + i*2);
      if ( 0x1 & ((bitmask) >> i) )  
	SetShort( 0x1080 + i*2, thres + 0x100);  /* the kill bit */
      else
	SetShort( 0x1080 + i*2, thres);
    }
  return;
}
//////////////////////////////////////////////////////////////////////////
UShort_t TCaen785::GetMultiplicity( UInt_t buffer)
{
  if ( (0x0007 & (buffer >> 24)) != 0x0002 )
    {
      printf("Module %s at 0x%x : file %s line %d\n", description, hwaddress, __FILE__, __LINE__);
      printf("Not a header, exit\n");
      exit(-1);
    }
  else
    return (0x3f & (buffer >> 8));
}
/////////////////////////////////////////////////////////////////////////
void TCaen785::GetDatum( UShort_t *channel, UShort_t *value)
{
  unsigned long datum;

  // Get the value at the read pointer
  datum = *fbase32;

  if ( (0x0007 & (datum >> 24)) != 0x0000 )   // a datum?
    {
      printf("Module %s at 0x%x : file %s line %d\n", description, hwaddress, __FILE__, __LINE__);
      printf("Not a datum, exit\n");
      exit(-1);
    }
  else
    {
      *channel = 0x3f & (datum >> 16);
      *value   = 0x3fff & datum;
    }
  return;
}
/////////////////////////////////////////////////////////////////////////
UShort_t TCaen785::IsEOB( UInt_t buffer)
{
  if ( (0x7 & (buffer >> 24)) != 0x4 )
    { 
      if ( debug == 1) // magnus >= 1
	{
	  printf("Module %s at 0x%x : file %s line %d\n", description, hwaddress, __FILE__, __LINE__);
	  printf("Not an EOB\n");
	}
      return 0;
    }
  else
    return 1;
}
//////////////////////////////////////////////////////////////////////////
UShort_t TCaen785::GetData( UShort_t *ch, UShort_t *val)
{
  UInt_t buffer;
  UShort_t mult, i;

  // Check that the module is not busy
  if ( Busy())
    {
      printf("Module %s at 0x%x : file %s line %d\n", description, hwaddress, __FILE__, __LINE__);
      printf("Module busy\n");
      return 0;
    }

  // Check that there is data
  if ( ! DataReady())
    {
      if ( debug > 1)
	{
	  printf("Module %s at 0x%x : file %s line %d\n", description, hwaddress, __FILE__, __LINE__);
	  printf("No data ready\n");
	}
      return 0;
    }

  buffer = *fbase32;
  if ( debug > 1)
    {
      printf("HEADER : ");
      bitprint( buffer);
    }

  mult = GetMultiplicity( buffer);  // will exit if not a header

  for ( i = 0; i < mult; i++)
    {
      // Get the value at the read pointer
      buffer = *fbase32;

      if ( (0x0007 & (buffer >> 24)) != 0x0000 )   // a datum?
	{
	  printf("Module %s at 0x%x : file %s line %d\n", description, hwaddress, __FILE__, __LINE__);
	  printf("Not a datum, exit\n");
	  exit(-1);
	}
      else
	{
	  *ch = 0x3f & (buffer >> 16);
	  *val = 0x3fff & buffer; 
	}

      if ( debug > 1)
	{
	  printf("DATUM : ");
	  bitprint( buffer);
	}

      if (debug > 1)
	printf("ch : %d     val : %d\n", *ch, *val);

      ch++;
      val++;
    }
  
  // Check for EOB
  buffer = *fbase32;

  if ( ! IsEOB( buffer))
    {
      printf("Module %s at 0x%x : file %s line %d\n", description, hwaddress, __FILE__, __LINE__);
      printf("Not an EOB, exit\n");
      exit(-1);
    }

  if (debug > 1)
    {
      printf("EOB : ");
      bitprint( buffer);
    }

  // Check that there is no more events
  if ( DataReady())
    {
      printf("Module %s at 0x%x : file %s line %d\n", description, hwaddress, __FILE__, __LINE__);
      printf("There is still data in buffer, reseting data and continue\n");
      DataReset();
    }
  
  return mult;
}
/////////////////////////////////////////////////////////////////////////
UShort_t TCaen785::BufferEmpty()
{
  if ( (GetShort(0x1022) >> 1) & 0x1 )
    return 1;
  else
    return 0;
}
//////////////////////////////////////////////////////////////////////////
void TCaen785::PrintThresholds()
{
  UShort_t i, val;

  fprintf( stderr, "----------- Thresholds -----------\n");

  for ( i = 0; i < 32; i++)
    {
      val = 0x1ff & GetShort( 0x1080 + i*2);
      fprintf( stderr, "Ch. %d  : 0x%x  : kill %x \n", i, 0xfff & (val << 4), 0x1 & (val >> 8));
    }
  return;
}
//////////////////////////////////////////////////////////////////////////
void TCaen785::Info()
{
  printf("firmware : 0x%x\n", GetShort( 0x1000));  
  
  printf("geo : 0x%x\n", 0x001f & GetShort( 0x1002));  

  printf("interrupt level: 0x%x\n", 0x0007 & GetShort(0x100a));  

  printf("interrupt vector: 0x%x\n", 0x00ff & GetShort(0x100c));  

  printf("event trigger: 0x%x\n", 0x001f & GetShort(0x1020));  

  printf("event counter low: 0x%x\n", GetShort(0x1024));  

  printf("event counter high: 0x%x\n", 0x00ff & GetShort(0x1026));  

  printf("Board id : %d \n", ((0x00ff & GetShort(0x803a)) << 8) + (0x00ff & GetShort(0x803e)));

  return;
}
//////////////////////////////////////////////////////////////////////////
void TCaen785::PrintStatus()
{
  unsigned short stat1,stat2,bitset1,bitset2;

  stat1 = GetShort( 0x100e);
  printf("stat1 %x\n", stat1);

  if ( stat1 & 0x0001 )
    printf("stat 1 \t: DREADY\n");
  else
    printf("stat 1 \t: NO DREADY\n");

  if ( stat1 & (0x0001 << 1) )
    printf("stat 1 \t: GLOBAL DREADY\n");
  else
    printf("stat 1 \t: NO GLOBAL DREADY\n");

  if ( stat1 & (0x0001 << 2) )
    printf("stat 1 \t: BUSY\n");
  else
    printf("stat 1 \t: NOT BUSY\n");

  if ( stat1 & (0x0001 << 3) )
    printf("stat 1 \t: GLOBAL BUSY\n");
  else
    printf("stat 1 \t: NOT GLOBAL BUSY\n");

  if ( stat1 & (0x0001 << 4) )
    printf("stat 1 \t: AMNESIA\n");
  else
    printf("stat 1 \t: NO AMNESIA\n");

  if ( stat1 & (0x0001 << 5) )
    printf("stat 1 \t: PURGED\n");
  else
    printf("stat 1 \t: NOT PURGED\n");

  if ( stat1 & (0x0001 << 6) )
    printf("stat 1 \t: ALL TERM ON\n");
  else
    printf("stat 1 \t: NOT ALL TERM ON\n");

  if ( stat1 & (0x0001 << 7) )
    printf("stat 1 \t: ALL TERM OFF\n");
  else
    printf("stat 1 \t: NOT ALL TERM OFF\n");

  if ( stat1 & (0x0001 << 8) )
    printf("stat 1 \t: EVRDY\n");
  else
    printf("stat 1 \t: NOT EVRDY\n");


  stat2 = GetShort( 0x1022);
  printf("stat2 \t %x\n", stat2);

  if ( stat2 & (0x0001 << 1) )
    printf("stat 2 \t: BUFFER EMPTY\n");
  else
    printf("stat 2 \t: BUFFER NOT EMPTY\n");

  if ( stat2 & (0x0001 << 2) )
    printf("stat 2 \t: BUFFER FULL\n");
  else
    printf("stat 2 \t: BUFFER NOT FULL\n");

  if ( stat2 & (0x0001 << 4) )
    printf("stat 2 \t: DSEL0 1\n");
  else
    printf("stat 2 \t: DSEL0 0\n");

  if ( stat2 & (0x0001 << 5) )
    printf("stat 2 \t: DSEL1 1\n");
  else
    printf("stat 2 \t: DSEL1 0\n");

  if ( stat2 & (0x0001 << 6) )
    printf("stat 2 \t: CSEL0 1\n");
  else
    printf("stat 2 \t: CSEL0 0\n");

  if ( stat2 & (0x0001 << 7) )
    printf("stat 2 \t: CSEL1 1\n");
  else
    printf("stat 2 \t: CSEL1 0\n");


  bitset1 = GetShort( 0x1006);
  printf("bitset1 %x\n", bitset1);

  if ( bitset1 & (0x0001 << 3) )
    printf("bitset 1 \t: BERR\n");
  else
    printf("bitset 1 \t: NO BERR\n");

  if ( bitset1 & (0x0001 << 4) )
    printf("bitset 1\t: SELECT ADDRESS ON (ADER)\n");
  else
    printf("bitset 1\t: SELECT ADDRESS OFF (ROTARY SW)\n");

  if ( bitset1 & (0x0001 << 7) )
    printf("bitset 1\t: SOFTWARE RESET\n");
  else
    printf("bitset 1\t: NO SOFTWARE RESET\n");


  bitset2 = GetShort( 0x1032);
  printf("bitset2 %x\n", bitset2);

  if ( bitset2 & (0x0001 << 0) )
    printf("bitset 2 \t: MEM TEST\n");
  else
    printf("bitset 2 \t: NO MEM TEST\n");

  if ( bitset2 & (0x0001 << 1) )
    printf("bitset 2 \t: OFFLINE\n");
  else
    printf("bitset 2 \t: NOT OFFLINE\n");

  if ( bitset2 & (0x0001 << 2) )
    printf("bitset 2 \t: CLEAR DATA\n");
  else
    printf("bitset 2 \t: NOT CLEAR DATA\n");

  if ( bitset2 & (0x0001 << 3) )
    printf("bitset 2 \t: OVER RANGE CHECK DISABLED\n");
  else
    printf("bitset 2 \t: OVER RANGE CHECK ENABLED\n");

  if ( bitset2 & (0x0001 << 4) )
    printf("bitset 2 \t: LOW THRESHOLD CHECK DISABLED\n");
  else
    printf("bitset 2 \t: LOW THRESHOLD CHECK ENABLED\n");

  if ( bitset2 & (0x0001 << 6) )
    printf("bitset 2 \t: TEST ACQ ENABLED\n");
  else
    printf("bitset 2 \t: TEST ACQ DISABLED\n");

  if ( bitset2 & (0x0001 << 7) )
    printf("bitset 2 \t: SLIDE ENABLED\n");
  else
    printf("bitset 2 \t: SLIDE DISABLED\n");

  if ( bitset2 & (0x0001 << 11) )
    printf("bitset 2 \t: AUTO INCR ENABLED\n");
  else
    printf("bitset 2 \t: AUTO INC DISABLED\n");

  if ( bitset2 & (0x0001 << 12) )
    printf("bitset 2 \t: EMPTY PROG ENABLED\n");
  else
    printf("bitset 2 \t: EMPTY PROG DISABLED\n");

  if ( bitset2 & (0x0001 << 13) )
    printf("bitset 2 \t: SLIDE_SUB DISABLED\n");
  else
    printf("bitset 2 \t: SLIDE_SUB ENABLED\n");

  if ( bitset2 & (0x0001 << 14) )
    printf("bitset 2 \t: ALL TRG ENABLED\n");
  else
    printf("bitset 2 \t: ALL TRG DISABLED\n");

  return;
}
/////////////////////////////////////////////////////////////////////////
void TCaen785::bitprint( UInt_t buffer)
{
  int i = 31;
  while( i >= 0)
    {
      if ( (buffer >> i) & 0x00000001 )
    	printf("1");
      else
	printf("0");
      if ( (i % 8 == 0) && (i != 0))
	printf("|");
      i--;
    }
  printf("\n");
}
//////////////////////////////////////////////////////////////////////////
