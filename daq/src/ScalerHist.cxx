////////////////////////////////////////////////////////////////////////

#include "TDirectory.h"
#include "TMapFile.h"
#include "ScalerHist.h"
#include "log.h"


ClassImp(ScalerHist)

using namespace aquadaq;


//________________________________________________________________________
ScalerHist::ScalerHist()
{
  ListOfScalerHistos = new THashList(1000);  // Set the capacity of the List
}
//________________________________________________________________________
ScalerHist::~ScalerHist()
{
  ListOfScalerHistos->Delete();
}
//______________________________________________________________________________
void ScalerHist::CreateHistograms()
{
  printf( "Creating Scaler histograms...\n");

  hc1_8 = new TH1F("hc1_8","Scaler c1_8", 32, 0, 32);
  ListOfScalerHistos->Add(hc1_8);
  hc1_8_rate = new TH1F("hc1_8_rate","Scaler rate c1_8", 32, 0, 32);
  ListOfScalerHistos->Add(hc1_8_rate);
  hLiveTime[0] = new TH1F("hLiveTime_1","X-trig live time evolution", BACKLOG, 0, BACKLOG - 1);
  ListOfScalerHistos->Add(hLiveTime[0]);
  hLiveTime[1] = new TH1F("hLiveTime_2","Pulser/clock live time evolution", BACKLOG, 0, BACKLOG - 1);
  ListOfScalerHistos->Add(hLiveTime[1]);
  hLiveTime[2] = new TH1F("hLiveTime_3","VME X-trig live time evolution", BACKLOG, 0, BACKLOG - 1);
  ListOfScalerHistos->Add(hLiveTime[2]);
  hLiveTime[3] = new TH1F("hLiveTime_4","VME Pulser/clock live time evolution", BACKLOG, 0, BACKLOG - 1);
  ListOfScalerHistos->Add(hLiveTime[3]);
  hVMEScaler = new TH1F("hVMEScaler","FP Scaler", 64, 0, 64);
  ListOfScalerHistos->Add(hVMEScaler);
  hVMEScalerRate = new TH1F("hVMEScalerRate","FP Scaler Rate", 64, 0, 64);
  ListOfScalerHistos->Add(hVMEScalerRate);
  /*
  hFC_time = new TH1F("hFC_time","Faraday Cup time evolution (un-inhibited)", BACKLOG, 0, BACKLOG - 1);
  ListOfScalerHistos->Add(hFC_time);

  hIBM_time = new TH1F("hIBM_time","IBM (John) time evolution (inhibited)", BACKLOG, 0, BACKLOG - 1);
  ListOfScalerHistos->Add(hIBM_time);
  */

  printf( "Scaler Histograms done!\n");
}
//___________________________________________________________________________
void ScalerHist::FillHistograms( ScalerEvent *sev)
{
  int i;
  Double_t oldvalue, newvalue, period = 2.0, overflow = 16777215.0, pread;
  Double_t newvalue1;

  static Double_t preadc1_9[16] = {0.0};
  static Double_t preadc1_10[16] = {0.0};
  static UShort_t peventnumber = 0;

  for( i = 0; i < 32; i++) {
    oldvalue = (Int_t)hVMEScaler->GetBinContent(i+1);
    hVMEScaler->SetBinContent( i + 1, sev->Getvs0(i));      
    hVMEScalerRate->SetBinContent(i + 1, (Int_t)((sev->Getvs0(i) - oldvalue)/period));
  }
  for( i = 0; i < 32; i++) {
    oldvalue = (Int_t)hVMEScaler->GetBinContent(i+33);
    hVMEScaler->SetBinContent( i + 33, sev->Getvs1(i));      
    hVMEScalerRate->SetBinContent(i + 33, (Int_t)((sev->Getvs1(i) - oldvalue)/period));
  }

  Double_t nv[32];
  for( i = 0; i < 32; i++) 
    {
      oldvalue = hc1_8->GetBinContent(i+1);
      newvalue = sev->Getc1_8(i);
      newvalue1 = oldvalue + newvalue;
      //printf("Chan %d, oldvalue %u, newvalue %u\n",i,oldvalue,newvalue);
      nv[i] = newvalue;
      hc1_8->SetBinContent( i + 1, newvalue1);      
      hc1_8_rate->SetBinContent( i + 1, newvalue/period);
    }
  /*
  for( i = 0; i < 32; i++) 
    {
      oldvalue = hc1_14->GetBinContent(i+1);
      newvalue = sev->Getc1_14(i);
      hc1_14->SetBinContent( i + 1, newvalue);      

      if ( newvalue < oldvalue) // overflowed
	hc1_14_rate->SetBinContent( i + 1, (overflow-oldvalue+newvalue)/period);
      else
	hc1_14_rate->SetBinContent( i + 1, (newvalue-oldvalue)/period);
    }
  */

  if ( sev->GetEventNumber() < BACKLOG && peventnumber == sev->GetEventNumber() - 1){
    if (nv[4] != 0){
      Double_t ratio1 = 100 * nv[5]/nv[4]; // latches/xtrigs
      hLiveTime[0]->SetBinContent( sev->GetEventNumber(), ratio1);
    } else {
      LOG(logWARNING) << " CAMAC xtrg scaler is zero, cannot calculate live time in scaler event " << sev->GetEventNumber();
    }
    if (nv[6] != 0){
      Double_t ratio2 = 100 * nv[7]/nv[6]; // inhibited clock/clock
      hLiveTime[1]->SetBinContent( sev->GetEventNumber(), ratio2);
    } else {
      LOG(logWARNING) << " CAMAC clock scaler is zero, cannot calculate live time in scaler event " << sev->GetEventNumber();
    }
    if (sev->Getvs0(0) != 0){
      Double_t ratio = 100 * ((double)sev->Getvs0(1))/((double)sev->Getvs0(0)); // latches/xtrigs
      hLiveTime[2]->SetBinContent( sev->GetEventNumber(), ratio);
    } else {
      LOG(logWARNING) << " VME xtrg scaler is zero, cannot calculate live time in scaler event " << sev->GetEventNumber();
    }
    if (sev->Getvs0(12) != 0){
      Double_t ratio = 100 * ((double)sev->Getvs0(13))/((double)sev->Getvs0(12)); // inh. clk/clk
      hLiveTime[3]->SetBinContent( sev->GetEventNumber(), ratio);
    } else {
      LOG(logWARNING) << " VME clock scaler is zero, cannot calculate live time in scaler event " << sev->GetEventNumber();
    }
  }

     // FC
     /*
     newvalue = sev->Getc1_10(15);
     pread = preadc1_10[15];
     if ( newvalue < pread) // overflowed
       hFC_time->SetBinContent( sev->GetEventNumber(), overflow-pread+newvalue);
     else
       hFC_time->SetBinContent( sev->GetEventNumber(), newvalue-pread);
     preadc1_10[15] = newvalue;
     */

     //
     // IBM
     //

     // true
  /*
     if ( sev->Getc1_9(4) < preadc1_9[4])  // overflowed 
       newvalue = overflow - preadc1_9[4] + sev->Getc1_9(4);
     else
       newvalue = sev->Getc1_9(4) - preadc1_9[4];
     preadc1_9[4] = sev->Getc1_9(4);

     // random
     if ( sev->Getc1_9(5) < preadc1_9[5])  // overflowed 
       newvalue1 = overflow - preadc1_9[5] + sev->Getc1_9(5);
     else
       newvalue1 = sev->Getc1_9(5) - preadc1_9[5];
     preadc1_9[5] = sev->Getc1_9(5);
  */
     // Need to include randoms
     //hIBM_time->SetBinContent( sev->GetEventNumber(), newvalue);
  peventnumber = sev->GetEventNumber();
}
//____________________________________________________________________________
void ScalerHist::ResetAll()
{
  TH1 *h1;
  TIter next(ListOfScalerHistos);
  while( (h1 = (TH1F *)next()) )
    h1->Reset();
}
//____________________________________________________________________________
void ScalerHist::WriteAll()
{
  TH1 *h1;
  TIter next(ListOfScalerHistos);
  while( (h1 = (TH1F *)next() ))
    h1->Write();
}
