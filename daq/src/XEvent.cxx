////////////////////////////////////////////////////////////////////////

#include "TDirectory.h"

#include "XEvent.h"

ClassImp(XEvent)
ClassImp(TVtdc)
ClassImp(TVqdc)

TClonesArray *XEvent::fgVtdc = 0;
TClonesArray *XEvent::fgVqdc = 0;

//________________________________________________________________________
XEvent::XEvent()
{
  if (!fgVtdc) fgVtdc = new TClonesArray("TVtdc", 256);
  fVtdc = fgVtdc;
  fNVtdc = 0;

  if (!fgVqdc) fgVqdc = new TClonesArray("TVqdc",2*ENVqdc);
  fVqdc = fgVqdc;
  fNVqdc = 0;

}
//________________________________________________________________________
XEvent::~XEvent()
{
}
//________________________________________________________________________
void XEvent::AddVtdc( UShort_t i, UShort_t d)
{ 
  TClonesArray &vtdc = *fVtdc;  
  new ( vtdc[fNVtdc++]) TVtdc( i, d);
}
//________________________________________________________________________
void XEvent::AddVqdc( UShort_t i, UShort_t d)
{ 
  TClonesArray &vqdc = *fVqdc;  
  new ( vqdc[fNVqdc++]) TVqdc( i, d);
}
//_________________________________________________________________________
void XEvent::Clear()
{
  int i;

  fVtdc->Clear(); 
  fNVtdc = 0;

  fVqdc->Clear(); 
  fNVqdc = 0;

  for ( i = 0; i < 8; i++){
    Setc1_4( i, 0);
    Setc1_5( i, 0);
    Setc1_6( i, 0);
    Setc1_7( i, 0);
    Setc1_9( i, 0);
  }
  for ( i = 0; i < 12; i++){
    Setc1_1( i, 0);
    Setc1_2( i, 0);
    Setc1_3( i, 0);
  }

  //Setc1_23(0);
}
//________________________________________________________________________
void XEvent::PrintAll()
{
  int i,n;

  printf("Event number %d\n", GetEventNumber());
  printf("---- exp vme tdcs ----\n");
  TClonesArray *vtdc = GetVtdc();
  n = GetNVtdc();
  for( i = 0; i < n; i++) 
    {
      TVtdc *t = (TVtdc *)vtdc->operator[](i);
      printf( "%d \t %d\n", t->Index(), t->Data() );
    }
  
  printf("---- exp vme qdcs ----\n");
  TClonesArray *vqdc = GetVqdc();
  n = GetNVqdc();
  for( i = 0; i < n; i++) 
    {
      TVqdc *t = (TVqdc *)vqdc->operator[](i);
      printf( "%d \t %d\n", t->Index(), t->Data() );
    }

  printf("---- camac 2259 VDC crate 1 slot 1 ----\n");
  for( i = 0; i < 12; i++)
    printf("ch %d \t %d\n", i, Getc1_1(i));
  printf("---- camac 2249 QDC crate 1 slot 2 ----\n");
  for( i = 0; i < 12; i++)
    printf("ch %d \t %d\n", i, Getc1_2(i));
  printf("---- camac 2249 QDC crate 1 slot 3 ----\n");
  for( i = 0; i < 12; i++)
    printf("ch %d \t %d\n", i, Getc1_3(i));
  printf("---- camac 2228 TDC crate 1 slot 4 ----\n");
  for( i = 0; i < 8; i++)
    printf("ch %d \t %d\n", i, Getc1_4(i));
  printf("---- camac 2228 TDC crate 1 slot 5 ----\n");
  for( i = 0; i < 8; i++)
    printf("ch %d \t %d\n", i, Getc1_5(i));
  printf("---- camac 2228 TDC crate 1 slot 6 ----\n");
  for( i = 0; i < 8; i++)
    printf("ch %d \t %d\n", i, Getc1_6(i));
  printf("---- camac 2228 TDC crate 1 slot 7 ----\n");
  for( i = 0; i < 8; i++)
    printf("ch %d \t %d\n", i, Getc1_7(i));
  printf("---- camac 4208 TDC crate 1 slot 9 ----\n");
  for( i = 0; i < 8; i++)
    printf("ch %d \t %d\n", i, Getc1_9(i));

}
