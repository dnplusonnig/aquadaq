/*
  Consumer process

  At run start:

  Start the data reader task.
  Tell the producer daemon at the VME system to load the data writer task.
  Tell the producer daemon to start the run.
    The data writer connects to the data reader task
    and starts to write data.

  At run stop:
  Tell the producer daemon to stop the run.
    The data writer writes end of run to the data reader task
    The data reader terminates.
  
*/

#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sstream>

#include "TROOT.h"

#include <DAQ.h>
#include <Mmap.h>
#include <configVariables.h> // parameters generated during CMake config step

pid_t StartDAQ();

pid_t rpid = 0;

struct run *Run;
char str[218];
char cwd[255];			// Current working directory

int InitRun( )
{
  Run = InitMem(1);
  Run->RunState = trsInit;
  Run->HistReset = 0;

  if( signal( SIGCHLD, SIG_IGN) == SIG_ERR )
    fprintf( stderr, "Failed to set the signal handler...\n");

#ifndef DRYRUN
  getcwd( cwd, 255 );
#endif

  return 0;
}

int CloseRun()
{
  Run->RunState = trsClosed;

  return 0;
}

int SetRunno( int runno = 0 )
{
  return Run->RunNumber = runno;
}

int SetExperiment(const char *s )
{
  strncpy( Run->Experiment, s, 128);
  return 1;
}

int SetWriteFile(int k = 1)
{
  return Run->DoWrite = k;
}

int SetFillHist(int k = 1)
{
  return Run->DoFill = k;
}

int TogglePauseRun()
{
  if (Run->RunState == trsRunning)
    {
      Run->RunState = trsPaused;
    }
  else if (Run->RunState == trsPaused)
    {
      Run->RunState = trsRunning;
    }
  else
    {
      fprintf( stderr, "Run is neither paused nor running...\n");      
      return -1;
    }

  return 0;
}

int SetHistReset(int k = 1)
{
  return Run->HistReset = k;
}

int StartRun( int events, int w = 1, int f = 1 )
{

  if( Run->RunState == trsRunning )
    {
      fprintf( stderr, "Already running...\n");
      return 1;
    }

  if( rpid != 0  && (kill( rpid, 0) != -1 ))
    {
      kill( rpid, SIGUSR1 );
      fprintf( stderr, "Killed daq process...\n");
      sleep(1);
    }

  if( rpid != 0  && (kill( rpid, 0) != -1 ))
    {
      kill( rpid, SIGKILL );
      fprintf( stderr, "Kill -9 daq process...\n");
      sleep(1);
    }

  rpid = StartDAQ();

  Run->DoWrite = w;
  Run->DoFill = f;

  Run->RunState = trsRunning;

  printf("Done StartRun()\n");

  return 0;
}

int StopRun( )
{
  int wstat;

  if( Run->RunState == trsRunning )
    Run->RunState = trsStopped;
  else
    fprintf( stderr, "No run in progress\n");

  waitpid( rpid, &wstat, 0);	// Wait for reader
      
  return 0;
}

pid_t StartDAQ()
{
  pid_t pid;
  std::stringstream ss;
  ss << PACKAGE_INSTALL_DIR << "/bin/aquadaq";

  switch( (pid = fork()) )
    {
    case -1:
      perror("Fork");
      break;
    case 0:
      execl( ss.str().c_str(), "aquadaq", NULL);
      perror("execl");
    default:
      printf("StartRun() pid is %d\n", pid);
      printf("Done StartDAQ()\n");
      return pid;
      break;
    }

  return 0;
}

#include <time.h>

void  GetDateTime( char *dt, int len )
{
  time_t t = time( (time_t *)0 );

  strftime( dt, len, "%Y-%m-%d  %H:%M:%S", localtime( &t ) );  
}
