#ifndef ROOT_ScalerHist
#define ROOT_ScalerHist

//////////////////////////////////////////////////////////////////////////
//                                                                      //
//                                                                      //
//                                                                      //
// Description of the ScalerHist
//                                                                      //
//////////////////////////////////////////////////////////////////////////

#include "TObject.h"
#include "TH1.h"
#include "TH2.h"
#include "THashList.h"
#include "ScalerEvent.h"

class TDirectory;


#define BACKLOG 10000

class ScalerHist : public TObject 
{

  private:

    THashList *ListOfScalerHistos;

    TH1F *hVMEScaler;
    TH1F *hVMEScalerRate;

    TH1F *hc1_8;          // camac scaler
    TH1F *hc1_8_rate;
    TH1F *hLiveTime[4];

  public:

    ScalerHist();
    virtual ~ScalerHist();
   
    void CreateHistograms();
    void FillHistograms(ScalerEvent *sev);
   
    void ResetAll();
    void WriteAll();
   
    ClassDef(ScalerHist,1)     
};
        
#endif
