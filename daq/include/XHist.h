#ifndef ROOT_XHist
#define ROOT_XHist

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// Hist                                                                //
//                                                                      //
// Description of the XHist
//                                                                      //
//////////////////////////////////////////////////////////////////////////
#include "THashList.h"
#include "TObject.h"
#include "TH1.h"
#include "TH2.h"
#include "XEvent.h"


class TDirectory;

class XHist : public TObject
{
  private:

    THashList *ListOfXHistos;
    /*
    TH1F *ht[64];      // tagger tdcs
    TH1F *htsum;       // tagger tdc sum
    TH1F *htmult;      // tagger multiplicity
    TH1F *hthits;      // tagger hits spectrum
    TH2F *ht2D;        // tagger time vs hits

    TH1F *hvt2sum;     // exp multi tdc sum
    TH1F *hvt2mult;    // exp multi tdc multiplicity
 */

    TH1F *hvq[2*ENVqdc];    // exp qdc
   
    TH1F *hvt2[64];    // exp multi tdc
    TH1F *hc1_1[12]; 
    TH1F *hc1_2[12]; 
    TH1F *hc1_3[12]; 
    TH1F *hc1_4[8]; 
    TH1F *hc1_5[8]; 
    TH1F *hc1_9[8];
    TH2F *psd2D[4];
    TH2F *psd[4];

 public:

    XHist();
    virtual ~XHist();
  
    void CreateHistograms();
    void FillHistograms(XEvent *xev);

    void ResetAll();
    void WriteAll();
  
    ClassDef(XHist,1) 
};
    
    
#endif
