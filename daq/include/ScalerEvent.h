#ifndef ROOT_ScalerEvent
#define ROOT_ScalerEvent

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// ScalerEvent                                                          //
//                                                                      //
// Description of a Scaler event                                        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

#include "TObject.h"

class TDirectory;

class ScalerEvent : public TObject {

  private:

    UInt_t fScalEvNo;

    UInt_t fvs0[32];         // vme fp scaler
    UInt_t fvs1[32];         // vme fp scaler

    UInt_t fc1_8[32];        // exp scaler 4434

  public:

    ScalerEvent();
    virtual ~ScalerEvent();

    void  SetEventNumber(UInt_t n) { fScalEvNo = n; }
    UInt_t GetEventNumber() { return fScalEvNo; }

    void  Setvs0(UShort_t ch, UInt_t data) { fvs0[ch] = data; }
    UInt_t Getvs0(UShort_t ch) { return fvs0[ch]; }
    void  Setvs1(UShort_t ch, UInt_t data) { fvs1[ch] = data; }
    UInt_t Getvs1(UShort_t ch) { return fvs1[ch]; }

    void  Setc1_8(UShort_t ch, UInt_t data) { fc1_8[ch] = data; }
    UInt_t Getc1_8(UShort_t ch) { return fc1_8[ch]; }

    void PrintScalers();
    void ClearScalers();

    ClassDef(ScalerEvent,1)  //ScalerEvent structure
};

#endif
