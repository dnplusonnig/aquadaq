#ifndef ROOT_Experiment
#define ROOT_Experiment

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// Experiment                                                           //
//                                                                      //
// Description of the Experiment                                        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

#include "TObject.h"

#include "XEvent.h"
#include "ScalerEvent.h"
#include <TSIS1100.h>
#include <TCamacBranch.h>
#include <TCamacCrate.h>
#include <TCamacUnit.h>      

#include <TCaen775.h>
#include <TCaen830.h>
#include <TLRS4434.h>
#include <TLRS2259.h>
#include <TLRS2228.h>
#include <TLRS4413.h>
#include <TLRS4418.h>
#include <TLRS4208.h>

#include <TCaen792.h>
#include <TCaen1190.h>
#include <TCaenC208.h>
#include <TBorer1008.h>      

class TDirectory;


class Experiment : public TObject
{
  private:

  //
  // Common stuff
  //
  TSIS1100 fSIS;
  TCamacBranch CBD;	// Camac Branch Driver	
  TCamacCrate fcrate1;  // camac crate No 1
  
  //
  // X stuff
  //
  //TCaen775 fvt0;        // vme tagger tdc 
  TCaen775 fvt1;        // vme tagger tdc 
  TCaen1190 fvt2;       // vme tdc (multi)
  //TCaen1190 fvtt;       // vme tdc (multi) tagger
  TCaen792 fvq0;        // vme qdc
  TCaen792 fvq1;        // vme qdc
  // VX1751 address 0x22100000
  // crate 1


  TLRS2259 fc1_1; // VDC LRS 2259 B
  TLRS2259 fc1_2; // QDC LRS 2259 A
  TLRS2259 fc1_3; // QDC LRS 2259 W

  TLRS2228 fc1_4; // TDC LRS 2228 A
  TLRS2228 fc1_5; // TDC LRS 2228 A

  TLRS4208 fc1_9; // TDC LRS 4208



  //  TCaenC208 fc1_4;
  //  TLRS4208 fc1_21;
  //  TLRS2259 fc1_20;

  //
  // Scaler stuff
  //
  TCaen830 fvs0;        // vme fp scaler
  TCaen830 fvs1;        // vme fp scaler

  TLRS4434 fc1_8;       // camac scaler
  //TLRS4434 fc1_13;      // camac scaler

 public:
  Experiment();
  virtual ~Experiment();

  Int_t ReadXEvent(XEvent *xev);
  Int_t ReadScalerEvent(ScalerEvent *sev);

  ClassDef(Experiment,1)     //Experiment structure
};


#endif
