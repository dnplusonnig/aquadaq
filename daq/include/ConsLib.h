#ifndef __CONSLIB_H__
#define __CONSLIB_H__

int SetRunno( int );
int StartRun(int, int, int);
int StopRun();
int InitRun();
int CloseRun();
int SetExperiment(const char *s );
int TogglePauseRun();
int SetHistReset(int);
void GetDateTime( char *dt, int len);

#endif
