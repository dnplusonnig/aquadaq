#ifndef __MMAP_H__
#define __MMAP_H__

#include "DAQ.h"
/*
  Simple mmap for faster communication
 */
struct run
{
  TRunState RunState;
  int DoWrite;
  int DoFill;
  int HistReset;
  long RunNumber;
  long nEvents;
  char fPrefix[64];
  char Experiment[128];
};

struct run *InitMem(int init);
void UnMem();
void Print( struct run *r);

#endif /* __MMAP_H__ */
