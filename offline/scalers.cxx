#include <iostream>
#include <TCanvas.h>
#include <TFile.h>
#include <TTree.h>
#include <TH1D.h>
#include <TMath.h>
#include "XEvent.h"
#include "ScalerEvent.h"
#include <sstream>

using namespace std;


int main(int argc, char *argv[]){
  if (argc != 2 ){
    cout << "Error: need file name argument. Please specify a ROOT file of which the scaler information should be plotted. " << endl;
    return 1;
  }

  string infilename(argv[1]);
  TFile *infile = new TFile(infilename.c_str(),"READ");    
  if (!infile->IsOpen()){
    cout << " Error opening file: " << infilename << endl;
    return 1;
  }
  
  // Create a new rootfile for the results
  string outfilename = infilename.substr(0,infilename.find_last_of("."));
  outfilename+="_scalerplots.root";
  TFile *output = new TFile(outfilename.c_str(), "RECREATE");

     
  cout << "------------------------------------------------------------ " << endl;
  cout << " Processing file " << infilename << ", writing results to " << outfilename << endl;
      
        
  // 
  //-------------------Loop over events----------------------
  //

  // set up tree, branches and scalerevent
  TTree *TS;
  TBranch *sbranch;
  ScalerEvent *scalerevent = new ScalerEvent();

  TS =  (TTree*) infile->Get("TS");
  sbranch  = TS->GetBranch("scalerbranch");
  sbranch->SetAddress(&scalerevent);
  int sentries = TS->GetEntries();
  cout<<"Number of scaler entries "<<sentries<<endl;

  int index = 0;
  int start = 0;
  const int stepsize = 10000;
  int stop = start + stepsize;

  // histograms showing the distribution of live times
  // CAMAC-based
  TH1D*  hLiveTimeProjCmcXTr = new TH1D("hLiveTime_Proj_Cmc_Xtr","X-trig live time projection", 100, 0, 100);
  TH1D*  hLiveTimeProjCmcClk = new TH1D("hLiveTime_Proj_Cmc_Clk","Pulser/clock live time projection", 100, 0, 100);
  TH1D*  hLiveTimeCmcClkMinusXTr = new TH1D("hLiveTime_Cmc_Clk-XTr","Pulser/clock - X-trig live time", 1000, -10, 10);
  // VME-based
  TH1D*  hLiveTimeProjVmeXtr = new TH1D("hLiveTime_Proj_Vme_Xtr","Vme scalers Xtrg live time projection", 100, 0, 100);
  TH1D*  hLiveTimeProjVmeClk = new TH1D("hLiveTime_Proj_Vme_Clk","Vme scalers clock live time projection", 100, 0, 100);  
  // comparison
  TH1D*  hLiveTimeCmcMinusVme = new TH1D("hLiveTime_Cmc-Vme","Camac - Vme Xtrg live time", 1000, -10, 10);
  
  // loop over whole file
  while (start < sentries ){
    cout<<" .. running over scaler segment " << start << " .. " << stop << endl;
    
    // set up event-wise histograms for each segment of [stepsize] events
    stringstream histname;
    histname << "hLiveTime_Cmc_Xtr" << index;
    TH1D*  hLiveTimeCmcXTr = new TH1D(histname.str().c_str(),"CAMAC X-trig live time evolution", stop-start+1, start - 0.5 , stop + 0.5);
    histname.str("");
    histname<< "hLiveTime_Cmc_Clk" << index;
    TH1D*  hLiveTimeCmcClk = new TH1D(histname.str().c_str(),"CAMAC Pulser/clock live time evolution", stop-start+1, start -0.5, stop + 0.5);
    histname.str("");
    histname<< "hLiveTime_Vme_Xtr" << index;
    TH1D*  hLiveTimeVmeXtr = new TH1D(histname.str().c_str(),"Vme xtr scaler live time evolution", stop-start+1, start-0.5, stop + 0.5);
    histname.str("");
    histname<< "hLiveTime_Vme_Clk" << index;
    TH1D*  hLiveTimeVmeClk = new TH1D(histname.str().c_str(),"Vme clock scaler live time evolution", stop-start+1, start-0.5, stop + 0.5);
    histname.str("");
    histname<< "hEvolCmcXtr" << index;
    TH1D*  hEvolCmcXtr = new TH1D(histname.str().c_str(),"Camac xtrg scaler time evolution", stop-start+1, start-0.5, stop + 0.5);
    histname.str("");
    histname<< "hEvolCmcLtch" << index;
    TH1D*  hEvolCmcLtch = new TH1D(histname.str().c_str(),"Camac latch scaler time evolution", stop-start+1, start -0.5, stop + 0.5);
    histname.str("");
    histname<< "hEvolCmcInhClk" << index;
    TH1D*  hEvolCmcInhClk = new TH1D(histname.str().c_str(),"Camac inhibited clock scaler time evolution", stop-start+1, start-0.5, stop + 0.5);
    histname.str("");
    histname<< "hEvolCmcClk" << index;
    TH1D*  hEvolCmcClk = new TH1D(histname.str().c_str(),"Camac clock scaler time evolution", stop-start+1, start -0.5, stop + 0.5);
    // random pin for cross-checks:
    histname.str("");
    histname<< "hEvolCmcScaler12_" << index;
    TH1D*  hEvolCmcScaler12 = new TH1D(histname.str().c_str(),"Camac scaler #12 time evolution", stop-start+1, start-0.5, stop + 0.5);
    // vme single pins raw scalers evolution
    histname.str("");
    histname<< "hEvolVmeXtr" << index;
    TH1D*  hEvolVmeXtr = new TH1D(histname.str().c_str(),"VME xtrg scaler time evolution", stop-start+1, start -0.5, stop + 0.5);
    histname.str("");
    histname<< "hEvolVmeLtch" << index;
    TH1D*  hEvolVmeLtch = new TH1D(histname.str().c_str(),"VME latch scaler time evolution", stop-start+1, start-0.5, stop + 0.5);
    histname.str("");
    histname<< "hEvolVmeClk" << index;
    TH1D*  hEvolVmeClk = new TH1D(histname.str().c_str(),"VME clock scaler time evolution", stop-start+1, start-0.5, stop + 0.5);
    histname.str("");
    histname<< "hEvolVmeInhClk" << index;
    TH1D*  hEvolVmeInhClk = new TH1D(histname.str().c_str(),"VME inh. clock scaler time evolution", stop-start+1, start-0.5, stop + 0.5);
    // vme raw time evolution with offline "clear"
    histname.str("");
    histname<< "hEvolVmeXtr_OffClr" << index;
    TH1D*  hEvolVmeXtrOffClr = new TH1D(histname.str().c_str(),"VME xtrg scaler (offline cleared) time evolution", stop-start+1, start-0.5, stop + 0.5);
    histname.str("");
    histname<< "hEvolVmeLtch_OffClr" << index;
    TH1D*  hEvolVmeLtchOffClr = new TH1D(histname.str().c_str(),"VME latch scaler (offline cleared) time evolution", stop-start+1, start-0.5, stop + 0.5);
    histname.str("");
    histname<< "hEvolVmeClk_OffClr" << index;
    TH1D*  hEvolVmeClkOffClr = new TH1D(histname.str().c_str(),"VME clock scaler (offline cleared) time evolution", stop-start+1, start-0.5, stop + 0.5);
    histname.str("");
    histname<< "hEvolVmeInhClk_OffClr" << index;
    TH1D*  hEvolVmeInhClkOffClr = new TH1D(histname.str().c_str(),"VME inh. clock scaler (offline cleared) time evolution", stop-start+1, start-0.5, stop + 0.5);

    // previous event's scaler values
    const uint NumVmeScalers = 16;
    UInt_t PrevVs0[NumVmeScalers];
    for (int i=0; i<NumVmeScalers; i++){
      PrevVs0[i] = 0;
    }
    
    // loop over current segment
    for ( int event = start; event < stop; event++){
      // check if this is the last event
      if (event == sentries) break;
      TS->GetEntry( event );
      if (scalerevent->GetEventNumber() != (event+1)){
	cout << "Warning: mismatch entry number (" << event << ") and scaler event number (" << scalerevent->GetEventNumber() << ")" << endl;
      }

      // fill evolution histograms of raw values
      hEvolCmcXtr->Fill(scalerevent->GetEventNumber(), scalerevent->Getc1_8(4)); // xtrigs
      hEvolCmcLtch->Fill(scalerevent->GetEventNumber(), scalerevent->Getc1_8(5)); // latches
      hEvolCmcClk->Fill(scalerevent->GetEventNumber(), scalerevent->Getc1_8(6)); // clock
      hEvolCmcInhClk->Fill(scalerevent->GetEventNumber(), scalerevent->Getc1_8(7)); // inh. clock
      hEvolCmcScaler12->Fill(scalerevent->GetEventNumber(), scalerevent->Getc1_8(12));
      // cross-check if bin is already filled -- debug artefacts in histogram
      if (hEvolVmeXtr->GetBinContent(hEvolVmeXtr->GetXaxis()->FindBin(scalerevent->GetEventNumber())) != 0){
	cout << " note: in entry " << event << " histogram hEvol_Vme_Xtr already has the corresponding bin filled -- binning problem?" << endl;
      }
      hEvolVmeXtr->Fill(scalerevent->GetEventNumber(), scalerevent->Getvs0(0)); // xtrigs
      hEvolVmeLtch->Fill(scalerevent->GetEventNumber(), scalerevent->Getvs0(1)); // latches
      hEvolVmeClk->Fill(scalerevent->GetEventNumber(), scalerevent->Getvs0(12)); // clk
      hEvolVmeInhClk->Fill(scalerevent->GetEventNumber(), scalerevent->Getvs0(13)); // inh clk
      hEvolVmeXtrOffClr->Fill(scalerevent->GetEventNumber(), scalerevent->Getvs0(0) - PrevVs0[0]); // xtrigs with "offline clear"
      hEvolVmeLtchOffClr->Fill(scalerevent->GetEventNumber(), scalerevent->Getvs0(1) - PrevVs0[1]); // latches with "offline clear"
      hEvolVmeClkOffClr->Fill(scalerevent->GetEventNumber(), scalerevent->Getvs0(12) - PrevVs0[12]); // clk with "offline clear"
      hEvolVmeInhClkOffClr->Fill(scalerevent->GetEventNumber(), scalerevent->Getvs0(13) - PrevVs0[13]); // inh clk with "offline clear"

      // fill histos (with weight of live time)
      if (scalerevent->Getc1_8(4) == 0){
	cout << " note: latch scaler == 0 in entry " << event << ", skipping." << endl;
      } else {
	double ratio = 100. * ((float) scalerevent->Getc1_8(5))/((float) scalerevent->Getc1_8(4));// latches/xtrigs
	hLiveTimeCmcXTr->Fill(scalerevent->GetEventNumber(), ratio);
	hLiveTimeProjCmcXTr->Fill(ratio);
      }
      if (scalerevent->Getc1_8(6) == 0){
	cout << " note: clock scaler == 0 in entry " << event << ", skipping." << endl;
      } else {
	double ratio = 100. * ((float)scalerevent->Getc1_8(7))/((float)scalerevent->Getc1_8(6)); // inhibited clock/clock
	hLiveTimeCmcClk->Fill(scalerevent->GetEventNumber(), ratio);
	hLiveTimeProjCmcClk->Fill(ratio);
      }
      // VME scalers
      if (scalerevent->Getvs0(0) == 0){
	cout << " note: VME scaler #0 == 0 in entry " << event << ", skipping." << endl;
      } else {
	double ratio = 100. * ((float)scalerevent->Getvs0(1))/((float)scalerevent->Getvs0(0)); // latches/xtrg
	hLiveTimeVmeXtr->Fill(scalerevent->GetEventNumber(), ratio);
	hLiveTimeProjVmeXtr->Fill(ratio);
      }
      if (scalerevent->Getvs0(12) == 0){
	cout << " note: VME scaler #12 == 0 in entry " << event << ", skipping." << endl;
      } else {
	double ratio = 100. * ((float)scalerevent->Getvs0(13))/((float)scalerevent->Getvs0(12)); // inh. clk/clk
	hLiveTimeVmeClk->Fill(scalerevent->GetEventNumber(), ratio);
	hLiveTimeProjVmeClk->Fill(ratio);
      }
      // compare the two live times
      if (scalerevent->Getc1_8(4) != 0 && scalerevent->Getc1_8(6) != 0){
	double ratioXTr = 100. * ((float) scalerevent->Getc1_8(5))/((float) scalerevent->Getc1_8(4));// latches/xtrg
	double ratioClk = 100. * ((float)scalerevent->Getc1_8(7))/((float)scalerevent->Getc1_8(6)); // inhibited clock/clock
	hLiveTimeCmcClkMinusXTr->Fill(ratioClk-ratioXTr);
      }
      // compare the two live times, CAMAC vs VME
      if (scalerevent->Getc1_8(4) != 0 && scalerevent->Getvs0(0) != 0){
	double ratioCmc = 100. * ((float) scalerevent->Getc1_8(5))/((float) scalerevent->Getc1_8(4));// latches/xtrg
	double ratioVme = 100. * ((float)scalerevent->Getvs0(1))/((float)scalerevent->Getvs0(0)); // inhibited clock/clock
	hLiveTimeCmcMinusVme->Fill(ratioCmc-ratioVme);
      }

      // save the current scalers
      for (int i=0; i<NumVmeScalers; i++){
	PrevVs0[i] = scalerevent->Getvs0(i);
      }

    }
    // increase counters
    index++;
    start = stop;
    stop += stepsize;
  } // while (start < sentries)
  infile->Close();  
  output->Write();
 
  delete output;
  delete infile;
  delete scalerevent;
 
}//close main --
