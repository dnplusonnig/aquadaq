  
//Header files that must be included to run properly

#include <stdio.h>
#include <stdlib.h>
#include <sstream>
#include <iostream>
#include <fstream>
#include "TApplication.h"
#include "TCanvas.h"
#include "TSystem.h"
#include "TStyle.h"
#include "TFile.h"
#include "TTree.h"
#include "TF1.h"
#include "TH1.h"
#include "TH1F.h"
#include "TH1D.h"
#include "TH2F.h"
#include "TROOT.h"
#include "TCutG.h"
#include "TClonesArray.h"
#include "TMath.h"
#include "XEvent.h"
#include "ScalerEvent.h"
#include "XHist.h"
#include "ScalerHist.h"
#include "TRandom3.h"
#include "TSpectrum.h"


using namespace std;

double c = 0.299792458; //c in m/ns

double fit_bit(double *x, double *par) {

  
  double norm  = par[0];
  double mean  = par[1];
  double sigma = par[2];

  double  result = norm*TMath::Gaus(x[0],mean,sigma);  
  return result;
}

double fit_background(double *x, double *par) {
  
  double norm  = par[0];

  double  result = norm;  
  return result;
}


int main(int argc, char *argv[]){//open main ++
  
  if (argc != 2 ){
    cout << "Error: need file name argument. Please specify a ROOT file which should be analyiezed. " << endl;
    return 1;
  }
  
  string infilename(argv[1]);
  TFile *infile = new TFile(infilename.c_str(),"READ");    
  if (!infile->IsOpen()){
    cout << " Error opening file: " << infilename << endl;
    return 1;
  }

  TROOT simple("simple", "offline");
  
  // Create a new rootfile for the results
  string outfilename = infilename.substr(0,infilename.find_last_of("."));
  outfilename+="_output.root";
  TFile *output = new TFile(outfilename.c_str(), "RECREATE");
  
  
  XEvent *xevent = new XEvent();
  ScalerEvent *scalerevent = new ScalerEvent();
  
  TTree *TX;
  TBranch *xbranch;

  TTree *TS;
  TBranch *sbranch;
  
  ofstream nentries_file;
  nentries_file.open ("numbers.txt");
  
  double s = 1.1;  // distance detector center  to sorce center in meter

  double pedestal_short[4] = {88.,31.,0,0}; // detector 0, 1, 2, 3
  double pedestal_long[4] = {34.,34.,0,0}; // detector 0, 1, 2, 3
  
  double bit_high[4] = {1193.,985.,0,0}; // detector 0, 1, 2, 3
  double bit_low[4] = {1179.,970.,0,0}; // detector 0, 1, 2, 3
  
  double offset[4] = {0.,-0.22,9.40,36.18}; // detector 0, 1, 2, 3
  
  
  
  char hname[256], htitle[256];
  

  //VME based histograms
  
  TH1F *hvq[2*ENVqdc];    // exp qdc
 
  TH1F *hvt[64];    // exp multi tdc
  
  for(int i = 0; i < 2*ENVqdc ; i++) 
    {
      sprintf( hname, "hvq_%d", i);
      sprintf( htitle, "qdc  hvq_%d", i);
      hvq[i] = new TH1F( hname, htitle, 5000, 0, 4999);
      // ListOfXHistos->Add(hvq[i]);
    }
  
  for(int i = 0; i < 64 ; i++) 
    {
      sprintf( hname, "hvt_%d", i);
      sprintf( htitle, "multi-tdc hvt_%d", i);
      hvt[i] = new TH1F( hname, htitle, 2500, -2000, 8000);
      // ListOfXHistos->Add(hvt[i]);
    }

  
  // Analysis histograms


  //Camac based
  TH1F *he_short[4];
  
  TH1F *he_short_neutron[4];
  
  TH1F *he_long[4];
  
  TH1F *he_long_neutron[4];
  
  TH2F *hPSD[4];

  TH2F *hPS_TOF[4];
  
  TH1F *he_tdc[4];
  
  TH1F *hTOF_tdc[4];
  
  TH1F *hTOF_tdc_neutron[4];

  TH1F *hTOF_tdc_neutron_background[4]; 
  
  TH1F *hTOF[4];

  TH1F *hTOF_neutron[4];

  TH1F *hTOF_neutron_background[4]; 

  TH1F *hc1_4_[4]; 
  
  TH1F *hEkin[4];

  TH1F *hEkin_background[4]; 

  //VME based
  TH1F *hve_short[4];
  
  TH1F *hve_short_neutron[4];
  
  TH1F *hve_long[4];
  
  TH1F *hve_long_neutron[4];
  
  TH2F *hvPSD[4];

  TH2F *hvPS_TOF[4];
  
  TH1F *hve_tdc[4];
  
  TH1F *hvTOF_tdc[4];
  
  TH1F *hvTOF_tdc_neutron[4];

  TH1F *hvTOF_tdc_neutron_background[4]; 
  
  TH1F *hvTOF[4];

  TH1F *hvTOF_neutron[4];

  TH1F *hvTOF_neutron_background[4]; 
  
  TH1F *hvEkin[4];

  TH1F *hvEkin_background[4]; 

  // Camac based
  
  for ( int i = 0; i < 4; i++){

    sprintf( hname, "hc1_4_%d",i);
    hc1_4_[i]= (TH1F*)infile->Get(hname);

    sprintf( hname, "he_short%d",i);
    sprintf( htitle, "E short gate %d",i);
    he_short[i] = new TH1F ( hname, htitle, 2048, 0, 2048);
    he_short[i] -> GetXaxis()->CenterTitle();
    he_short[i] -> GetXaxis()->SetTitle("QDC [arb. units]");
    he_short[i] -> GetYaxis()->CenterTitle();
    he_short[i] -> GetYaxis()->SetTitle("counts");
    
    sprintf( hname, "he_long%d",i);
    sprintf( htitle, "E long gate test %d",i);
    he_long[i] = new TH1F ( hname, htitle, 2048, 0, 2048);
    he_long[i] -> GetXaxis()->CenterTitle();
    he_long[i] -> GetXaxis()->SetTitle("QDC [arb. units]");
    he_long[i] -> GetYaxis()->CenterTitle();
    he_long[i] -> GetYaxis()->SetTitle("counts");
    
    sprintf( hname, "he_short_neutron%d",i);
    sprintf( htitle, "E short gate %d",i);
    he_short_neutron[i] = new TH1F ( hname, htitle, 2048, 0, 2048);
    he_short_neutron[i] -> GetXaxis()->CenterTitle();
    he_short_neutron[i] -> GetXaxis()->SetTitle("QDC [arb. units]");
    he_short_neutron[i] -> GetYaxis()->CenterTitle();
    he_short_neutron[i] -> GetYaxis()->SetTitle("counts");
    
    sprintf( hname, "he_long_neutron%d",i);
    sprintf( htitle, "E long gate test %d",i);
    he_long_neutron[i] = new TH1F ( hname, htitle, 2048, 0, 2048);
    he_long_neutron[i] -> GetXaxis()->CenterTitle();
    he_long_neutron[i] -> GetXaxis()->SetTitle("QDC [arb. units]");
    he_long_neutron[i] -> GetYaxis()->CenterTitle();
    he_long_neutron[i] -> GetYaxis()->SetTitle("counts");
    
    sprintf( hname, "hPSD%d",i);
    sprintf( htitle, "PSD %d",i);
    hPSD[i] = new TH2F ( hname, htitle, 2048, 0, 2048, 2048, 0, 1);
    hPSD[i] -> GetXaxis()->CenterTitle();
    hPSD[i] -> GetXaxis()->SetTitle("QDC [arb. units]");
    hPSD[i] -> GetYaxis()->CenterTitle();
    hPSD[i] -> GetYaxis()->SetTitle("PS [arb. units]");
    
    sprintf( hname, "he_tdc%d",i);
    sprintf( htitle, "Bit TDC %d",i);
    he_tdc[i] = new TH1F ( hname, htitle, 2048, 0, 2048);
    he_tdc[i] -> GetXaxis()->CenterTitle();
    he_tdc[i] -> GetXaxis()->SetTitle("TDC [arb. units]");
    he_tdc[i] -> GetYaxis()->CenterTitle();
    he_tdc[i] -> GetYaxis()->SetTitle("counts");                                
    
    sprintf( hname, "hTOF_tdc%d",i);
    sprintf( htitle, "TOF TDC %d",i);
    hTOF_tdc[i]= new TH1F ( hname, htitle, 2048, 0, 2048);
    hTOF_tdc[i]->GetXaxis()->CenterTitle();
    hTOF_tdc[i]->GetXaxis()->SetTitle("TDC [arb. units]");
    hTOF_tdc[i]->GetYaxis()->CenterTitle();
    hTOF_tdc[i]->GetYaxis()->SetTitle("counts");
    
    sprintf( hname, "hTOF_tdc_neutron%d",i);
    sprintf( htitle, "TOF TDC Neutron %d",i);
    hTOF_tdc_neutron[i]= new TH1F ( hname, htitle, 2048, 0, 2048);
    hTOF_tdc_neutron[i]->GetXaxis()->CenterTitle();
    hTOF_tdc_neutron[i]->GetXaxis()->SetTitle("TDC [arb. units]");
    hTOF_tdc_neutron[i]->GetYaxis()->CenterTitle();
    hTOF_tdc_neutron[i]->GetYaxis()->SetTitle("counts");

    sprintf( hname, "hTOF_tdc_neutron_background%d",i);
    sprintf( htitle, "TOF TDC Neutron Background %d",i);
    hTOF_tdc_neutron_background[i]= new TH1F ( hname, htitle, 2048, 0, 2048);
    hTOF_tdc_neutron_background[i]->GetXaxis()->CenterTitle();
    hTOF_tdc_neutron_background[i]->GetXaxis()->SetTitle("TDC [arb. units]");
    hTOF_tdc_neutron_background[i]->GetYaxis()->CenterTitle();
    hTOF_tdc_neutron_background[i]->GetYaxis()->SetTitle("counts");
    
                                                
    sprintf( hname, "hTOF%d",i);
    sprintf( htitle, "TOF %d",i);
    hTOF[i]= new TH1F ( hname, htitle, 2048, -50, 500 );
    hTOF[i]->GetXaxis()->CenterTitle();
    hTOF[i]->GetXaxis()->SetTitle("ToF [ns]");
    hTOF[i]->GetYaxis()->CenterTitle();
    hTOF[i]->GetYaxis()->SetTitle("counts");

    sprintf( hname, "hPS_TOF%d",i);
    sprintf( htitle, "PS vs. TOF %d",i);
    hPS_TOF[i] = new TH2F ( hname, htitle, 1024, 0, 500, 512, 0, 1);
    hPS_TOF[i] -> GetXaxis()->CenterTitle();
    hPS_TOF[i] -> GetXaxis()->SetTitle("TOF [ns]");
    hPS_TOF[i] -> GetYaxis()->CenterTitle();
    hPS_TOF[i] -> GetYaxis()->SetTitle("PS [arb. units]");
                                                
    sprintf( hname, "hTOF_neutron%d",i);
    sprintf( htitle, "TOF Neutron %d",i);
    hTOF_neutron[i]= new TH1F ( hname, htitle, 2048, -50, 500 );
    hTOF_neutron[i]->GetXaxis()->CenterTitle();
    hTOF_neutron[i]->GetXaxis()->SetTitle("ToF [ns]");
    hTOF_neutron[i]->GetYaxis()->CenterTitle();
    hTOF_neutron[i]->GetYaxis()->SetTitle("counts");

    sprintf( hname, "hTOF_neutron_background%d",i);
    sprintf( htitle, "TOF Neutron Background %d",i);
    hTOF_neutron_background[i]= new TH1F ( hname, htitle, 2048, -50, 500 );
    hTOF_neutron_background[i]->GetXaxis()->CenterTitle();
    hTOF_neutron_background[i]->GetXaxis()->SetTitle("ToF [ns]");
    hTOF_neutron_background[i]->GetYaxis()->CenterTitle();
    hTOF_neutron_background[i]->GetYaxis()->SetTitle("counts");

    sprintf( hname, "hEkin%d",i);
    sprintf( htitle, "Ekin %d",i);
    hEkin[i]= new TH1F ( hname, htitle, 50, 0, 10. );
    hEkin[i]->GetXaxis()->CenterTitle();
    hEkin[i]->GetXaxis()->SetTitle("T [MeV]");
    hEkin[i]->GetYaxis()->CenterTitle();
    hEkin[i]->GetYaxis()->SetTitle("counts");
    
    sprintf( hname, "hEkin_background%d",i);
    sprintf( htitle, "Ekin Background%d",i);
    hEkin_background[i]= new TH1F ( hname, htitle, 50, 0, 10. );
    hEkin_background[i]->GetXaxis()->CenterTitle();
    hEkin_background[i]->GetXaxis()->SetTitle("T [MeV]");
    hEkin_background[i]->GetYaxis()->CenterTitle();
    hEkin_background[i]->GetYaxis()->SetTitle("counts");

  }


  //VME based

    
  for ( int i = 0; i < 4; i++){


    sprintf( hname, "hve_short%d",i);
    sprintf( htitle, "E short gate %d",i);
    hve_short[i] = new TH1F ( hname, htitle, 5000, 0, 5000);
    hve_short[i] -> GetXaxis()->CenterTitle();
    hve_short[i] -> GetXaxis()->SetTitle("QDC [arb. units]");
    hve_short[i] -> GetYaxis()->CenterTitle();
    hve_short[i] -> GetYaxis()->SetTitle("counts");
    
    sprintf( hname, "hve_long%d",i);
    sprintf( htitle, "E long gate test %d",i);
    hve_long[i] = new TH1F ( hname, htitle, 5000, 0, 5000);
    hve_long[i] -> GetXaxis()->CenterTitle();
    hve_long[i] -> GetXaxis()->SetTitle("QDC [arb. units]");
    hve_long[i] -> GetYaxis()->CenterTitle();
    hve_long[i] -> GetYaxis()->SetTitle("counts");
    
    sprintf( hname, "hve_short_neutron%d",i);
    sprintf( htitle, "E short gate %d",i);
    hve_short_neutron[i] = new TH1F ( hname, htitle, 5000, 0, 5000);
    hve_short_neutron[i] -> GetXaxis()->CenterTitle();
    hve_short_neutron[i] -> GetXaxis()->SetTitle("QDC [arb. units]");
    hve_short_neutron[i] -> GetYaxis()->CenterTitle();
    hve_short_neutron[i] -> GetYaxis()->SetTitle("counts");
    
    sprintf( hname, "hve_long_neutron%d",i);
    sprintf( htitle, "E long gate test %d",i);
    hve_long_neutron[i] = new TH1F ( hname, htitle, 5000, 0, 5000);
    hve_long_neutron[i] -> GetXaxis()->CenterTitle();
    hve_long_neutron[i] -> GetXaxis()->SetTitle("QDC [arb. units]");
    hve_long_neutron[i] -> GetYaxis()->CenterTitle();
    hve_long_neutron[i] -> GetYaxis()->SetTitle("counts");
    
    sprintf( hname, "hvPSD%d",i);
    sprintf( htitle, "PSD %d",i);
    hvPSD[i] = new TH2F ( hname, htitle, 5000, 0, 5000, 2048, 0, 1);
    hvPSD[i] -> GetXaxis()->CenterTitle();
    hvPSD[i] -> GetXaxis()->SetTitle("QDC [arb. units]");
    hvPSD[i] -> GetYaxis()->CenterTitle();
    hvPSD[i] -> GetYaxis()->SetTitle("PS [arb. units]");
    
    sprintf( hname, "hve_tdc%d",i);
    sprintf( htitle, "Bit TDC %d",i);
    hve_tdc[i] = new TH1F ( hname, htitle, 2500, -2000, 8000);
    hve_tdc[i] -> GetXaxis()->CenterTitle();
    hve_tdc[i] -> GetXaxis()->SetTitle("TDC [arb. units]");
    hve_tdc[i] -> GetYaxis()->CenterTitle();
    hve_tdc[i] -> GetYaxis()->SetTitle("counts");                                
    
    sprintf( hname, "hvTOF_tdc%d",i);
    sprintf( htitle, "TOF TDC %d",i);
    hvTOF_tdc[i]= new TH1F ( hname, htitle, 2500, -2000, 8000);
    hvTOF_tdc[i]->GetXaxis()->CenterTitle();
    hvTOF_tdc[i]->GetXaxis()->SetTitle("TDC [arb. units]");
    hvTOF_tdc[i]->GetYaxis()->CenterTitle();
    hvTOF_tdc[i]->GetYaxis()->SetTitle("counts");
    
    sprintf( hname, "hvTOF_tdc_neutron%d",i);
    sprintf( htitle, "TOF TDC Neutron %d",i);
    hvTOF_tdc_neutron[i]= new TH1F ( hname, htitle, 2048, 0, 2048);
    hvTOF_tdc_neutron[i]->GetXaxis()->CenterTitle();
    hvTOF_tdc_neutron[i]->GetXaxis()->SetTitle("TDC [arb. units]");
    hvTOF_tdc_neutron[i]->GetYaxis()->CenterTitle();
    hvTOF_tdc_neutron[i]->GetYaxis()->SetTitle("counts");

    sprintf( hname, "hvTOF_tdc_neutron_background%d",i);
    sprintf( htitle, "TOF TDC Neutron Background %d",i);
    hvTOF_tdc_neutron_background[i]= new TH1F ( hname, htitle, 2048, 0, 2048);
    hvTOF_tdc_neutron_background[i]->GetXaxis()->CenterTitle();
    hvTOF_tdc_neutron_background[i]->GetXaxis()->SetTitle("TDC [arb. units]");
    hvTOF_tdc_neutron_background[i]->GetYaxis()->CenterTitle();
    hvTOF_tdc_neutron_background[i]->GetYaxis()->SetTitle("counts");
    
                                                
    sprintf( hname, "hvTOF%d",i);
    sprintf( htitle, "TOF %d",i);
    hvTOF[i]= new TH1F ( hname, htitle, 2048, -50, 500 );
    hvTOF[i]->GetXaxis()->CenterTitle();
    hvTOF[i]->GetXaxis()->SetTitle("ToF [ns]");
    hvTOF[i]->GetYaxis()->CenterTitle();
    hvTOF[i]->GetYaxis()->SetTitle("counts");

    sprintf( hname, "hvPS_TOF%d",i);
    sprintf( htitle, "PS vs. TOF %d",i);
    hvPS_TOF[i] = new TH2F ( hname, htitle, 2048, -50, 50, 2048, 0, 1);
    hvPS_TOF[i] -> GetXaxis()->CenterTitle();
    hvPS_TOF[i] -> GetXaxis()->SetTitle("TOF [ns]");
    hvPS_TOF[i] -> GetYaxis()->CenterTitle();
    hvPS_TOF[i] -> GetYaxis()->SetTitle("PS [arb. units]");
                                                
    sprintf( hname, "hvTOF_neutron%d",i);
    sprintf( htitle, "TOF Neutron %d",i);
    hvTOF_neutron[i]= new TH1F ( hname, htitle, 2048, -50, 500 );
    hvTOF_neutron[i]->GetXaxis()->CenterTitle();
    hvTOF_neutron[i]->GetXaxis()->SetTitle("ToF [ns]");
    hvTOF_neutron[i]->GetYaxis()->CenterTitle();
    hvTOF_neutron[i]->GetYaxis()->SetTitle("counts");

    sprintf( hname, "hvTOF_neutron_background%d",i);
    sprintf( htitle, "TOF Neutron Background %d",i);
    hvTOF_neutron_background[i]= new TH1F ( hname, htitle, 2048, -50, 500 );
    hvTOF_neutron_background[i]->GetXaxis()->CenterTitle();
    hvTOF_neutron_background[i]->GetXaxis()->SetTitle("ToF [ns]");
    hvTOF_neutron_background[i]->GetYaxis()->CenterTitle();
    hvTOF_neutron_background[i]->GetYaxis()->SetTitle("counts");

    sprintf( hname, "hvEkin%d",i);
    sprintf( htitle, "Ekin %d",i);
    hvEkin[i]= new TH1F ( hname, htitle, 50, 0, 10. );
    hvEkin[i]->GetXaxis()->CenterTitle();
    hvEkin[i]->GetXaxis()->SetTitle("T [MeV]");
    hvEkin[i]->GetYaxis()->CenterTitle();
    hvEkin[i]->GetYaxis()->SetTitle("counts");
    
    sprintf( hname, "hvEkin_background%d",i);
    sprintf( htitle, "Ekin Background%d",i);
    hvEkin_background[i]= new TH1F ( hname, htitle, 50, 0, 10. );
    hvEkin_background[i]->GetXaxis()->CenterTitle();
    hvEkin_background[i]->GetXaxis()->SetTitle("T [MeV]");
    hvEkin_background[i]->GetYaxis()->CenterTitle();
    hvEkin_background[i]->GetYaxis()->SetTitle("counts");

  }


  // histograms showing the distribution of live times
  // CAMAC-based
  TH1D*  hLiveTimeProjCmcXTr = new TH1D("hLiveTime_Proj_Cmc_Xtr","X-trig live time projection", 100, 0, 100);
  TH1D*  hLiveTimeProjCmcClk = new TH1D("hLiveTime_Proj_Cmc_Clk","Pulser/clock live time projection", 100, 0, 100);
  TH1D*  hLiveTimeCmcClkMinusXTr = new TH1D("hLiveTime_Cmc_Clk-XTr","Pulser/clock - X-trig live time", 1000, -10, 10);
  // VME-based
  TH1D*  hLiveTimeProjVmeXtr = new TH1D("hLiveTime_Proj_Vme_Xtr","Vme scalers Xtrg live time projection", 100, 0, 100);
  TH1D*  hLiveTimeProjVmeClk = new TH1D("hLiveTime_Proj_Vme_Clk","Vme scalers clock live time projection", 100, 0, 100);  
  // comparison
  TH1D*  hLiveTimeCmcMinusVme = new TH1D("hLiveTime_Cmc-Vme","Camac - Vme Xtrg live time", 1000, -10, 10);

  
  //----------------------------------------------------------------------------------
  // Find the position of the selfe timed bit in the bit TDC



    for ( int i = 0; i < 4; i++){

    double par[3];

    TSpectrum *spec;

    spec =  new TSpectrum(2,1);


    Int_t nfound = spec->Search(hc1_4_[i],10,"",100);  
    float *xpeaks = spec->GetPositionX();
    double xp = xpeaks[0];
    int bin = hc1_4_[i]->GetXaxis()->FindBin(xp);
    double yp = hc1_4_[i] ->GetBinContent(bin);
    
    par[0] = yp;
    par[1] = xp;
    par[2] = 5.;
  
    TF1 *fit = new TF1("fit",fit_bit,0,1000,3);
    fit->SetParameters(par);
    hc1_4_[i]->Fit("fit", "q");
    fit->GetParameters(par);
    bit_low[i] = par[1]-par[2];
    bit_high[i] =par[1]+par[2];

    cout <<"Low bitTDC Channel Numerber for detector "<< i<<": "<< bit_low[i]<< endl; 
    cout <<"High bitTDC Channel Numerber for detector "<< i<<": "<<  bit_high[i]<< endl; 
  }
  

  /*----------------------------------------------------------------------------------*/
  /*Scalers*/
  
  TH1F *scaler_entries = new TH1F("scaler_entries","scaler_entries",1,0,1);
  TH1F *free_scalers = new TH1F("free_scalers","Free Scaler counts",64,0,64);
  
  /*----------------------------------------------------------------------------------*/ 
  int run = 0;
  int event = 0;
  int nentries = 0;
  int sentries = 0;
  char *temp;
  
  
  
  
  
  
  printf("------------------------------------------------------------\n");
  
  printf("------------------------------------------------------------\n");  
  fprintf( stderr, "Processing %s\n", infilename.c_str());
  
  
  
  // 
  //-------------------Loop over events----------------------
  //
  
  
  // Scaler
  //
  
  TS =  (TTree*) infile->Get("TS");
  sbranch  = TS->GetBranch("scalerbranch");
  sbranch->SetAddress(&scalerevent);
  sentries = TS->GetEntries();
  cout<<"Number of scaler entries "<<sentries<<endl;
  nentries_file<<"Scalers "<<sentries<<endl;

  int index = 0;
  int start = 0;
  const int stepsize = 10000;
  int stop = start + stepsize;
  
  for ( event = 0; event < sentries; event++) {           
    TS->GetEntry(event);       
    //scalerevent->PrintScalers();
  }
  scaler_entries->SetBinContent(1,sentries);
  
  
  for (int j=0;j<32;j++){
    free_scalers->SetBinContent(j+1,scalerevent->Getvs0(j));
    free_scalers->SetBinContent(j+1+32,scalerevent->Getvs1(j));
  }

  // loop over whole file
  while (start < sentries ){
    cout<<" .. running over scaler segment " << start << " .. " << stop << endl;
    
    // set up event-wise histograms for each segment of [stepsize] events
    stringstream histname;
    histname << "hLiveTime_Cmc_Xtr" << index;
    TH1D*  hLiveTimeCmcXTr = new TH1D(histname.str().c_str(),"CAMAC X-trig live time evolution", stop-start+1, start - 0.5 , stop + 0.5);
    histname.str("");
    histname<< "hLiveTime_Cmc_Clk" << index;
    TH1D*  hLiveTimeCmcClk = new TH1D(histname.str().c_str(),"CAMAC Pulser/clock live time evolution", stop-start+1, start -0.5, stop + 0.5);
    histname.str("");
    histname<< "hLiveTime_Vme_Xtr" << index;
    TH1D*  hLiveTimeVmeXtr = new TH1D(histname.str().c_str(),"Vme xtr scaler live time evolution", stop-start+1, start-0.5, stop + 0.5);
    histname.str("");
    histname<< "hLiveTime_Vme_Clk" << index;
    TH1D*  hLiveTimeVmeClk = new TH1D(histname.str().c_str(),"Vme clock scaler live time evolution", stop-start+1, start-0.5, stop + 0.5);
    histname.str("");
    histname<< "hEvolCmcXtr" << index;
    TH1D*  hEvolCmcXtr = new TH1D(histname.str().c_str(),"Camac xtrg scaler time evolution", stop-start+1, start-0.5, stop + 0.5);
    histname.str("");
    histname<< "hEvolCmcLtch" << index;
    TH1D*  hEvolCmcLtch = new TH1D(histname.str().c_str(),"Camac latch scaler time evolution", stop-start+1, start -0.5, stop + 0.5);
    histname.str("");
    histname<< "hEvolCmcInhClk" << index;
    TH1D*  hEvolCmcInhClk = new TH1D(histname.str().c_str(),"Camac inhibited clock scaler time evolution", stop-start+1, start-0.5, stop + 0.5);
    histname.str("");
    histname<< "hEvolCmcClk" << index;
    TH1D*  hEvolCmcClk = new TH1D(histname.str().c_str(),"Camac clock scaler time evolution", stop-start+1, start -0.5, stop + 0.5);
    // random pin for cross-checks:
    histname.str("");
    histname<< "hEvolCmcScaler12_" << index;
    TH1D*  hEvolCmcScaler12 = new TH1D(histname.str().c_str(),"Camac scaler #12 time evolution", stop-start+1, start-0.5, stop + 0.5);
    // vme single pins raw scalers evolution
    histname.str("");
    histname<< "hEvolVmeXtr" << index;
    TH1D*  hEvolVmeXtr = new TH1D(histname.str().c_str(),"VME xtrg scaler time evolution", stop-start+1, start -0.5, stop + 0.5);
    histname.str("");
    histname<< "hEvolVmeLtch" << index;
    TH1D*  hEvolVmeLtch = new TH1D(histname.str().c_str(),"VME latch scaler time evolution", stop-start+1, start-0.5, stop + 0.5);
    histname.str("");
    histname<< "hEvolVmeClk" << index;
    TH1D*  hEvolVmeClk = new TH1D(histname.str().c_str(),"VME clock scaler time evolution", stop-start+1, start-0.5, stop + 0.5);
    histname.str("");
    histname<< "hEvolVmeInhClk" << index;
    TH1D*  hEvolVmeInhClk = new TH1D(histname.str().c_str(),"VME inh. clock scaler time evolution", stop-start+1, start-0.5, stop + 0.5);
    // vme raw time evolution with offline "clear"
    histname.str("");
    histname<< "hEvolVmeXtr_OffClr" << index;
    TH1D*  hEvolVmeXtrOffClr = new TH1D(histname.str().c_str(),"VME xtrg scaler (offline cleared) time evolution", stop-start+1, start-0.5, stop + 0.5);
    histname.str("");
    histname<< "hEvolVmeLtch_OffClr" << index;
    TH1D*  hEvolVmeLtchOffClr = new TH1D(histname.str().c_str(),"VME latch scaler (offline cleared) time evolution", stop-start+1, start-0.5, stop + 0.5);
    histname.str("");
    histname<< "hEvolVmeClk_OffClr" << index;
    TH1D*  hEvolVmeClkOffClr = new TH1D(histname.str().c_str(),"VME clock scaler (offline cleared) time evolution", stop-start+1, start-0.5, stop + 0.5);
    histname.str("");
    histname<< "hEvolVmeInhClk_OffClr" << index;
    TH1D*  hEvolVmeInhClkOffClr = new TH1D(histname.str().c_str(),"VME inh. clock scaler (offline cleared) time evolution", stop-start+1, start-0.5, stop + 0.5);

    // previous event's scaler values
    const uint NumVmeScalers = 16;
    UInt_t PrevVs0[NumVmeScalers];
    for (int i=0; i<NumVmeScalers; i++){
      PrevVs0[i] = 0;
    }
    
    // loop over current segment
    for ( int event = start; event < stop; event++){
      // check if this is the last event
      if (event == sentries) break;
      TS->GetEntry( event );
      if (scalerevent->GetEventNumber() != (event+1)){
	cout << "Warning: mismatch entry number (" << event << ") and scaler event number (" << scalerevent->GetEventNumber() << ")" << endl;
      }
      
      // fill evolution histograms of raw values
      hEvolCmcXtr->Fill(scalerevent->GetEventNumber(), scalerevent->Getc1_8(4)); // xtrigs
      hEvolCmcLtch->Fill(scalerevent->GetEventNumber(), scalerevent->Getc1_8(5)); // latches
      hEvolCmcClk->Fill(scalerevent->GetEventNumber(), scalerevent->Getc1_8(6)); // clock
      hEvolCmcInhClk->Fill(scalerevent->GetEventNumber(), scalerevent->Getc1_8(7)); // inh. clock
      hEvolCmcScaler12->Fill(scalerevent->GetEventNumber(), scalerevent->Getc1_8(12));
      // cross-check if bin is already filled -- debug artefacts in histogram
      if (hEvolVmeXtr->GetBinContent(hEvolVmeXtr->GetXaxis()->FindBin(scalerevent->GetEventNumber())) != 0){
	cout << " note: in entry " << event << " histogram hEvol_Vme_Xtr already has the corresponding bin filled -- binning problem?" << endl;
      }
      hEvolVmeXtr->Fill(scalerevent->GetEventNumber(), scalerevent->Getvs0(0)); // xtrigs
      hEvolVmeLtch->Fill(scalerevent->GetEventNumber(), scalerevent->Getvs0(1)); // latches
      hEvolVmeClk->Fill(scalerevent->GetEventNumber(), scalerevent->Getvs0(12)); // clk
      hEvolVmeInhClk->Fill(scalerevent->GetEventNumber(), scalerevent->Getvs0(13)); // inh clk
      hEvolVmeXtrOffClr->Fill(scalerevent->GetEventNumber(), scalerevent->Getvs0(0) - PrevVs0[0]); // xtrigs with "offline clear"
      hEvolVmeLtchOffClr->Fill(scalerevent->GetEventNumber(), scalerevent->Getvs0(1) - PrevVs0[1]); // latches with "offline clear"
      hEvolVmeClkOffClr->Fill(scalerevent->GetEventNumber(), scalerevent->Getvs0(12) - PrevVs0[12]); // clk with "offline clear"
      hEvolVmeInhClkOffClr->Fill(scalerevent->GetEventNumber(), scalerevent->Getvs0(13) - PrevVs0[13]); // inh clk with "offline clear"

      // fill histos (with weight of live time)
      if (scalerevent->Getc1_8(4) == 0){
	cout << " note: latch scaler == 0 in entry " << event << ", skipping." << endl;
      } else {
	double ratio = 100. * ((float) scalerevent->Getc1_8(5))/((float) scalerevent->Getc1_8(4));// latches/xtrigs
	hLiveTimeCmcXTr->Fill(scalerevent->GetEventNumber(), ratio);
	hLiveTimeProjCmcXTr->Fill(ratio);
      }
      if (scalerevent->Getc1_8(6) == 0){
	cout << " note: clock scaler == 0 in entry " << event << ", skipping." << endl;
      } else {
	double ratio = 100. * ((float)scalerevent->Getc1_8(7))/((float)scalerevent->Getc1_8(6)); // inhibited clock/clock
	hLiveTimeCmcClk->Fill(scalerevent->GetEventNumber(), ratio);
	hLiveTimeProjCmcClk->Fill(ratio);
      }
      // VME scalers
      if (scalerevent->Getvs0(0) == 0){
	cout << " note: VME scaler #0 == 0 in entry " << event << ", skipping." << endl;
      } else {
	double ratio = 100. * ((float)scalerevent->Getvs0(1))/((float)scalerevent->Getvs0(0)); // latches/xtrg
	hLiveTimeVmeXtr->Fill(scalerevent->GetEventNumber(), ratio);
	hLiveTimeProjVmeXtr->Fill(ratio);
      }
      if (scalerevent->Getvs0(12) == 0){
	cout << " note: VME scaler #12 == 0 in entry " << event << ", skipping." << endl;
      } else {
	double ratio = 100. * ((float)scalerevent->Getvs0(13))/((float)scalerevent->Getvs0(12)); // inh. clk/clk
	hLiveTimeVmeClk->Fill(scalerevent->GetEventNumber(), ratio);
	hLiveTimeProjVmeClk->Fill(ratio);
      }
      // compare the two live times
      if (scalerevent->Getc1_8(4) != 0 && scalerevent->Getc1_8(6) != 0){
	double ratioXTr = 100. * ((float) scalerevent->Getc1_8(5))/((float) scalerevent->Getc1_8(4));// latches/xtrg
	double ratioClk = 100. * ((float)scalerevent->Getc1_8(7))/((float)scalerevent->Getc1_8(6)); // inhibited clock/clock
	hLiveTimeCmcClkMinusXTr->Fill(ratioClk-ratioXTr);
      }
      // compare the two live times, CAMAC vs VME
      if (scalerevent->Getc1_8(4) != 0 && scalerevent->Getvs0(0) != 0){
	double ratioCmc = 100. * ((float) scalerevent->Getc1_8(5))/((float) scalerevent->Getc1_8(4));// latches/xtrg
	double ratioVme = 100. * ((float)scalerevent->Getvs0(1))/((float)scalerevent->Getvs0(0)); // inhibited clock/clock
	hLiveTimeCmcMinusVme->Fill(ratioCmc-ratioVme);
      }

      // save the current scalers
      for (int i=0; i<NumVmeScalers; i++){
	PrevVs0[i] = scalerevent->Getvs0(i);
      }
      
    }
    // increase counters
    index++;
    start = stop;
    stop += stepsize;
  } // while (start < sentries)
  

  
  //
  // X
  //
  TX = (TTree*)infile->Get("TX");
  xbranch  = TX->GetBranch("xbranch");
  xbranch->SetAddress(&xevent);  
  nentries = TX->GetEntries();
  
  printf("------------------------------------------------------------\n");
  
  cout<<"Number of X entries "<<nentries<<endl;
  nentries_file<<"Xevents "<<nentries<<endl;
  
  printf("------------------------------------------------------------\n");
  


  for ( event = 0; event < 2000000; event++) {//Loop over X events ++  

    TX->GetEntry( event);
  
    
    if ( event%1000000 == 0) {
      printf("\n \n x entry No. %d\n", event);
    }

    // loop over all four (possibly installed) detectors
    for(int i=0; i<4; i++) {
      // check if this detector was active (from self-triggered TDC)
      he_tdc[i]->Fill(xevent->Getc1_4(i));

      // use parameters from gaussian fit (+/- width)
      if(xevent->Getc1_4(i) > bit_low[i] && xevent->Getc1_4(i) < bit_high[i])	{ //cut on bit TDC to elect detector i


        // Multi hit TDC
        TClonesArray *vtm = xevent->GetVtdc();
        int n = xevent->GetNVtdc();
        // Find ref channel 63
        int ref = 0;
        TVtdc* tv;
        for(int j = 0; j < n; j++){
          tv = (TVtdc *)vtm->operator[](j);
          if ( tv->Index() == 63){
            ref =  tv->Data();
            //printf("n=%u  ref=%u\n",n,ref);
            break;
          }
        }
        if (ref != 0){
          for(int j = 0; j < n; j++) 
            {
              tv = (TVtdc *)vtm->operator[](j);
              Int_t att = (Int_t)(tv->Data() - ref);
              hvt[tv->Index()]->Fill( att );
              //printf("index=%u data=%u\n",tv->Index(),tv->Data());
            }
        }
        
        //
        // VME  QDC
	// fill all QDC channels regardless of content and detector
	// could be useful to determine pedestals (in case of multiple detectors)
        TClonesArray *vq = xevent->GetVqdc();
        n = xevent->GetNVqdc();
        for(int j = 0; j < n; j++) 
          {
            TVqdc *q = (TVqdc *)vq->operator[](j);
            Int_t qi = q->Index();
            hvq[qi]->Fill( q->Data());
          }
   
        // CAMAC QDC, specific for detectors
	he_short[i]->Fill(xevent->Getc1_2(i)); // fill short-gate QDC histogram for this detector
	he_long[i]->Fill(xevent->Getc1_3(i));  // fill long-gate QDC histogram for this detector
	// fill PSD histogram (using hard-coded values for pedestal)
	hPSD[i]->Fill(xevent->Getc1_3(i), (xevent->Getc1_3(i)-pedestal_long[i]-xevent->Getc1_2(i)+pedestal_short[i])/xevent->Getc1_3(i));

	// VME QDC, specific for detectors
        TVqdc *q_long = (TVqdc *)vq->operator[](i);
        Int_t qi_long = q_long->Index();
        TVqdc *q_short = (TVqdc *)vq->operator[](i+32);
        Int_t qi_short = q_short->Index();
	// fill PSD histogram (using hard-coded values for pedestal)
        hvPSD[i]->Fill((q_long->Data()-68.48),(q_long->Data()-68.48 - q_short->Data()+52.94)/(q_long->Data()-68.48));

        // loop over YAPs
	for(int j = 0; j<4;j++){
	  // fill PS_TOF hists:
	  // (-1)*(-2000+VALUE): 'flip' TOF spectrum (to compensate YAPs stopping TOF)
	  // s: distance [m] (hard-coded)
	  // c: speed of light [m/ns]
	  // offset, hard-coded '-190': set t_0 such that gamma flash position corresponds to gamma flight time to reach ext. detector
	  // /4. : normalize to ns (250ps per YAP TDC channel)
          hPS_TOF[i]->Fill((-(-2000+offset[j]+xevent->Getc1_5(j))-190.)/4.+s/c, (xevent->Getc1_3(i)-pedestal_long[i]-xevent->Getc1_2(i)+pedestal_short[i])/xevent->Getc1_3(i)); 
	  hTOF_tdc[i]->Fill(-(-2000+offset[j]+xevent->Getc1_5(j)));
          hTOF[i]->Fill((-(-2000+offset[j]+xevent->Getc1_5(j))-190.)/4.+s/c);
	  // proceed with cuts on PSD spectrum to distinguish neutrons from gammas
	  if(
	     ((xevent->Getc1_3(i)-pedestal_long[i]-xevent->Getc1_2(i)+pedestal_short[i])/xevent->Getc1_3(i) >0.36
	      && (xevent->Getc1_3(i)-pedestal_long[i]-xevent->Getc1_2(i)+pedestal_short[i])/xevent->Getc1_3(i) <0.6
	      && xevent->Getc1_3(i) >600.)
	     ||
	     ((xevent->Getc1_3(i)-pedestal_long[i]-xevent->Getc1_2(i)+pedestal_short[i])/xevent->Getc1_3(i) >0.45-0.0001*xevent->Getc1_3(i)
	      && (xevent->Getc1_3(i)-pedestal_long[i]-xevent->Getc1_2(i)+pedestal_short[i])/xevent->Getc1_3(i) <0.6
	      && xevent->Getc1_3(i) <=600.)
	     ) { // neutron contour cut on the PSD
            if(xevent->Getc1_3(i) > 0.){ // PS cut 
              hTOF_tdc_neutron[i]->Fill(-(-2000.+offset[j] +xevent->Getc1_5(j)));
              hTOF_neutron[i]->Fill((-(-2000+offset[j]+xevent->Getc1_5(j))-190.)/4.+s/c);
	      // 939.57: mass of n in MeV
              if(((-(-2000+offset[j]+xevent->Getc1_5(j))-190.)/4.+s/c) > 10.)
                hEkin[i]->Fill(0.5*939.57*s*s/c/c/((-(-2000+offset[j]+xevent->Getc1_5(j))-190.)/4.+s/c)/((-(-2000+offset[j]+xevent->Getc1_5(j))-190.)/4.+s/c));
              if(-(-2000.+offset[j]+xevent->Getc1_5(j)) <600. && -(-2000.+offset[j] +xevent->Getc1_5(j)) >  330.){
                he_short_neutron[i]->Fill(xevent->Getc1_2(i));             
                he_long_neutron[i]->Fill(xevent->Getc1_3(i));
              }
	    }
	  }
	}
      }
    }
    
  }// End loop over X events --

  for(int i=0; i<4; i++) { // get background information for TOF


    double par [3];
    
    TF1 *fit = new TF1("fit",fit_background,0,2047,1);

    hTOF_tdc_neutron[i]->Fit("fit", "q");

    fit->GetParameters(par);
    
    for ( int j = 0; j < 2048; j++){

      hTOF_tdc_neutron_background[i]->SetBinContent(j, par[0]);
    }
    
        
    TF1 *fit2 = new TF1("fit2",fit_background,-50,500,1);

    hTOF_neutron[i]->Fit("fit2", "q");

    fit2->GetParameters(par);
    
    for ( int j = 0; j < 2048; j++){
      hTOF_neutron_background[i]->SetBinContent(j, par[0]);
    }
        
  }


  for(int i=0; i<4; i++) {    
    for(int j=190; j<2048; j++) {
       for(int l = 0; l < hTOF_neutron_background[i]->GetBinContent(j); l++){  
         double tof = double(j)*550./2048-50.;   
         hEkin_background[i] -> Fill(0.5*939.57*s*s/c/c/tof/tof);
       }
     }
  }
   
  /*
  for(int i=0; i<4; i++) {	     
    he_tdc[i]->Scale(1./sentries);
    he_short[i]->Scale(1./sentries);             
    he_long[i]->Scale(1./sentries);
    he_short_neutron[i]->Scale(1./sentries);             
    he_long_neutron[i]->Scale(1./sentries);
    hPSD[i]->Scale(1./sentries);
    hTOF_tdc[i]->Scale(1./sentries);
    hTOF_tdc_neutron[i]->Scale(1./sentries);
  }
  */
  
  infile->Close();  
  
  run++;
  
  
  
  nentries_file.close();
  
  output->Write();
  
  delete output;
  delete xevent;
  delete scalerevent;
  delete infile;
  
  return 0;
}//close main --
