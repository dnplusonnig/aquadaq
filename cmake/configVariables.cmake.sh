#!/bin/sh

AQUADAQ_VERSION="@AQUADAQ_FULL_VERSION@"
AQUADAQ_SOURCE_DIR="@PROJECT_SOURCE_DIR@"
AQUADAQ_DATA_PATH="@DATA_PATH@"
AQUADAQ_INSTALL_PATH="@CMAKE_INSTALL_PREFIX@"
